/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.3.15-MariaDB : Database - samaktadb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`samaktadb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `samaktadb`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id_admin` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_admin` varchar(100) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password_admin` varchar(100) DEFAULT NULL,
  `level_admin` varchar(5) DEFAULT NULL COMMENT 'ADM, MNG,ADR',
  `path_foto_admin` varchar(100) DEFAULT NULL,
  `status_admin` varchar(2) DEFAULT 'A',
  PRIMARY KEY (`id_admin`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `bidang_usaha` */

DROP TABLE IF EXISTS `bidang_usaha`;

CREATE TABLE `bidang_usaha` (
  `id_bidang_usaha` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_bidang_usaha` varchar(150) DEFAULT NULL,
  `status_bidang_usaha` varchar(2) DEFAULT 'A',
  PRIMARY KEY (`id_bidang_usaha`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `city_id` varchar(10) NOT NULL,
  `province_id` varchar(5) DEFAULT NULL,
  `city_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`city_id`),
  KEY `province_id` (`province_id`),
  CONSTRAINT `city_ibfk_1` FOREIGN KEY (`province_id`) REFERENCES `province` (`province_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tipe_customer` int(10) unsigned DEFAULT NULL,
  `nama_customer` varchar(100) DEFAULT NULL,
  `npwp_customer` varchar(50) DEFAULT NULL,
  `alamat_customer` varchar(255) DEFAULT NULL,
  `city_id` varchar(10) DEFAULT NULL,
  `kode_pos_customer` varchar(10) DEFAULT NULL,
  `telp_customer` varchar(15) DEFAULT NULL,
  `email_customer` varchar(50) DEFAULT NULL,
  `website_customer` varchar(50) DEFAULT NULL,
  `id_bidang_usaha` int(10) unsigned DEFAULT NULL,
  `pic_customer` varchar(100) DEFAULT NULL,
  `telp_pic_customer` varchar(15) DEFAULT NULL,
  `id_sales` int(10) unsigned DEFAULT NULL,
  `path_foto_customer` varchar(100) DEFAULT NULL,
  `status_customer` varchar(2) DEFAULT 'A' COMMENT 'A aktif, T Tidak Aktif',
  PRIMARY KEY (`id_customer`),
  KEY `id_tipe_customer` (`id_tipe_customer`),
  KEY `city_id` (`city_id`),
  KEY `id_sales` (`id_sales`),
  KEY `id_bidang_usaha` (`id_bidang_usaha`),
  CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`id_tipe_customer`) REFERENCES `tipe_customer` (`id_tipe_customer`) ON UPDATE CASCADE,
  CONSTRAINT `customer_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`) ON UPDATE CASCADE,
  CONSTRAINT `customer_ibfk_3` FOREIGN KEY (`id_sales`) REFERENCES `sales` (`id_sales`) ON UPDATE CASCADE,
  CONSTRAINT `customer_ibfk_4` FOREIGN KEY (`id_bidang_usaha`) REFERENCES `bidang_usaha` (`id_bidang_usaha`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;

/*Table structure for table `district` */

DROP TABLE IF EXISTS `district`;

CREATE TABLE `district` (
  `district_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `city_id` varchar(10) DEFAULT NULL,
  `district_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`district_id`),
  KEY `RELATION_728_FK` (`city_id`),
  CONSTRAINT `district_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7094 DEFAULT CHARSET=latin1;

/*Table structure for table `jabatan` */

DROP TABLE IF EXISTS `jabatan`;

CREATE TABLE `jabatan` (
  `id_jabatan` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(50) DEFAULT NULL,
  `status_jabatan` varchar(2) DEFAULT 'A',
  PRIMARY KEY (`id_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

/*Table structure for table `jenis_lampiran` */

DROP TABLE IF EXISTS `jenis_lampiran`;

CREATE TABLE `jenis_lampiran` (
  `id_jenis_lampiran` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_jenis_lampiran` varchar(50) DEFAULT NULL,
  `jenis_lampiran_untuk` varchar(2) DEFAULT NULL COMMENT 'C Customer, V Vendor',
  `status_jenis_lampiran` varchar(2) DEFAULT 'A',
  PRIMARY KEY (`id_jenis_lampiran`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Table structure for table `lampiran_customer` */

DROP TABLE IF EXISTS `lampiran_customer`;

CREATE TABLE `lampiran_customer` (
  `id_customer` int(10) unsigned NOT NULL,
  `id_jenis_lampiran` int(10) unsigned NOT NULL,
  `file_lampiran` varchar(100) DEFAULT NULL COMMENT 'Nama file lampiran Customer',
  PRIMARY KEY (`id_customer`,`id_jenis_lampiran`),
  KEY `id_customer` (`id_customer`,`id_jenis_lampiran`),
  KEY `id_jenis_lampiran` (`id_jenis_lampiran`),
  CONSTRAINT `lampiran_customer_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`) ON UPDATE CASCADE,
  CONSTRAINT `lampiran_customer_ibfk_2` FOREIGN KEY (`id_jenis_lampiran`) REFERENCES `jenis_lampiran` (`id_jenis_lampiran`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `lampiran_vendor` */

DROP TABLE IF EXISTS `lampiran_vendor`;

CREATE TABLE `lampiran_vendor` (
  `id_vendor` int(10) unsigned NOT NULL,
  `id_jenis_lampiran` int(10) unsigned NOT NULL,
  `file_lampiran` varchar(100) DEFAULT NULL COMMENT 'Nama file lampiran',
  PRIMARY KEY (`id_vendor`,`id_jenis_lampiran`),
  KEY `id_jenis_lampiran` (`id_jenis_lampiran`),
  KEY `id_vendor` (`id_vendor`,`id_jenis_lampiran`),
  CONSTRAINT `lampiran_vendor_ibfk_1` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id_vendor`) ON UPDATE CASCADE,
  CONSTRAINT `lampiran_vendor_ibfk_2` FOREIGN KEY (`id_jenis_lampiran`) REFERENCES `jenis_lampiran` (`id_jenis_lampiran`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_product` varchar(50) DEFAULT NULL,
  `status_product` varchar(2) DEFAULT 'A' COMMENT 'A Aktif; T tidak aktif',
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `project` */

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id_project` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_year` varchar(4) DEFAULT NULL,
  `nama_project` varchar(150) DEFAULT NULL,
  `id_product` int(10) unsigned DEFAULT NULL,
  `project_type` varchar(2) DEFAULT NULL COMMENT 'TL TUnjuk langsung, TE Tender',
  `tipe_kontrak_project` varchar(2) DEFAULT NULL COMMENT 'O : One Time, R Recurring',
  `nilai_project` double DEFAULT NULL,
  `project_start_date` date DEFAULT NULL,
  `project_end_date` date DEFAULT NULL,
  `tgl_fs_approved` date DEFAULT NULL,
  `tgl_project_final` date DEFAULT NULL,
  `no_fs_barang` varchar(50) DEFAULT NULL,
  `nilai_fs_barang` double DEFAULT NULL,
  `spk_fs_barang` varchar(50) DEFAULT NULL,
  `tgl_fs_barang` date DEFAULT NULL,
  `nilai_project_barang` double DEFAULT NULL,
  `start_date_barang` date DEFAULT NULL,
  `end_date_barang` date DEFAULT NULL,
  `no_fs_jasa` varchar(50) DEFAULT NULL,
  `nilai_fs_jasa` int(11) DEFAULT NULL,
  `spk_fs_jasa` varchar(50) DEFAULT NULL,
  `tgl_fs_jasa` date DEFAULT NULL,
  `nilai_project_jasa` double DEFAULT NULL,
  `start_date_jasa` date DEFAULT NULL,
  `end_date_jasa` date DEFAULT NULL,
  `tipe_pembelian` varchar(2) DEFAULT NULL COMMENT 'B Barang, J jasa, BJ Barang dan jasa',
  `id_customer` int(10) unsigned DEFAULT NULL,
  `nama_pic_project` varchar(50) DEFAULT NULL,
  `no_hp_pic_project` varchar(15) DEFAULT NULL,
  `email_pic_project` varchar(50) DEFAULT NULL,
  `id_sales` int(10) unsigned DEFAULT NULL,
  `status_project` varchar(2) DEFAULT 'P' COMMENT 'P: Pending/waiting,W: Win L: Lose, C:Cancel',
  `tgl_pengisian_tender` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `created_by` varchar(25) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `modified_by` varchar(25) DEFAULT NULL,
  `project_deleted` int(2) DEFAULT 0,
  PRIMARY KEY (`id_project`),
  KEY `id_product` (`id_product`),
  KEY `id_customer` (`id_customer`),
  KEY `id_sales` (`id_sales`),
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`) ON UPDATE CASCADE,
  CONSTRAINT `project_ibfk_2` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id_customer`) ON UPDATE CASCADE,
  CONSTRAINT `project_ibfk_3` FOREIGN KEY (`id_sales`) REFERENCES `sales` (`id_sales`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `project_vendor` */

DROP TABLE IF EXISTS `project_vendor`;

CREATE TABLE `project_vendor` (
  `id_project` int(10) unsigned NOT NULL,
  `id_vendor` int(10) unsigned NOT NULL,
  `tipe_pembelian` varchar(2) NOT NULL COMMENT 'B Barang J Jasa',
  `tipe_project_vendor` varchar(5) DEFAULT NULL COMMENT 'TL TUnjuk langsung, TE Tender',
  `no_fpt` varchar(100) DEFAULT NULL,
  `no_quotation` varchar(100) DEFAULT NULL,
  `nilai_quotation_awal` int(11) DEFAULT NULL,
  `nilai_quotation_akhir` int(11) DEFAULT NULL,
  `expired_quotation` date DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL COMMENT 'W Win ; L Lose ; P : Waiting',
  `keterangan` varchar(255) DEFAULT NULL,
  `lampiran` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_project`,`id_vendor`,`tipe_pembelian`),
  KEY `id_project` (`id_project`),
  KEY `id_vendor` (`id_vendor`),
  CONSTRAINT `project_vendor_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id_project`) ON UPDATE CASCADE,
  CONSTRAINT `project_vendor_ibfk_2` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id_vendor`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `province` */

DROP TABLE IF EXISTS `province`;

CREATE TABLE `province` (
  `province_id` varchar(5) NOT NULL,
  `country_id` char(2) DEFAULT NULL,
  `province_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`province_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `sales` */

DROP TABLE IF EXISTS `sales`;

CREATE TABLE `sales` (
  `id_sales` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_jabatan` int(10) unsigned DEFAULT NULL,
  `nik_sales` varchar(10) DEFAULT NULL COMMENT 'nomor induk karyawan',
  `nama_sales` varchar(100) DEFAULT NULL,
  `telp_sales` varchar(15) DEFAULT NULL,
  `email_sales` varchar(50) DEFAULT NULL,
  `path_foto_sales` varchar(100) DEFAULT NULL,
  `status_sales` varchar(2) DEFAULT 'A',
  PRIMARY KEY (`id_sales`),
  UNIQUE KEY `nik` (`nik_sales`),
  KEY `id_jabatan` (`id_jabatan`),
  CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id_jabatan`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id_settings` varchar(50) NOT NULL,
  `nama_settings` varchar(100) DEFAULT NULL,
  `setting_value` varchar(250) DEFAULT NULL,
  `setting_default` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_settings`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `target_perusahaan` */

DROP TABLE IF EXISTS `target_perusahaan`;

CREATE TABLE `target_perusahaan` (
  `tahun_target` varchar(4) NOT NULL,
  `nilai_target` double DEFAULT NULL,
  `nilai_target_one_time` double DEFAULT NULL,
  `nilai_target_recurring` double DEFAULT NULL,
  PRIMARY KEY (`tahun_target`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tender_vendor` */

DROP TABLE IF EXISTS `tender_vendor`;

CREATE TABLE `tender_vendor` (
  `id_project` int(10) unsigned NOT NULL,
  `id_vendor` int(10) unsigned NOT NULL,
  `tipe_pembelian` varchar(2) NOT NULL,
  `no_po_spk` varchar(50) DEFAULT NULL,
  `v_commitment_start` date DEFAULT NULL,
  `v_commitment_finish` date DEFAULT NULL,
  `spl_create` date DEFAULT NULL,
  `spl_internal_approve` date DEFAULT NULL,
  `spl_full_approve` date DEFAULT NULL,
  `fpt_sent` date DEFAULT NULL,
  `tgl_nego` date DEFAULT NULL,
  `wbs_create` date DEFAULT NULL,
  `p3_fps_create` date DEFAULT NULL,
  `p3_fps_release` date DEFAULT NULL,
  `sp_release` date DEFAULT NULL,
  `spk_fps_create` date DEFAULT NULL,
  `no_spk_fps` varchar(100) DEFAULT NULL,
  `spk_fps_approve` date DEFAULT NULL,
  `contract_po_create` date DEFAULT NULL,
  `no_contract_po` varchar(100) DEFAULT NULL,
  `contract_po_release` date DEFAULT NULL,
  `p3_fpb_cancel` date DEFAULT NULL,
  `cancel_vendor` date DEFAULT NULL,
  PRIMARY KEY (`id_project`,`id_vendor`,`tipe_pembelian`),
  KEY `id_project` (`id_project`),
  KEY `id_vendor` (`id_vendor`),
  CONSTRAINT `tender_vendor_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id_project`) ON UPDATE CASCADE,
  CONSTRAINT `tender_vendor_ibfk_2` FOREIGN KEY (`id_vendor`) REFERENCES `vendor` (`id_vendor`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tipe_customer` */

DROP TABLE IF EXISTS `tipe_customer`;

CREATE TABLE `tipe_customer` (
  `id_tipe_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_tipe_customer` varchar(100) DEFAULT NULL,
  `status_tipe_customer` varchar(2) DEFAULT 'A' COMMENT 'A : aktif , T : Tidak Aktif',
  PRIMARY KEY (`id_tipe_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `vendor` */

DROP TABLE IF EXISTS `vendor`;

CREATE TABLE `vendor` (
  `id_vendor` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_vendor` varchar(100) DEFAULT NULL,
  `npwp_vendor` varchar(50) DEFAULT NULL,
  `alamat_vendor` varchar(255) DEFAULT NULL,
  `city_id` varchar(10) DEFAULT NULL,
  `kode_pos_vendor` varchar(10) DEFAULT NULL,
  `telp_vendor` varchar(15) DEFAULT NULL,
  `email_vendor` varchar(50) DEFAULT NULL,
  `website_vendor` varchar(50) DEFAULT NULL,
  `bidang_usaha_vendor` varchar(100) DEFAULT NULL,
  `pic_vendor` varchar(100) DEFAULT NULL,
  `telp_pic_vendor` varchar(15) DEFAULT NULL,
  `path_foto_vendor` varchar(100) DEFAULT NULL,
  `status_vendor` varchar(2) DEFAULT 'A' COMMENT 'A aktif, T Tidak Aktif',
  PRIMARY KEY (`id_vendor`),
  KEY `city_id` (`city_id`),
  CONSTRAINT `vendor_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=latin1;

/* Trigger structure for table `project_vendor` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `before_insert_project_vendor` */$$

/*!50003 CREATE */ /*!50003 TRIGGER `before_insert_project_vendor` BEFORE INSERT ON `project_vendor` FOR EACH ROW BEGIN	
	set NEW.no_fpt = generate_no_fpt(NEW.id_project,NEW.tipe_project_vendor);
    END */$$


DELIMITER ;

/* Function  structure for function  `format_kode_project` */

/*!50003 DROP FUNCTION IF EXISTS `format_kode_project` */;
DELIMITER $$

/*!50003 CREATE FUNCTION `format_kode_project`(`p_id_project` INT) RETURNS varchar(25) CHARSET latin1
BEGIN
	declare v_urutan int;
	declare v_kode varchar(25);
	declare	v_romawi varchar(10);
	declare v_tahun_project varchar(4);
	
	select to_roman(DATE_FORMAT(created_at,'%c')) as romawi, date_format(created_at,'%Y') as tahun into v_romawi, v_tahun_project
	from project 
	where id_project = p_id_project;
	
	set @row_num = 0;
	select urutan INTO v_urutan 
	from (select @row_num := @row_num+1 as urutan, id_project
		FROM project
		where project_year = v_tahun_project
		order by id_project asc) as tabel_project
	where id_project = p_id_project;
	
	set v_kode = concat(lpad(v_urutan,3,'0'),'/SM/',v_romawi,'/',v_tahun_project);
	
	return v_kode;
    END */$$
DELIMITER ;

/* Function  structure for function  `generate_no_fpt` */

/*!50003 DROP FUNCTION IF EXISTS `generate_no_fpt` */;
DELIMITER $$

/*!50003 CREATE FUNCTION `generate_no_fpt`(p_id_project int,p_tipe_project_vendor varchar(5)) RETURNS varchar(20) CHARSET latin1
BEGIN
    
	declare v_new_no_fpt varchar(20);
	declare v_urutan int;
	declare v_romawi varchar(5);
	declare v_exist_fpt_tender varchar(20);
	DECLARE v_tahun VARCHAR(4);
	
	select to_roman(DATE_FORMAT(created_at,'%c')) into v_romawi from project where id_project = p_id_project;
	select date_format(created_at,'%Y') into v_tahun from project WHERE id_project = p_id_project;
	
	if p_tipe_project_vendor = 'TE' then
		select distinct no_fpt into v_exist_fpt_tender from project_vendor where id_project = p_id_project and tipe_project_vendor = 'TE';
		
		if v_exist_fpt_tender is null then
			-- create baru		
			SELECT (IFNULL(CAST(MAX(SUBSTR(no_fpt,1,4)) AS UNSIGNED),0)+1) INTO v_urutan FROM project_vendor;
			SET v_new_no_fpt = CONCAT(LPAD(v_urutan,4,'0'),'/FPT/',v_romawi,'/',v_tahun);
		else
			-- gunakan yg sudah ada	 
			set v_new_no_fpt = v_exist_fpt_tender;
		end if;
	else
		SELECT (IFNULL(CAST(MAX(SUBSTR(no_fpt,1,4)) AS UNSIGNED),0)+1) into v_urutan FROM project_vendor;
		set v_new_no_fpt = CONCAT(LPAD(v_urutan,4,'0'),'/FPT/',v_romawi,'/',v_tahun);
	end if;
    
	return v_new_no_fpt;
    

    END */$$
DELIMITER ;

/* Function  structure for function  `get_sum_target` */

/*!50003 DROP FUNCTION IF EXISTS `get_sum_target` */;
DELIMITER $$

/*!50003 CREATE FUNCTION `get_sum_target`(p_tahun varchar(4), p_tipe varchar(2)) RETURNS double
BEGIN
	declare v_result double;
	
	select sum(nilai_project) as nilai into v_result
	from project
	where project_year = p_tahun
	and tipe_kontrak_project = p_tipe and status_project = 'W';
	
	return v_result;
    END */$$
DELIMITER ;

/* Function  structure for function  `to_roman` */

/*!50003 DROP FUNCTION IF EXISTS `to_roman` */;
DELIMITER $$

/*!50003 CREATE FUNCTION `to_roman`(`inArabic` INT) RETURNS varchar(15) CHARSET latin1
BEGIN
	DECLARE numeral CHAR(7) DEFAULT 'IVXLCDM';

	    DECLARE stringInUse CHAR(3);
	    DECLARE position tinyint DEFAULT 1;
	    DECLARE currentDigit tinyint;

	    DECLARE returnValue VARCHAR(15) DEFAULT '';

	    IF(inArabic > 3999) THEN RETURN 'overflow'; END IF;
	    IF(inArabic = 0) THEN RETURN 'N'; END IF;

	    WHILE position <= CEIL(LOG10(inArabic + .1)) DO
		SET currentDigit := MOD(FLOOR(inArabic / POW(10, position - 1)), 10);

		SET returnValue := CONCAT(
		    CASE currentDigit
			WHEN 4 THEN CONCAT(SUBSTRING(numeral, position * 2 - 1, 1), SUBSTRING(numeral, position * 2, 1))
			WHEN 9 THEN CONCAT(SUBSTRING(numeral, position * 2 - 1, 1), SUBSTRING(numeral, position * 2 + 1, 1))
			ELSE CONCAT(
			    REPEAT(SUBSTRING(numeral, position * 2, 1), currentDigit >= 5),
			    REPEAT(SUBSTRING(numeral, position * 2 - 1, 1), MOD(currentDigit, 5))
			)
		    END,
		    returnValue);

		SET position := position + 1;
	    END WHILE;
	    RETURN returnValue;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

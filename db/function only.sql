/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.1.38-MariaDB : Database - samaktadb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`samaktadb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `samaktadb`;

/* Function  structure for function  `format_kode_project` */

/*!50003 DROP FUNCTION IF EXISTS `format_kode_project` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `format_kode_project`(`p_id_project` INT) RETURNS varchar(25) CHARSET latin1
BEGIN
	declare v_urutan int;
	declare v_kode varchar(25);
	declare	v_romawi varchar(10);
	declare v_tahun_project varchar(4);
	
	select to_roman(DATE_FORMAT(created_at,'%c')) as romawi, date_format(created_at,'%Y') as tahun into v_romawi, v_tahun_project
	from project 
	where id_project = p_id_project;
	
	set @row_num = 0;
	select urutan INTO v_urutan 
	from (select @row_num := @row_num+1 as urutan, id_project
		FROM project
		where project_year = v_tahun_project and project_deleted = 0
		order by id_project asc) as tabel_project
	where id_project = p_id_project;
	
	set v_kode = concat(lpad(v_urutan,3,'0'),'/SM/',v_romawi,'/',v_tahun_project);
	
	return v_kode;
    END */$$
DELIMITER ;

/* Function  structure for function  `generate_no_fpt` */

/*!50003 DROP FUNCTION IF EXISTS `generate_no_fpt` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `generate_no_fpt`(`p_id_project` INT, `p_tipe_project_vendor` VARCHAR(5)) RETURNS varchar(20) CHARSET latin1
BEGIN
    
	declare v_new_no_fpt varchar(20);
	declare v_urutan int;
	declare v_romawi varchar(10);
	declare v_exist_fpt_tender varchar(20);
	DECLARE v_tahun VARCHAR(4);
	
	select to_roman(DATE_FORMAT(created_at,'%c')) into v_romawi from project where id_project = p_id_project;
	select date_format(created_at,'%Y') into v_tahun from project WHERE id_project = p_id_project;
	
	if p_tipe_project_vendor = 'TE' then
		select distinct no_fpt into v_exist_fpt_tender from project_vendor where id_project = p_id_project and tipe_project_vendor = 'TE';
		
		if v_exist_fpt_tender is null then
			-- create baru		
			SELECT (IFNULL(CAST(MAX(SUBSTR(no_fpt,1,4)) AS UNSIGNED),0)+1) INTO v_urutan FROM project_vendor;
			SET v_new_no_fpt = CONCAT(LPAD(v_urutan,4,'0'),'/FPT/',v_romawi,'/',v_tahun);
		else
			-- gunakan yg sudah ada	 
			set v_new_no_fpt = v_exist_fpt_tender;
		end if;
	else
		SELECT (IFNULL(CAST(MAX(SUBSTR(no_fpt,1,4)) AS UNSIGNED),0)+1) into v_urutan FROM project_vendor;
		set v_new_no_fpt = CONCAT(LPAD(v_urutan,4,'0'),'/FPT/',v_romawi,'/',v_tahun);
	end if;
    
	return v_new_no_fpt;
    

    END */$$
DELIMITER ;

/* Function  structure for function  `get_sum_target` */

/*!50003 DROP FUNCTION IF EXISTS `get_sum_target` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `get_sum_target`(`p_tahun` VARCHAR(4), `p_tipe` VARCHAR(2)) RETURNS double
BEGIN
	declare v_result double;
	
	select sum(nilai_project) as nilai into v_result
	from project
	where project_year = p_tahun and project_deleted = 0
	and tipe_kontrak_project = p_tipe and status_project = 'W';
	
	return v_result;
    END */$$
DELIMITER ;

/* Function  structure for function  `to_roman` */

/*!50003 DROP FUNCTION IF EXISTS `to_roman` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `to_roman`(`inArabic` INT) RETURNS varchar(15) CHARSET latin1
BEGIN
	DECLARE numeral CHAR(7) DEFAULT 'IVXLCDM';

	    DECLARE stringInUse CHAR(3);
	    DECLARE position tinyint DEFAULT 1;
	    DECLARE currentDigit tinyint;

	    DECLARE returnValue VARCHAR(15) DEFAULT '';

	    IF(inArabic > 3999) THEN RETURN 'overflow'; END IF;
	    IF(inArabic = 0) THEN RETURN 'N'; END IF;

	    WHILE position <= CEIL(LOG10(inArabic + .1)) DO
		SET currentDigit := MOD(FLOOR(inArabic / POW(10, position - 1)), 10);

		SET returnValue := CONCAT(
		    CASE currentDigit
			WHEN 4 THEN CONCAT(SUBSTRING(numeral, position * 2 - 1, 1), SUBSTRING(numeral, position * 2, 1))
			WHEN 9 THEN CONCAT(SUBSTRING(numeral, position * 2 - 1, 1), SUBSTRING(numeral, position * 2 + 1, 1))
			ELSE CONCAT(
			    REPEAT(SUBSTRING(numeral, position * 2, 1), currentDigit >= 5),
			    REPEAT(SUBSTRING(numeral, position * 2 - 1, 1), MOD(currentDigit, 5))
			)
		    END,
		    returnValue);

		SET position := position + 1;
	    END WHILE;
	    RETURN returnValue;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

<div class="nav-item">
<a href="<?= base_url('site/dashboard') ?>" class="menu-item"><i class="ik ik-bar-chart-2"></i><span>Dashboard</span></a>
    <a href="<?= base_url('site/monitoring') ?>" class="menu-item"><i class="fa fa-desktop"></i><span>Monitoring</span></a>
</div>
<!-- <div class="nav-lavel">Data</div> -->
<div class="nav-lavel">DATA MASTER</div>
<div class="nav-item has-sub">
	<a href="#"><i class="ik ik-users"></i><span>Customer</span></a>
	<div class="submenu-content">
	<a href="<?= base_url('site/tipe_customer') ?>" class="menu-item">Tipe Customer</a>
		<a href="<?= base_url('site/data_customer') ?>" class="menu-item">Data Customer</a>
	</div>
</div>

<div class="nav-item has-sub">
	<a href="#"><i class="ik ik-link"></i><span>Vendor</span></a>
	<div class="submenu-content">
		<a href="<?= base_url('site/data_vendor') ?>" class="menu-item">Data Vendor</a>
	</div>
</div>

<div class="nav-item has-sub">
	<a href="#"><i class="ik ik-edit"></i><span>Project</span></a>
	<div class="submenu-content">
	<a href="<?= base_url('site/product') ?>" class="menu-item">Product</a>
		<a href="<?= base_url('site/data_project') ?>" class="menu-item">Data Project</a>
	</div>
</div>

<div class="nav-item has-sub">
	<a href="#"><i class="ik ik-check-square"></i><span>Sales</span></a>
	<div class="submenu-content">
	<a href="<?= base_url('site/jabatan') ?>" class="menu-item">Jabatan</a>
		<a href="<?= base_url('site/data_sales') ?>" class="menu-item">Data Sales</a>
	</div>
</div>
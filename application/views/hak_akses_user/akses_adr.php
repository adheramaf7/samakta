<div class="nav-item">
    <a href="<?= base_url('site/dashboard') ?>" class="menu-item"><i class="ik ik-bar-chart-2"></i><span>Dashboard</span></a>
    <a href="<?= base_url('site/monitoring') ?>" class="menu-item"><i class="fa fa-desktop"></i><span>Monitoring</span></a>
</div>
<div class="nav-lavel">DATA MASTER</div>
<div class="nav-item has-sub">
	<a href="#"><i class="ik ik-hard-drive"></i><span>Umum</span></a>
	<div class="submenu-content">
		<a href="<?= base_url('site/jenis_lampiran') ?>" class="menu-item">Jenis Lampiran</a>
	</div>
</div>
<div class="nav-item has-sub">
	<a href="#"><i class="ik ik-users"></i><span>Customer</span></a>
	<div class="submenu-content">
		<a href="<?= base_url('site/tipe_customer') ?>" class="menu-item">Tipe Customer</a>
		<a href="<?= base_url('site/bidang_usaha') ?>" class="menu-item">Bidang Usaha</a>
		<a href="<?= base_url('site/data_customer') ?>" class="menu-item">Data Customer</a>
	</div>
</div>

<div class="nav-item has-sub">
	<a href="#"><i class="ik ik-link"></i><span>Vendor</span></a>
	<div class="submenu-content">
		<a href="<?= base_url('site/data_vendor') ?>" class="menu-item">Data Vendor</a>
	</div>
</div>

<div class="nav-item has-sub">
	<a href="#"><i class="ik ik-edit"></i><span>Project</span></a>
	<div class="submenu-content">
		<a href="<?= base_url('site/product') ?>" class="menu-item">Product</a>
		<a href="<?= base_url('site/data_project') ?>" class="menu-item">Data Project</a>
	</div>
</div>

<div class="nav-item has-sub">
	<a href="#"><i class="ik ik-check-square"></i><span>Sales</span></a>
	<div class="submenu-content">
		<a href="<?= base_url('site/jabatan') ?>" class="menu-item">Jabatan</a>
		<a href="<?= base_url('site/data_sales') ?>" class="menu-item">Data Sales</a>
	</div>
</div>

<div class="nav-lavel">ADMIN MASTER</div>
<div class="nav-item">
	<a href="<?= base_url('site/data_admin') ?>" class="menu-item"><i class="ik ik-user"></i><span>Data Admin</span></a>
</div>

<div class="nav-lavel">SETTING</div>

<div class="nav-item has-sub">
	<a href="#"><i class="ik ik-users"></i><span>Setting</span></a>
	<div class="submenu-content">
		<a href="<?= base_url('site/setting_perusahaan') ?>" class="menu-item"><span>Setting Perusahaan</span></a>
		<a href="<?= base_url('site/target_perusahaan') ?>" class="menu-item"><span>Target Perusahaan</span></a>
	</div>
</div>
<div class="nav-item">
	<!-- <a href="<?= base_url('site/seeder') ?>" class="menu-item"><i class="ik ik-database"></i><span>Data Seeder</span></a> -->
</div>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h5>Target Perusahaan</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url() ?>"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">Setting</li>
                    <li class="breadcrumb-item active" aria-current="page">Target Perusahaan</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-3">
        <div class="card" id="form_card">
            <div class="card-header"><h3>Setting Target</h3></div>
            <div class="card-body">
                <form class="forms-sample" id="form_target" method="POST" action="javascript:;">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="tahun_target">Tahun</label>
                                <input type="text" class="form-control tahun" data-target="#tahun_target" name="tahun_target" id="tahun_target" value="<?= date('Y') ?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="nilai_target_one_time">Nilai Target One Time (Rp)</label>
                                <input type="text" class="form-control autonumeric" name="nilai_target_one_time" id="nilai_target_one_time">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="nilai_target_recurring">Nilai Target Recurring (Rp)</label>
                                <input type="text" class="form-control autonumeric" name="nilai_target_recurring" id="nilai_target_recurring">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="nilai_target">Nilai Target (Rp)</label>
                                <input type="text" class="form-control autonumeric" name="nilai_target" id="nilai_target" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <button id="btnSimpan" type="submit" class="btn btn-primary mr-2" <?= !$ha['update']? 'disabled' : '' ?>>Simpan Perubahan</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="card" id="form_card">
            <div class="card-header"><h3>List Target</h3></div>
            <div class="card-body">
                <div style="padding: 1%">
                    <table id="tabel" class="table table-inverse table-hover table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th rowspan="2">Tahun</th>
                                <th colspan="2" class="text-center">Nilai Target</th>
                                <th colspan="2" class="text-center">One Time</th>
                                <th colspan="2" class="text-center">Recurring</th>
                            </tr>
                            <tr>
                                <th>Target</th>
                                <th>Pencapaian</th>
                                <th>Target</th>
                                <th>Pencapaian</th>
                                <th>Target</th>
                                <th>Pencapaian</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var mys;
    var form_validator_target;

    $(document).ready(function() {
        mys = Object.create(myscript_js);
        mys.init('<?= base_url() ?>');

        $('#tabel').DataTable({
            "scrollCollapse": true,
            "sDom": "t",
            "processing": true,
            "iDisplayLength": 10,
            "scrollX":true,
            "ajax":{
                url : mys.base_url+'settings/get_data_target',
                type : 'GET',
            },
            "language": {
                "url": mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            "columnDefs": [
            {"visible" : false, "targets" : []},
            {
                render: function ( data, type, row ) {
                    if (type == 'sort') {
                        return data;
                    }
                    return mys.formatMoney(data,0,',','.');
                },
                targets: [1,2,3,4,5,6]
            },
            ],
            "columns": [
            {"width": "10%", "orderable" : false},
            {"width": "15%", "orderable" : false},
            {"width": "15%", "orderable" : false},
            {"width": "15%", "orderable" : false},
            {"width": "15%", "orderable" : false},
            {"width": "15%", "orderable" : false},
            {"width": "15%", "orderable" : false},
            ],
            "order" : [
            [0, "asc"],
            ],
            "fnDrawCallback" : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        form_validator_target = $('#form_target').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent("div").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        $('#form_target').submit(function(event) {
            if (form_validator_target.form()) {
                simpan_target();
            }
        });

        $('#nilai_target_one_time,#nilai_target_recurring').on('keyup', function(event) {
            set_nilai_target();
        });

        $('#tahun_target').on('change.datetimepicker', function(event) {
            load_target();
        });
        load_target();
    });

    function reload() {
        var t = $('#tabel').DataTable();
        t.ajax.reload();
    }

    function set_nilai_target() {
        var nilai_target_one_time = $('#nilai_target_one_time').val()? mys.reverse_format_ribuan($('#nilai_target_one_time').val()) : 0;
        var nilai_target_recurring = $('#nilai_target_recurring').val()? mys.reverse_format_ribuan($('#nilai_target_recurring').val()) : 0;

        var nilai_target = parseInt(nilai_target_one_time) + parseInt(nilai_target_recurring);

        $('#nilai_target').val(nilai_target).blur();
    }

    function reset_target() {
        form_validator_target.resetForm();
        $('#form_target')[0].reset();
        $('#form_target').find('input[type="hidden"]').val('');
        $('#form_target').find('label,select,input,textarea').removeClass('is-invalid text-red');
        $('#form_target').find('label,select,input,textarea').removeAttr('disabled');
    }

    function load_target() {
        mys.blok();
        var tahun_target = $('#tahun_target').val();
        $.ajax({
            url: mys.base_url+'settings/load_target',
            type: 'GET',
            dataType: 'JSON',
            data:{
                tahun_target : tahun_target,
            },
            success:function(data){
                reset_target();
                $('#tahun_target').val(tahun_target);
                if (data) {
                    $('#nilai_target_recurring').val(data.nilai_target_recurring).blur();
                    $('#nilai_target_one_time').val(data.nilai_target_one_time).blur();
                    $('#nilai_target').val(parseInt(data.nilai_target_recurring)+parseInt(data.nilai_target_one_time)).blur();
                }
            },
            error:function(data){
                mys.notifikasi("Gagal Mengambil data dari server", "error");
            },
        })
        .always(function() {
            mys.unblok();
        });
    }

    function simpan_target() {
        mys.blok()
        $.ajax({
            url: mys.base_url + 'settings/save_target',
            type: 'POST',
            dataType: 'JSON',
            data: {
                tahun_target : $('#tahun_target').val(),
                nilai_target : mys.reverse_format_ribuan($('#nilai_target').val()),
                nilai_target_one_time : mys.reverse_format_ribuan($('#nilai_target_one_time').val()),
                nilai_target_recurring : mys.reverse_format_ribuan($('#nilai_target_recurring').val()),
            },
            success: function(data) {
                if (data.status) {
                    mys.notifikasi("Data Berhasil Disimpan", "success");
                    load_target();
                    reload();
                } else {
                    mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.", "error");
                }
            },
            error: function(data) {
                mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.", "error");

            }
        })
        .always(function() {
            mys.unblok();
        });
    }


</script>
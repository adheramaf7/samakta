<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h5>Jabatan</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url() ?>"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">Lain-lain</li>
                    <li class="breadcrumb-item active" aria-current="page">Seeder</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" id="form_card">
            <div class="card-header"><h3>Form Seeder Data</h3></div>
            <div class="card-body">
                <form class="forms-sample" id="form" method="POST" action="javascript:;">
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="val_tabel">Tabel</label>
                                <select name="val_tabel" id="val_tabel" class="form-control cmb_select2" required="required">
                                    <option ></option>
                                    <option value="S">Sales</option>
                                    <option value="C">Customer</option>
                                    <option value="V">Vendor</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="val_jumlah">Jumlah Data</label>
                                <input type="text" class="form-control" name="val_jumlah" id="val_jumlah" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label for="btnSimpan">&nbsp;</label>
                                <button id="btnSimpan" type="submit" class="btn btn-primary btn-block"><span class="ik ik-send"></span>&nbsp;&nbsp;&nbsp;Seed Data</button>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label for="btnReset">&nbsp;</label>
                                <button id="btnReset" type="button" class="btn btn-warning btn-block"><span class="ik ik-refresh-cw"></span>&nbsp;&nbsp;&nbsp;Reset Form</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4" id="alert_seeder">
                            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var mys;
    var form_validator;

    $(document).ready(function() {
        mys = Object.create(myscript_js);
        mys.init('<?= base_url() ?>');

        form_validator = $('#form').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent("div").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            },
            rules:{
                val_jumlah : {
                    min : 1,
                    digits: true
                }
            }
        });
        
        $("#form").submit(function(event) {
            if (form_validator.form()) {
                simpan();
            }
        });
    });

    function simpan(){
        mys.blok()
        $.ajax({
            url: mys.base_url+'seeder/save',
            type: 'POST',
            dataType: 'JSON',
            data: $('#form').serialize(),
            success: function(data){
                $('#alert_seeder').empty();
                var html_alert = '<div class="alert alert-info" role="alert">\
                    <strong>Hasil Seeder</strong> <p>\
                    <ul>\
                        <li><strong>Berhasil: </strong> '+data.sukses+' baris</li>\
                        <li><strong>Gagal: </strong> '+data.gagal+' baris</li>\
                    </ul>\
                    </p>\
                </div>';
                $('#alert_seeder').html(html_alert);
                $('#alert_seeder').fadeIn('slow');
                setTimeout(function(){
                    $('#alert_seeder').fadeOut('slow')
                }, 5000);
            },
            error:function(data){
                mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.","error");

            }
        })
        .always(function() {
            mys.unblok();
            reset_form();
        });
    }

    function reset_form() {
        form_validator.resetForm();
        $('#form')[0].reset();
        $('#form').find('input[type="hidden"]').val('');
        $('#form').find('label,select,input,textarea').removeClass('is-invalid text-red');
        $('#form').find('.cmb_select2').val('').trigger('change');
    }

</script>
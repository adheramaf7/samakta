<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h5>Profil</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url() ?>"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Profil</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 col-md-5">
        <div class="card">
            <div class="card-body">
            <form id="form_foto" action="javascript:;" method="POST">
                <div class="text-center"> 
                    <img src="<?= base_url() ?>assets/img/avatar.png" class="img-thumbnail" width="200" id="foto_preview">
                    <h4 class="card-title mt-20">Foto Profil</h4>
                </div>
                <hr>
                <div class="row mb-10" id="section_hapus_foto">
                    
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <input type="file" id="upload_foto" name="upload_foto" class="file-upload-default" accept=".png, .jpg, .jpeg">
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled="" placeholder="File Foto">
                                <span class="input-group-append">
                                <button class="file-upload-browse btn btn-primary" type="button">Pilih File</button>
                                </span>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <span id="error_upload_foto"></span>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit" id="btnGantiFoto" disabled>Perbarui Foto</button>
            </form>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-7">
        <div class="card">
            <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" id="pills-data-profil-tab" data-toggle="pill" href="#data_profil" role="tab" aria-controls="pills-data-profil" aria-selected="false">Data Profil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-data-password-tab" data-toggle="pill" href="#ubah_password" role="tab" aria-controls="pills-data-password" aria-selected="false">Ubah Password</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade" id="data_profil" role="tabpanel" aria-labelledby="pills-data-profil-tab">
                    <div class="card-body">
                        <form class="form-horizontal" id="form_profil" action="javascript:;" method="POST">
                            <div class="form-group">
                                <label for="username_admin">Username</label>
                                <input type="text" class="form-control" name="username_admin" id="username_admin" readonly> 
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="nama_admin">Nama Lengkap</label>
                                <input type="text" class="form-control" name="nama_admin" id="nama_admin" required>
                                <span class="help-block"></span>
                            </div>
                            <button class="btn btn-primary" type="submit">Perbarui Profil</button>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade" id="ubah_password" role="tabpanel" aria-labelledby="pills-data-password-tab">
                    <div class="card-body">
                        <form class="form-horizontal" id="form_password" action="javascript:;" method="POST">
                            <div class="form-group">
                                <label for="password_lama">Password Lama</label>
                                <input type="password" value="" class="form-control" name="password_lama" id="password_lama" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="password_baru">Password Baru</label>
                                <input type="password" value="" class="form-control" name="password_baru" id="password_baru" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="re_password_baru">Ulangi Password Baru</label>
                                <input type="password" value="" class="form-control" name="re_password_baru" id="re_password_baru" required>
                                <span class="help-block"></span>
                            </div>
                            <div id="alert_form_password">
                                
                            </div>
                            <button class="btn btn-primary" type="submit">Ganti Password</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var mys;
    var form_validator_profil;
    var form_validator_password;

    $(document).ready(function() {
        mys = Object.create(myscript_js);
        mys.init('<?= base_url() ?>');

        $('#upload_foto').change(function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('foto_preview');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        });

        form_validator_profil = $('#form_profil').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent("div").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            }
        });


        form_validator_password = $('#form_password').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent("div").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            },
            rules: {
                password_lama : {
                    required : true,
                },
                password_baru : {
                    required : true,
                    minlength : 8,
                },
                re_password_baru : {
                    required : true,
                    equalTo : "#password_baru",
                    minlength : 8,
                }
            }
        });

        $('#form_foto').on('submit', function(event) {
            update_foto();
        });

        $("#form_profil").submit(function(event) {
            if (form_validator_profil.form()) {
                update_profil();
            }
        });

        $('#upload_foto').on('change', function(event) {
            $('#btnGantiFoto').prop('disabled',false);
        });

        $("#form_password").submit(function(event) {
            if (form_validator_password.form()) {
                ganti_password();
            }
        });

        load_profil();

    });

    function reset_all(argument) {
        form_validator_profil.resetForm();
        $('#form_profil')[0].reset();
        $('#form_profil').find('label,select,input,textarea').removeClass('is-invalid text-red');
        $('#form_profil').find('label,select,input,textarea').removeAttr('disabled');
        $('#form_profil').find('label,select,input,textarea').removeAttr('readonly');

        form_validator_password.resetForm();
        $('#form_password')[0].reset();
        $('#form_password').find('label,select,input,textarea').removeClass('is-invalid text-red');
        $('#form_password').find('label,select,input,textarea').removeAttr('disabled');
        $('#form_password').find('label,select,input,textarea').removeAttr('readonly');

        $('#form_foto')[0].reset();

        $('#pills-data-profil-tab').click();
    }

    function load_profil() {
        reset_all();
        mys.blok();
        $.ajax({
            url: mys.base_url+'profil/load_profil',
            type: 'POST',
            dataType: 'JSON',
            success:function(data){
                $('#username_admin').val(data.username).attr('readonly', true);
                $('#nama_admin').val(data.nama_admin);
                if (data.path_foto_admin) {
                    $('#section_hapus_foto').html('<div class="col-12" >\
                                        <div class="form-check ">\
                                            <label class="custom-control custom-checkbox">\
                                                <input type="checkbox" class="custom-control-input" name="hapus_foto" id="hapus_foto" onclick="status_tombol_foto($(this))">\
                                                <span class="custom-control-label">&nbsp; Centang untuk Menghapus Foto</span>\
                                            </label>\
                                        </div>\
                                    </div>');
                    $('#foto_preview').prop('src', mys.base_url+'assets/upload/admin/foto/'+data.id_admin+'/'+data.path_foto_admin);
                } else{
                    $('#section_hapus_foto').empty();
                    $('#foto_preview').prop('src', mys.base_url+'assets/img/avatar.png');
                }
            },
            error:function(data){
                mys.notifikasi("Gagal Mengambil data dari server", "error");
            },
        })
        .always(function() {
            mys.unblok();
        });
    }

    function status_tombol_foto(ini) {
        if (ini.is(':checked')) {
            $('#btnGantiFoto').prop('disabled',false);
        }else{
            $('#btnGantiFoto').prop('disabled',true);
        }
    }

    function update_foto() {
        mys.blok()
        var formData = new FormData($('#form_foto')[0]);
        $.ajax({
            url: mys.base_url + 'profil/update_foto',
            type: 'POST',
            dataType: 'JSON',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                if (data.status) {
                    mys.notifikasi("Data Berhasil Disimpan", "success");
                    load_profil();
                } else {
                    mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.", "error");
                    $('#error_upload_foto').empty();
                    $('#error_upload_foto').fadeIn('slow');
                    $('#error_upload_foto').html('<div class="alert alert-danger" role="alert">'+data.pesan+'</div>');
                    setTimeout( ()=> $('#error_upload_foto').fadeOut('slow'), 5000);
                }
            },
            error: function(data) {
                mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.", "error");
            },
        })
        .always(function() {
            mys.unblok();
        });
    }

    function update_profil() {
        mys.blok()
        var formData = new FormData($('#form_profil')[0]);
        $.ajax({
            url: mys.base_url + 'profil/update_profil',
            type: 'POST',
            dataType: 'JSON',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                if (data.status) {
                    mys.notifikasi("Data Berhasil Disimpan", "success");
                    load_profil();
                } else {
                    mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.", "error");
                }
            },
            error: function(data) {
                mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.", "error");
            },
        })
        .always(function() {
            mys.unblok();
        });
    }

    function ganti_password() {
        mys.blok()
        var formData = new FormData($('#form_password')[0]);
        $.ajax({
            url: mys.base_url + 'profil/ganti_password',
            type: 'POST',
            dataType: 'JSON',
            contentType: false,
            processData: false,
            data: formData,
            success: function(data) {
                if (data.status) {
                    mys.notifikasi("Data Berhasil Disimpan", "success");
                    load_profil();
                } else {
                    mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.", "error");

                    if (data.pesan) {
                        $('#alert_form_password').empty();
                        $('#alert_form_password').html('<div class="alert alert-danger" role="alert">\
                                        <strong>Error!</strong> '+data.pesan+'\
                                    </div>');
                        $('#alert_form_password').fadeIn('slow');
                        setTimeout(function(){
                            $('#alert_form_password').fadeOut('slow');
                        }, 5000);
                    }
                    
                }
            },
            error: function(data) {
                mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.", "error");
            },
        })
        .always(function() {
            mys.unblok();
        });
    }
</script>
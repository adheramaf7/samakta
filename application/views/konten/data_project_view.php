<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h5>Data Project</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url() ?>"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">Data Master</li>
                    <li class="breadcrumb-item" aria-current="page">Project</li>
                    <li class="breadcrumb-item active" aria-current="page">Data Project</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card" id="tabel_card" style="display: block;">
            <div class="card-header d-block">
                <h6 style="font-weight: bold;">Filter Berdasarkan:</h6>
                <div class="row">
                    <div class="col-md-4">
                        <label for="fl_bulan">Bulan</label>
                        <select name="fl_bulan" id="fl_bulan" class="form-control cmb_select2" required="required">
                            <option value="A">Semua Bulan</option>
                            <?php $now=date('m'); foreach (list_bulan() as $key => $value): ?>
                                <option value="<?= $value['id'] ?>" <?= $now==$value['id']? 'selected' : '' ?>><?= $value['name'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="fl_tahun">Tahun</label>
                        <input type="text" name="fl_tahun" id="fl_tahun" class="form-control tahun" data-target="#fl_tahun" value="<?= date('Y') ?>">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row clearfix">
                    <div class="col-lg-2">
                    <?php if ($ha['insert']): ?>
                        <button id="btnAdd" class="btn btn-primary btn-block">(+) Data</button>
                    <?php endif ?>
                    </div>
                    <div class="col-lg-1" style="text-align:right;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-9">
                        <input type="text" id="input_pencarian" class="form-control pull-right" placeholder="ketik disini untuk mencari ...">
                    </div>
                </div>
                <div style="padding: 1%">
                    <table id="tabel" class="table table-inverse table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Kode</th>
                                <th>Nama Project</th>
                                <th>Nama Customer</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Status Project</th>
                                <th>Vendor</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- <div class="row clearfix">
                    <div class="col-lg-6">
                        <span style="font-weight: bold">Export Data: </span>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" title="Export to PDF" class="btn btn-danger" id="exportPDF">PDF</button>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <div class="card" id="form_card" style="display: none">
            <div class="card-header">
                <h3 class="col-6 float-left">Form</h3>
            </div>
            <div class="card-body">
                <form class="forms-sample" id="form" method="POST" action="javascript:;">
                    <input type="hidden" name="id_project" id="id_project">
                    <div class="row">
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <div class="form-group" style="display: none">
                                <label for="kode_project">Kode Project</label>
                                <input type="text" class="form-control" name="kode_project" id="kode_project" readonly>
                                <span class="help-block"></span>
                            </div>

                            <div class="form-group">
                                <label for="project_year">Tahun</label>
                                <input type="text" class="form-control tahun" data-target="#project_year" name="project_year" id="project_year" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="nama_project">Nama Project</label>
                                <input type="text" class="form-control" name="nama_project" id="nama_project" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="id_product">Product</label>
                                <select name="id_product" id="id_product" class="form-control cmb_select2" required="required">
                                    <option ></option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="start_end_date">Tanggal Pengerjaan</label>
                                        <input type="text" class="form-control" name="start_end_date" id="start_end_date" readonly>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="durasi_pengerjaan">Durasi</label>
                                        <input type="text" class="form-control" name="durasi_pengerjaan" id="durasi_pengerjaan" readonly>
                                        <span class="help-block"></span>
                                    </div>                           
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="project_type">Tipe Project</label>
                                <select name="project_type" id="project_type" class="form-control cmb_select2" required="required">
                                    <option></option>
                                    <option value="TL">Tunjuk Langsung</option>
                                    <option value="TE">Tender</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="tipe_kontrak_project">Tipe Kontrak Project</label>
                                <select name="tipe_kontrak_project" id="tipe_kontrak_project" class="form-control cmb_select2" required="required">
                                    <option></option>
                                    <option value="O">One Time</option>
                                    <option value="R">Recurring</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="tgl_fs_approved">Tanggal FS Approved</label>
                                <input type="text" class="form-control tgl" data-target="#tgl_fs_approved" name="tgl_fs_approved" id="tgl_fs_approved">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="tgl_project_final">Tanggal Project Final</label>
                                <input type="text" class="form-control tgl" data-target="#tgl_project_final" name="tgl_project_final" id="tgl_project_final">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="text-center"> 
                                <img src="<?= base_url() ?>assets/img/avatar.png" class="img-thumbnail" width="200" id="foto_preview">
                                <h4 class="card-title mt-20">Foto Customer</h4>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-4">
                                    <h6 class="font-weight-bold" >Barang</h6>
                                </div>
                                <div class="col-8">
                                <div class="form-check float-right">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="cb_project_barang" id="cb_project_barang">
                                        <span class="custom-control-label">&nbsp; Centang Apabila Diperlukan</span>
                                    </label>
                                </div>
                            </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="nilai_project_barang">Nilai Project Barang</label>
                                <input type="text" class="form-control project_barang autonumeric" name="nilai_project_barang" id="nilai_project_barang" required disabled>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="start_end_project_barang">Tanggal Pengerjaan</label>
                                <input type="text" class="form-control  project_barang tgl_range" name="start_end_project_barang" id="start_end_project_barang" disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-4">
                                    <h6 class="font-weight-bold" >Jasa</h6>
                                </div>
                                <div class="col-8">
                                <div class="form-check float-right">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="cb_project_jasa" id="cb_project_jasa">
                                        <span class="custom-control-label">&nbsp; Centang Apabila Diperlukan</span>
                                    </label>
                                </div>
                            </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="nilai_project_jasa">Nilai Project Jasa</label>
                                <input type="text" class="form-control project_jasa autonumeric" name="nilai_project_jasa" id="nilai_project_jasa" required disabled>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="start_end_project_jasa">Tanggal Pengerjaan</label>
                                <input type="text" class="form-control  project_jasa tgl_range" name="start_end_project_jasa" id="start_end_project_jasa" disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-4">
                                    <h6 class="font-weight-bold" >FS Barang</h6>
                                </div>
                                <div class="col-8">
                                <div class="form-check float-right">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="cb_fs_barang" id="cb_fs_barang">
                                        <span class="custom-control-label">&nbsp; Centang Apabila Diperlukan</span>
                                    </label>
                                </div>
                            </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="no_fs_barang">Nomor FS Barang</label>
                                <input type="text" class="form-control fs_barang" name="no_fs_barang" id="no_fs_barang" required disabled>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="nilai_fs_barang">Nilai FS Barang (Rp)</label>
                                <input type="text" class="form-control  fs_barang autonumeric" name="nilai_fs_barang" id="nilai_fs_barang" required disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="row">
                                <div class="col-4">
                                    <h6 class="font-weight-bold" >FS Jasa</h6>
                                </div>
                                <div class="col-8">
                                <div class="form-check float-right">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="cb_fs_jasa" id="cb_fs_jasa">
                                        <span class="custom-control-label">&nbsp; Centang Apabila Diperlukan</span>
                                    </label>
                                </div>
                            </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="no_fs_jasa">Nomor FS Jasa</label>
                                <input type="text" class="form-control fs_jasa" name="no_fs_jasa" id="no_fs_jasa" required disabled>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="nilai_fs_jasa">Nilai FS Jasa (Rp)</label>
                                <input type="text" class="form-control autonumeric fs_jasa" name="nilai_fs_jasa" id="nilai_fs_jasa" required disabled>
                                <span class="help-block"></span>
                            </div>
                            
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <h6 class="font-weight-bold">Customer</h6>
                            <hr>
                            <div class="form-group">
                                <label for="id_customer">Nama Customer</label>
                                <select name="id_customer" id="id_customer" class="form-control cmb_select2" required="required">
                                    <option ></option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="telp_customer">No HP</label>
                                <input type="text" class="form-control" name="telp_customer" id="telp_customer" readonly>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="email_customer">Email</label>
                                <input type="text" class="form-control" name="email_customer" id="email_customer" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <h6 class="font-weight-bold">PIC Project</h6>
                            <hr>
                            <div class="form-group">
                                <label for="nama_pic_project">Nama PIC Project</label>
                                <input type="text" class="form-control" name="nama_pic_project" id="nama_pic_project">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="no_hp_pic_project">No HP PIC Project</label>
                                <input type="text" class="form-control" name="no_hp_pic_project" id="no_hp_pic_project">
                                <span class="help-block"></span>
                            </div>

                            <div class="form-group">
                                <label for="email_pic_project">Email PIC Project</label>
                                <input type="email" class="form-control" name="email_pic_project" id="email_pic_project">
                                <span class="help-block"></span>
                            </div>
                            
                        </div>
                    </div>
                    <hr>
                    <h6 class="font-weight-bold">Sales</h6>
                    <hr>
                    <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label for="id_sales">Nama Sales</label>
                                <select name="id_sales" id="id_sales" class="form-control cmb_select2" required="required">
                                    <option ></option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label for="telp_sales">No HP</label>
                                <input type="text" class="form-control" name="telp_sales" id="telp_sales" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group">
                                <label for="email_sales">Email</label>
                                <input type="text" class="form-control" name="email_sales" id="email_sales" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h6 class="font-weight-bold">Daftar Vendor</h6>
                    <hr>
                    <div class="row">
                        <?php if ($ha['project_vendor']['insert']): ?>
                        <div class="col-lg-2">
                            <button type="button" id="btn_add_vendor" class="btn btn-primary btn-block">(+) Vendor</button>
                        </div>
                        <?php endif ?>
                        <div class="col-lg-1" style="text-align:right;padding-top:7px">
                            Cari :
                        </div>
                        <div class="col-lg-9">
                            <input type="text" id="input_pencarian_vendor" class="form-control pull-right" placeholder="ketik disini untuk mencari ...">
                        </div>
                    </div>
                    <div style="padding: 1%">
                        <table id="tabel_vendor" class="table table-inverse table-hover" style="width: 100%">
                            <thead>
                                <tr>
                                    <th data-width="2%">No.</th>
                                    <th data-width="16%">Nama</th>
                                    <th data-width="8%">Tipe Pembelian</th>
                                    <th data-width="8%">Tipe Vendor</th>
                                    <!-- <th data-width="10%">No Quotation</th> -->
                                    <th data-width="15%">Nilai Quotation Awal</th>
                                    <th data-width="15%">Nilai Quotation Akhir</th>
                                    <!-- <th data-width="10%">Expired Quotation</th> -->
                                    <th data-width="11%">Lampiran</th>
                                    <th data-width="5%">Status</th>
                                    <th data-width="10%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <p><i>Nb: Klik pada status untuk melihat detil keterangan</i></p>
                    </div>

                    <div class="row">
                         <div class="col-md-12" id="alert_project_form">
                         </div>
                    </div>
                    <button id="btnSimpan" type="submit" class="btn btn-primary mr-2">Simpan</button>
                    <button class="btn btn-danger" type="button" id="btnBack">Batal</button>
                </form>
            </div>
        </div>
        <div class="card" id="form_tender_card" style="display: none">
            <div class="card-header">
                <h3 class="col-6 float-left">Form Tender</h3>
            </div>
            <div class="card-body">
                <form class="forms-sample" id="form_tender" method="POST" action="javascript:;">
                    <input type="hidden" name="id_project_tender" id="id_project_tender">
                    <input type="hidden" name="tgl_pengisian_tender" id="tgl_pengisian_tender">
                    <div class="row">
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <div class="form-group" style="display: none">
                                <label for="kode_project_tender">Kode Project</label>
                                <input type="text" class="form-control" name="kode_project_tender" id="kode_project_tender" readonly>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="nama_project_tender">Nama Project</label>
                                <input type="text" class="form-control" name="nama_project_tender" id="nama_project_tender" readonly>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="nama_customer_tender">Nama Customer</label>
                                <input type="text" class="form-control" name="nama_customer_tender" id="nama_customer_tender" readonly>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="tipe_pembelian_tender">Tipe Pembelian</label>
                                <input type="text" class="form-control" name="tipe_pembelian_tender" id="tipe_pembelian_tender" readonly>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="status_project_tender">Status Project</label>
                                <select name="status_project_tender" id="status_project_tender" class="form-control cmb_select2" required="required">
                                    <option></option>
                                    <option value="P">Waiting</option>
                                    <option value="W">Win</option>
                                    <option value="L">Lose</option>
                                    <option value="C">Cancel</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="nilai_project_tender">Nilai Project</label>
                                <input type="text" class="form-control autonumeric" name="nilai_project_tender" id="nilai_project_tender" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <div class="text-center"> 
                                <img src="<?= base_url() ?>assets/img/avatar.png" class="img-thumbnail" width="200" id="foto_preview_tender">
                                <h4 class="card-title mt-20">Foto Customer</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="fs_barang_tender" class="col-md-6" style="display: none">
                            <h6 class="font-weight-bold">FS Barang</h6>
                            <hr>
                            <div class="form-group">
                                <label for="nilai_project_barang_tender">Nilai Project (Rp)</label>
                                <input type="text" class="form-control autonumeric" name="nilai_project_barang_tender" id="nilai_project_barang_tender" readonly>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="spk_fs_barang_tender">No. PO/SPKB</label>
                                <input type="text" class="form-control" name="spk_fs_barang_tender" id="spk_fs_barang_tender" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="tgl_fs_barang_tender">Tanggal Terima</label>
                                <input type="text" class="form-control tgl" data-target="#tgl_fs_barang_tender" name="tgl_fs_barang_tender" id="tgl_fs_barang_tender">
                                <span class="help-block"></span>
                            </div>
                            <hr>
                        </div>

                        <div id="fs_jasa_tender" class="col-md-6" style="display: none">
                            <h6 class="font-weight-bold">FS Jasa</h6>
                            <hr>
                            <div class="form-group">
                                <label for="nilai_project_jasa_tender">Nilai Project (Rp)</label>
                                <input type="text" class="form-control autonumeric" name="nilai_project_jasa_tender" id="nilai_project_jasa_tender" readonly>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="spk_fs_jasa_tender">No. SPK</label>
                                <input type="text" class="form-control" name="spk_fs_jasa_tender" id="spk_fs_jasa_tender" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="tgl_fs_jasa_tender">Tanggal Terima</label>
                                <input type="text" class="form-control tgl" data-target="#tgl_fs_jasa_tender" name="tgl_fs_jasa_tender" id="tgl_fs_jasa_tender">
                                <span class="help-block"></span>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <button id="btnSimpanTender" type="submit" class="btn btn-primary mr-2">Simpan</button>
                    <button class="btn btn-danger" type="button" id="btnBackTender">Batal</button>
                </form>
            </div>
        </div>
        <div class="card" id="form_tender_vendor_card" style="display: none;">
            <div class="card-header d-block">
                <h3>Form Tender Vendor</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="forms-sample" id="form_tender_vendor" method="POST" action="javascript:;">
                            <input type="hidden" name="id_project_tv" id="id_project_tv">
                            <input type="hidden" name="tgl_fs_approved_tv" id="tgl_fs_approved_tv">
                            <input type="hidden" name="tgl_project_final_tv" id="tgl_project_final_tv">
                            <div class="row">
                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <div class="form-group" style="display: none">
                                        <label for="kode_project_tv">Kode Project</label>
                                        <input type="text" class="form-control" name="kode_project_tv" id="kode_project_tv" readonly>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_project_tv">Nama Project</label>
                                        <input type="text" class="form-control" name="nama_project_tv" id="nama_project_tv" readonly>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_customer_tv">Nama Customer</label>
                                        <input type="text" class="form-control" name="nama_customer_tv" id="nama_customer_tv" readonly>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="tipe_pembelian_tv">Tipe Pembelian</label>
                                        <input type="text" class="form-control" name="tipe_pembelian_tv" id="tipe_pembelian_tv" readonly>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="status_project_tv">Status Project</label>
                                        <input type="text" class="form-control" name="status_project_tv" id="status_project_tv" readonly>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="nilai_project_tv">Nilai Project</label>
                                        <input type="text" class="form-control autonumeric" name="nilai_project_tv" id="nilai_project_tv" readonly>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="text-center"> 
                                        <img src="<?= base_url() ?>assets/img/avatar.png" class="img-thumbnail" width="200" id="foto_preview_tv">
                                        <h4 class="card-title mt-20">Foto Customer</h4>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h6 class="font-weight-bold">Data Tender Vendor</h6>
                            <hr>
                            <div class="row" >
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="nama_vendor_tv">Vendor</label>
                                        <div class="input-group">
                                            <input type="hidden" name="id_vendor_tv" id="id_vendor_tv">
                                            <input type="hidden" name="tipe_pembelian_tender_vendor_tv" id="tipe_pembelian_tender_vendor_tv">
                                            <input type="text" class="form-control" id="nama_vendor_tv" placeholder="Nama Vendor" readonly>
                                            <span class="input-group-append" role="button" id="btn_cari_vendor">
                                                <label class="input-group-text btn btn-success bg-success" role="button" style="color: white"><i class="fa fa-search"></i></label>
                                            </span>
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                            <div class="form-group">
                                                <label for="tipe_pembelian_tv_view">Tipe Pembelian</label>
                                                <input type="text" class="form-control" name="tipe_pembelian_tv_view" id="tipe_pembelian_tv_view" readonly>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                            <div class="form-group">
                                                <label for="tipe_project_vendor_tv">Tipe Vendor</label>
                                                <input type="text" class="form-control" name="tipe_project_vendor_tv" id="tipe_project_vendor_tv" readonly>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                            <div class="form-group">
                                                <label for="no_fpt_project_vendor">No FPT</label>
                                                <input type="text" class="form-control" name="no_fpt_project_vendor" id="no_fpt_project_vendor" readonly>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                            <div class="form-group">
                                                <label>Status</label>
                                                <br>
                                                <span class="badge badge-primary" id="status_tender_vendor">Status</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="vendor_commitment">Vendor Commitment</label>
                                         <input type="text" class="form-control tgl_range" name="vendor_commitment" id="vendor_commitment" required disabled>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="no_po_spk">No. PO/SPK</label>
                                        <input type="text" class="form-control" name="no_po_spk" id="no_po_spk" required disabled>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>

                            <br/>
                            <h6 class="font-weight-bold">Target Memo Penunjukan Langsung (MPL)</h6>
                            <br/>
                            <div class="row" >
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row spl_create">
                                        <label for="target_date-spl_create" class="col-sm-4 col-form-label">Target MPL</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-spl_create" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row spl_create">
                                        <label for="spl_create" class="col-sm-4 col-form-label">MPL Create</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#spl_create" name="spl_create" id="spl_create">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row spl_internal_approve">
                                        <label for="target_date-spl_internal_approve" class="col-sm-4 col-form-label">Target Internal Approve</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-spl_internal_approve" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row spl_internal_approve">
                                        <label for="spl_internal_approve" class="col-sm-4 col-form-label">MPL Internal Approve (CEO - CFO)</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#spl_internal_approve" name="spl_internal_approve" id="spl_internal_approve">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row spl_full_approve">
                                        <label for="target_date-spl_full_approve" class="col-sm-4 col-form-label">Target Full Approve</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-spl_full_approve" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row spl_full_approve">
                                        <label for="spl_full_approve" class="col-sm-4 col-form-label">MPL Full Approve</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#spl_full_approve" name="spl_full_approve" id="spl_full_approve">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <br/>
                            <h6 class="font-weight-bold">Target Negoisasi</h6>
                            <br/>
                            <div class="row" >
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row fpt_sent">
                                        <label for="target_date-fpt_sent" class="col-sm-4 col-form-label">Target FPT Send</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-fpt_sent" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row fpt_sent">
                                        <label for="fpt_sent" class="col-sm-4 col-form-label">FPT Sent</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#fpt_sent" name="fpt_sent" id="fpt_sent">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row tgl_nego">
                                        <label for="target_date-tgl_nego" class="col-sm-4 col-form-label">Target Nego</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-tgl_nego" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row tgl_nego">
                                        <label for="tgl_nego" class="col-sm-4 col-form-label">Tanggal Nego</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#tgl_nego" name="tgl_nego" id="tgl_nego">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                                
                            <br/>
                            <h6 class="font-weight-bold">Target WBS & P3/FPB</h6>
                            <br/>
                            <div class="row" >    
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row wbs_create">
                                        <label for="target_date-wbs_create" class="col-sm-4 col-form-label">Target WBS</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-wbs_create" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row wbs_create">
                                        <label for="wbs_create" class="col-sm-4 col-form-label">WBS Create</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#wbs_create" name="wbs_create" id="wbs_create">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row p3_fps_create">
                                        <label for="target_date-p3_fps_create" class="col-sm-4 col-form-label">Target P3/FPB Create</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-p3_fps_create" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row p3_fps_create">
                                        <label for="p3_fps_create" class="col-sm-4 col-form-label">P3/FPB Create</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#p3_fps_create" name="p3_fps_create" id="p3_fps_create">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row p3_fps_release">
                                        <label for="target_date-p3_fps_release" class="col-sm-4 col-form-label">Target P3/FPB Release</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-p3_fps_release" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row p3_fps_release">
                                        <label for="p3_fps_release" class="col-sm-4 col-form-label">P3/FPB Release</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#p3_fps_release" name="p3_fps_release" id="p3_fps_release">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <br/>
                            <h6 class="font-weight-bold">Target SP Release, SPK/FPS & PO/Contract</h6>
                            <br/>
                            <div class="row" >    
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row sp_release">
                                        <label for="target_date-sp_release" class="col-sm-4 col-form-label">Target SP</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-sp_release" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row sp_release">
                                        <label for="sp_release" class="col-sm-4 col-form-label">SP Release</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#sp_release" name="sp_release" id="sp_release">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row spk_fps_create">
                                        <label for="target_date-spk_fps_create" class="col-sm-4 col-form-label">Target SPK/FPS</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-spk_fps_create" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row spk_fps_create">
                                        <label for="spk_fps_create" class="col-sm-4 col-form-label">SPK/FPS Create</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#spk_fps_create" name="spk_fps_create" id="spk_fps_create">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="no_spk_fps" class="col-sm-4 col-form-label">No. SPK/FPS</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="no_spk_fps" id="no_spk_fps">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row spk_fps_approve">
                                        <label for="target_date-spk_fps_approve" class="col-sm-4 col-form-label">Target SPK/FPS</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-spk_fps_approve" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row spk_fps_approve">
                                        <label for="spk_fps_approve" class="col-sm-4 col-form-label">SPK/FPS Approve</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#spk_fps_approve" name="spk_fps_approve" id="spk_fps_approve">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row contract_po_create">
                                        <label for="target_date-contract_po_create" class="col-sm-4 col-form-label">Target Contract/PO</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-contract_po_create" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row contract_po_create">
                                        <label for="contract_po_create" class="col-sm-4 col-form-label">Contract/PO Create</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#contract_po_create" name="contract_po_create" id="contract_po_create">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="no_contract_po" class="col-sm-4 col-form-label">No. Contract/PO</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="no_contract_po" id="no_contract_po">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row contract_po_release">
                                        <label for="target_date-contract_po_release" class="col-sm-4 col-form-label">Target Contract/PO Release</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-contract_po_release" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row contract_po_release">
                                        <label for="contract_po_release" class="col-sm-4 col-form-label">Contract/PO Release</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#contract_po_release" name="contract_po_release" id="contract_po_release">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <hr>
                            <br/>
                            <h6 class="font-weight-bold">Target & Info P3/FB Cancel</h6>
                            <br/>
                            <div class="row" >    
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row p3_fpb_cancel">
                                        <label for="target_date-p3_fpb_cancel" class="col-sm-4 col-form-label">Target P3/FPB Cancel</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-p3_fpb_cancel" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row p3_fpb_cancel">
                                        <label for="p3_fpb_cancel" class="col-sm-4 col-form-label">P3/FPB Cancel</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#p3_fpb_cancel" name="p3_fpb_cancel" id="p3_fpb_cancel">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <div class="form-group row cancel_vendor">
                                        <label for="target_date-cancel_vendor" class="col-sm-4 col-form-label">Target Info Cancel Vendor</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="target_date-cancel_vendor" style="border: none; background-color: white" readonly value="-">
                                        </div>
                                    </div>                                    
                                    <div class="form-group row cancel_vendor">
                                        <label for="cancel_vendor" class="col-sm-4 col-form-label">Info Cancel Vendor</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control tgl target_tv" data-target="#cancel_vendor" name="cancel_vendor" id="cancel_vendor">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <br/>
                        <br/>

                            <button id="btnSimpanTenderVendor" type="submit" class="btn btn-primary mr-2" disabled>Simpan</button>
                            <button class="btn btn-danger" type="button" id="btnBackTenderVendor">Batal</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var mys;
    var form_validator;
    var form_validator_vendor;
    var form_validator_tender;
    var form_validator_tender_vendor;
    var tabel_vendor;
    var tabel_list_vendor;
    var data_vendor = [];
    var target_date = {};
        target_date.fs_approve = null;
        target_date.spl_create = null;
        target_date.spl_internal_approve = null;
        target_date.spl_full_approve = null;
        target_date.fpt_sent = null;
        target_date.tgl_nego = null;
        target_date.wbs_create = null;
        target_date.p3_fps_create = null;
        target_date.p3_fps_release = null;
        target_date.sp_release = null;
        target_date.spk_fps_create = null;
        target_date.spk_fps_approve = null;
        target_date.contract_po_create = null;
        target_date.contract_po_create = null;
        target_date.contract_po_release = null;
        target_date.p3_fpb_cancel = null;
        target_date.cancel_vendor = null;
    var tipe_vendor = null;


    $(document).ready(function() {
        mys = Object.create(myscript_js);
        mys.init('<?= base_url() ?>');

        load_sales();
        load_vendor();
        load_product();
        load_customer();


        $('#tabel').DataTable({
            "scrollCollapse": true,
            "sDom": "t<'row'<'col-md-4'i><'col-md-8'p>>",
            "processing": true,
            "iDisplayLength": 10,
            "scrollX":true,
            "ajax":{
                url : mys.base_url+'project/get_data?bulan='+$('fl_bulan').val()+'&tahun='+$('#fl_tahun').val(),
                type : 'GET',
            },
            "language": {
                "url": mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            "columnDefs": [
            {"visible" : false, "targets" : []},
            {
                render: function ( data, type, row ) {
                    if (type == 'sort') {
                        return data;
                    }
                    return moment(data).format('DD MMM YYYY');
                },
                targets: [4,5]
            },
            {
                render: function ( data, type, row ) {
                    return data == 'P' ? '<span class="badge badge-pill badge-secondary">Waiting</span>' : data == 'W' ? '<span class="badge badge-pill badge-success">WIN</span>' : data == 'L' ? '<span class="badge badge-pill badge-danger">LOSE</span>' : '<span class="badge badge-pill badge-danger">CANCEL</span>';
                },
                targets: [6]
            },
            {
                render: function ( data, type, row ) {
                    return '<button type="button" class="btn btn-primary tender_vendor" data-toggle="tooltip" title="Kelola Tender Vendor" <?= !$ha['tender_vendor']['open'] ? 'disabled' : '' ?> >\
                                            <span class="badge badge-light">'+data+'</span>\
                                            &nbsp;Tender Vendor\
                                        </button>'

                },
                targets: [7]
            },
            {
                "render": function ( data, type, row ) {
                   return '<?= $ha['view']? '<button type="button" title="View Data" data-toggle="tooltip" class="btn btn-primary ubah"><span class="fa fa-edit"></span></button> ' : '' ?><?= $ha['delete']? '<button type="button" title="Hapus Data" data-toggle="tooltip" class="btn btn-danger hapus"><span class="fa fa-trash"></span></button>' : '' ?>'+'<?= $ha['tender_project']['open']? ' <button type="button" title="Kelola Tender Project" data-toggle="tooltip" class="btn btn-info tender"><span class="fa fa-folder"></span></button>' : '' ?>'+' <button type="button" title="Export Data as PDF" data-toggle="tooltip" class="btn btn-warning exportpdf"><span class="fa fa-file-pdf"></span></button>';
                },
                "targets": [8]
            },
            // {"className": "dt-center", "targets": [0,3]}
            ],
            "columns": [
            {"width": "3%" },
            {"width": "10%"},
            {"width": "12%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "10%"},
            {"width": "10%"},
            {"width": "10%", "orderable": false},
            {"width": "18%", "orderable": false}
            ],
            "order" : [
            [0, "asc"],
            ],
            "fnDrawCallback" : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        tabel_vendor= $('#tabel_vendor').DataTable({
            scrollCollapse: true,
            sDom: "t<'row'<'col-md-4'i><'col-md-8'p>>",
            processing: true,
            iDisplayLength: 10,
            scrollX:true,
            language: {
                url: mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            rowsGroup: [1],
            columnDefs: [
            {visible : false, targets : []},
            {
                render: function ( data, type, row ) {
                   return data == 'B' ? 'Barang' : 'Jasa';
                },
                targets: [2]
            },
            {
                render: function ( data, type, row ) {
                   return data == 'TE' ? 'Tender' : 'Tunjuk Langsung';
                },
                targets: [3]
            },
            {
                render: function ( data, type, row ) {
                    if (type=='sort') {
                        return data;
                    } else{
                        return mys.formatMoney(data,0,',','.');
                    }
                },
                targets: [4,5]
            },
            // {
            //     render: function ( data, type, row ) {
            //         return moment(data).format('DD MMM YYYY')
            //     },
            //     targets: [6]
            // },
            {
                render: function ( data, type, row ) {
                    if (data!= null && typeof(data) == "object") {
                        return data.name;
                    }

                    return data? '<a class="btn btn-link btn-rounded" href="'+mys.base_url+'project/download_lampiran?file='+data+'&project='+row['id_project']+'" title="Download Lampiran" data-toggle="tooltip"><span class="fa fa-download"></span>&nbsp; '+data+'</a>' : '<span class="badge badge-pill badge-default">Tidak Ada Lampiran</span>';
                },
                targets: [6]
            },
            {
                render: function ( data, type, row ) {
                    return data == 'W' ? '<a href="javascript:;" role="button" class="badge badge-success mb-1 status_vendor_project" title="Klik untuk melihat Keterangan" data-toggle="tooltip">WIN</a>' : data == 'L' ? '<a href="javascript:;" role="button" class="badge badge-danger mb-1 status_vendor_project" title="Klik untuk melihat Keterangan" data-toggle="tooltip">LOSE</a>' : '<a href="javascript:;" role="button" class="badge badge-secondary mb-1 status_vendor_project" title="Klik untuk melihat Keterangan" data-toggle="tooltip">WAITING</a>';
                },
                targets: [7]
            },
            {
                render: function ( data, type, row ) {
                    var ubah  = '';
                    var hapus = '';
                    <?php if ($ha['project_vendor']['update']): ?>
                        ubah= '<button type="button" title="Ubah Data" data-toggle="tooltip" class="btn btn-primary ubah_vendor"><span class="fa fa-edit"></span></button>&nbsp;';
                    <?php endif ?>
                    <?php if ($ha['project_vendor']['delete']): ?>
                        hapus = '<button type="button" title="Hapus Data" data-toggle="tooltip" class="btn btn-danger hapus_vendor"><span class="fa fa-trash"></span></button>';
                    <?php endif ?>
                    return ubah+hapus;
                },
                targets: [8]
            },
            // {"className": "dt-center", "targets": [0,3]}
            ],
            data: data_vendor,
            columns : [
                { data : null},
                { data : "nama_vendor"},
                { data : "tipe_pembelian"},
                { data : "tipe_project_vendor"},
                // { data : "no_quotation"},
                { data : "nilai_quotation_awal"},
                { data : "nilai_quotation_akhir"},
                // { data : "expired_quotation"},
                { data : "lampiran"},
                { data : "status"},
                { data : "id_vendor", "orderable": false},
            ],

            order : [
            [1, "asc"],
            ],
            fnRowCallback : function(nRow, aData, iDisplayIndex){
                $("td:first", nRow).html((iDisplayIndex +1)+'.');
                return nRow;
            },
            fnDrawCallback : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        tabel_list_vendor= $('#tabel_list_vendor').DataTable({
            scrollCollapse: true,
            sDom: "t<'row'<'col-md-4'i><'col-md-8'p>>",
            processing: true,
            iDisplayLength: 10,
            scrollX:true,
            language: {
                url: mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            rowsGroup: [1],
            columnDefs: [
            {visible : false, targets : []},
            {
                render: function ( data, type, row ) {
                   return data == 'B' ? 'Barang' : 'Jasa';
                },
                targets: [2]
            },
            {
                render: function ( data, type, row ) {
                   return data == 'TE' ? 'Tender' : 'Tunjuk Langsung';
                },
                targets: [3]
            },
            {
                render: function ( data, type, row ) {
                    return data == 'W' ? '<span class="badge badge-success mb-1" data-toggle="tooltip">WIN</span>' : data=='L' ? '<span class="badge badge-danger mb-1" data-toggle="tooltip">LOSE</span>' :'<span class="badge badge-secondary mb-1" data-toggle="tooltip">WAITING</span>';
                },
                targets: [4]
            },
            {
                render: function ( data, type, row ) {
                    return '<button type="button" class="btn btn-primary pilih" data-toggle="tooltip" title="Pilih Vendor">Pilih</button>'
                },
                targets: [5]
            },
            // {"className": "dt-center", "targets": [0,3]}
            ],
            data: data_vendor,
            columns : [
                { data : null},
                { data : "nama_vendor"},
                { data : "tipe_pembelian"},
                { data : "tipe_project_vendor"},
                { data : "status"},
                { data : "id_vendor", "orderable": false},
            ],

            order : [
            [1, "asc"],
            ],
            fnRowCallback : function(nRow, aData, iDisplayIndex){
                $("td:first", nRow).html((iDisplayIndex +1)+'.');
                return nRow;
            },
            fnDrawCallback : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });


        form_validator = $('#form').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent("div").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            },
            rules: {
                no_hp_pic_project: {
                    required: false,
                    digits: true
                },
            }
        });

        form_validator_vendor = $('#form_vendor').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent("div").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        form_validator_tender = $('#form_tender').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent("div").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        form_validator_tender_vendor = $('#form_tender_vendor').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent("div").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
        
        $("#form").submit(function(event) {
            if (form_validator.form()) {
                if (data_vendor.length == 0) {
                    mys.swconfirm('Simpan','Data Vendor Masih Kosong. Apakah anda ingin melanjutkan simpan data project?',simpan);                    
                } else{
                    mys.swconfirm('Simpan','Apakah anda yakin untuk menyimpan data project?',simpan);
                }
            }
        });

        $("#form_vendor").submit(function(event) {
            if (form_validator_vendor.form()) {
                tambah_vendor();
            }
        });

        $("#form_tender").submit(function(event) {
            if (form_validator_tender.form()) {
                simpan_tender();
            }
        });

        $("#form_tender_vendor").submit(function(event) {
            if (form_validator_tender_vendor.form()) {
                simpan_tender_vendor();
            }
        });

        $('#btn_add_vendor').on('click', function(event) {
            reset_form_vendor();
            $('#jenis_masukan').val('new');
            $('#modal_vendor').modal('toggle');
            $('#alert_vendor_form').empty();
        });

        $('#btn_insert_vendor').on('click', function(event) {
            $('#form_vendor').submit();
        });


        $('#start_end_date').change(function(event) {
            var val = $(this).val();
            var date_arr = val.split(" s.d. ");
            var start_date = moment(date_arr[0],"DD-MM-YYYY");
            var end_date = moment(date_arr[1],"DD-MM-YYYY");
            var selisih_hari = end_date.diff(start_date,'days');
            // $('#durasi_pengerjaan').val(end_date.to(start_date,true));
            $('#durasi_pengerjaan').val(selisih_hari+' hari');
        });

        $('#start_end_project_barang').change(function(event) {
            set_start_end_date();
        });


        $('#start_end_project_jasa').change(function(event) {
            set_start_end_date();
        });

        $('#tabel tbody').on( 'click', '.ubah', function () {
            var row = $(this);
            var table = $('#tabel').DataTable();
            var data = table.row( row.parents('tr') ).data();
            ubah_data(data[8]);
        });

        $('#tabel tbody').on( 'click', '.exportpdf', function () {
            var row = $(this);
            var table = $('#tabel').DataTable();
            var data = table.row( row.parents('tr') ).data();
            var id_project =  data[8];
            console.log(id_project);
            var jendela = window.open( "", "Print", 'width=800,height=700,status=yes,toolbar=no,menubar=no, titlebar=yes,re sizable=yes,location=no,scrollbars=yes' );
            var form = "<input type='hidden' name='id_project' value='"+id_project+"'>";
            $(jendela.document.body).html('<form id="form_redirect" action="'+mys.base_url+'project/cetak_detil" method="POST">'+form+'</form>');
            $(jendela.document).find('#form_redirect').submit();
        });

        $('#tabel tbody').on( 'click', '.tender', function () {
            var row = $(this);
            var table = $('#tabel').DataTable();
            var data = table.row( row.parents('tr') ).data();
            kelola_tender(data[8]);
        });

        $('#tabel tbody').on( 'click', '.tender_vendor', function () {
            var row = $(this);
            var table = $('#tabel').DataTable();
            var data = table.row( row.parents('tr') ).data();
            kelola_tender_vendor(data[8]);
        });

        $('#tabel tbody').on( 'click', '.hapus', function () {
            var row = $(this);
            var table = $('#tabel').DataTable();
            var data = table.row( row.parents('tr') ).data();
            mys.swconfirm("Hapus","Apakah anda yakin ingin menghapus data ini?",hapus,data[8]);
        });

        $('#tabel_vendor tbody').on( 'click', '.ubah_vendor', function () {
            var row = $(this);
            var table = $('#tabel_vendor').DataTable();
            var data = table.row( row.parents('tr') ).data();
            var index = data_vendor.findIndex(x => x.id == data.id_vendor+''+data.tipe_pembelian);
            if (index != -1) {
                ubah_vendor(index);
            } else{
                mys.notifikasi('Data Tidak Ditemukan','error');
            }
        });

        $('#tabel_vendor tbody').on( 'click', '.hapus_vendor', function () {
            var row = $(this);
            var table = $('#tabel_vendor').DataTable();
            var data = table.row( row.parents('tr') ).data();
            mys.swconfirm("Hapus","Apakah anda yakin ingin menghapus data ini?",function(){
                var index = data_vendor.findIndex(x => x.id == data.id_vendor+''+data.tipe_pembelian);
                if (index != -1) {
                    data_vendor.splice(index, 1);
                    reload_tabel_vendor();
                } else{
                    mys.notifikasi('Data Tidak Ditemukan','error');
                }
            });
        });

        $('#tabel_vendor tbody').on( 'click', '.status_vendor_project', function () {
            var row = $(this);
            var table = $('#tabel_vendor').DataTable();
            var data = table.row( row.parents('tr') ).data();
            var index = data_vendor.findIndex(x => x.id == data.id_vendor+''+data.tipe_pembelian);
            if (index != -1) {
                tampil_keterangan(index);
            } else{
                mys.notifikasi('Data Tidak Ditemukan','error');
            }
        });

        $('#tabel_list_vendor tbody').on( 'click', '.pilih', function () {
            var row = $(this);
            var table = $('#tabel_list_vendor').DataTable();
            var data = table.row( row.parents('tr') ).data();
            var index = data_vendor.findIndex(x => x.id == data.id_vendor+''+data.tipe_pembelian);
            if (index != -1) {
                load_tender_vendor(index);
            } else{
                mys.notifikasi('Data Tidak Ditemukan','error');
            }
        });

        $('#input_pencarian').on('keyup', function(event) {
            var tabel = $('#tabel');
            tabel.dataTable().fnFilter($(this).val());
        });
        
        $('#input_pencarian_vendor').on('keyup', function(event) {
            tabel_vendor.search( $(this).val() ).draw();
        });

        $('#input_pencarian_list_vendor').on('keyup', function(event) {
            tabel_list_vendor.search( $(this).val() ).draw();
        });

        $('#btnAdd').on('click', function(event) {
            buka_form();
        });

        $('#btnBack').on('click', function(event) {
            tutup_form();
        });

        $('#btnBackTender').on('click', function(event) {
            tutup_form();
        });

        $('#btnBackTenderVendor').on('click', function(event) {
            tutup_form();
        });

        $('#id_customer').on('change', function(event) {
            load_detil_customer($(this).val());
        });

        $('#id_sales').on('change', function(event) {
            load_detil_sales($(this).val());
        });

        $('#cb_fs_barang').on('click', function(event) {
            if ($(this).is(':checked')) {
                $('.fs_barang').prop('disabled',false);
            } else{
                $('.fs_barang').prop('disabled',true);
                reset_form_fs('barang');
            } 
        });

        $('#cb_fs_jasa').on('click', function(event) {
            if ($(this).is(':checked')) {
                $('.fs_jasa').prop('disabled',false);
            } else{
                $('.fs_jasa').prop('disabled',true);
                reset_form_fs('jasa');
            } 
        });

        $('#cb_project_barang').on('click', function(event) {
            if ($(this).is(':checked')) {
                $('.project_barang').prop('disabled',false);
            } else{
                $('.project_barang').prop('disabled',true);
                reset_form_pr('barang');
            }
            set_start_end_date(); 
        });

        $('#cb_project_jasa').on('click', function(event) {
            if ($(this).is(':checked')) {
                $('.project_jasa').prop('disabled',false);
            } else{
                $('.project_jasa').prop('disabled',true);
                reset_form_pr('jasa');
            } 
            set_start_end_date(); 
        });

        $('.btn_status').on('click', function(event) {

            var id = $(this).prop('id');

            var pesan1 = 'Klik untuk Memilih Status';
            var pesan2 = 'Klik untuk Reset Status';

            if (id=='btnWin') {
                if ($(this).hasClass('btn-success')) {
                    $(this).removeClass('btn-success');
                    $('#btnLose').removeClass('btn-danger');
                    $('.btn_status').prop('title', pesan1);                    
                }else{
                    $(this).addClass('btn-success');
                    $('#btnLose').removeClass('btn-danger');
                    $('.btn_status').prop('title', pesan2);                    
                }
                // $(this).prop('disabled', true);
            } else{
                if ($(this).hasClass('btn-danger')) {
                    $(this).removeClass('btn-danger')                    
                    $('#btnWin').removeClass('btn-success');                    
                    $('.btn_status').prop('title', pesan1);                    
                }else{
                    $(this).addClass('btn-danger');
                    $('#btnWin').removeClass('btn-success');                    
                    $('.btn_status').prop('title', pesan2);                    
                }
            }
            
        });

        $('#btn_cari_vendor').on('click', function(event) {
            mys.blok();
            $('#modal_list_vendor').modal('show');
        });

        $('#modal_list_vendor').on('shown.bs.modal', function(event) {
            $('#tabel_list_vendor').DataTable().columns.adjust().draw();
            mys.unblok();
        });

        $('.target_tv').on('change.datetimepicker', function(event) {
            var id_input = $(this).prop('id');
            set_target_date(id_input);
            validasi_tanggal(id_input);
        });

        $('#status_project_tender').on('change', function(event) {
            if ($(this).val() != 'W') {
                disable_spk();
            } else{
                disable_spk(false);
            }
        });

        $('#fl_bulan').on('change', function(event) {
            var bulan = $('#fl_bulan').val();
            var tahun = $('#fl_tahun').val();
            $('#tabel').DataTable().ajax.url(mys.base_url + 'project/get_data?bulan=' +bulan+'&tahun='+tahun ).load();
        });

        $('#fl_tahun').on('change.datetimepicker', function(event) {
            var bulan = $('#fl_bulan').val();
            var tahun = $('#fl_tahun').val();
            $('#tabel').DataTable().ajax.url(mys.base_url + 'project/get_data?bulan=' +bulan+'&tahun='+tahun ).load();
        });
    });

    function disable_spk(status = true) {
        $('#fs_barang_tender').find('input').prop('disabled', status);
        $('#fs_jasa_tender').find('input').prop('disabled', status);
    }

    function set_start_end_date() {
        var list_tanggal_pengerjaan = [];
        
        if ($('#start_end_project_barang').val()) {
            var date_arr = $('#start_end_project_barang').val().split(' s.d. ');
            list_tanggal_pengerjaan.push(date_arr[0]);
            list_tanggal_pengerjaan.push(date_arr[1]);
        }

        if ($('#start_end_project_jasa').val()) {
            var date_arr = $('#start_end_project_jasa').val().split(' s.d. ');
            list_tanggal_pengerjaan.push(date_arr[0]);
            list_tanggal_pengerjaan.push(date_arr[1]);
        }
        
        if (list_tanggal_pengerjaan.length > 0) {
            let moments = list_tanggal_pengerjaan.map(d => moment(d,'DD-MM-YYYY'));
            let maxDate = moment.max(moments);
            let minDate = moment.min(moments);
            $('#start_end_date').val(minDate.format('DD-MM-YYYY')+' s.d. '+maxDate.format('DD-MM-YYYY')).trigger('change');
        }
    }


    function buka_form() {
        reset_form();
        $('#tabel_card').hide();
        $('#form_tender_card').hide();
        $('#form_tender_vendor_card').hide();
        $('#form_card').show();
        // $('#status_project').val('P').trigger('change');
        reload_tabel_vendor();
        $('#tabel_vendor').DataTable().columns.adjust().draw();
    }

    function buka_form_tender() {
        reset_form_tender();
        $('#tabel_card').hide();
        $('#form_card').hide();
        $('#form_tender_vendor_card').hide();
        $('#form_tender_card').show();
    }

    function buka_form_tender_vendor() {
        reset_form_tender_vendor();
        $('#tabel_card').hide();
        $('#form_card').hide();
        $('#form_tender_card').hide();
        $('#form_tender_vendor_card').show();
    }


    function ubah_data(id){
        mys.blok()
        $.ajax({
            url: mys.base_url+'project/get_data_by_id',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id
            },
            success: function(data){
                buka_form();
                
                var project = data.project;
                    $('#id_project').val(project.id_project);
                    $('#kode_project').val(project.kode_project);
                    $('#kode_project').parents('.form-group').show();
                    $('#nama_project').val(project.nama_project);
                    $('#project_year').val(project.project_year);
                    $('#id_product').val(project.id_product).trigger('change');
                    $('#project_type').val(project.project_type).trigger('change');
                    $('#tipe_kontrak_project').val(project.tipe_kontrak_project).trigger('change');
                    $('#start_end_date').val(mys.toDate(project.project_start_date)+" s.d. "+mys.toDate(project.project_end_date)).trigger('change');
                    $('#nilai_project_barang').val(project.nilai_project_barang).blur();
                    $('#start_end_project_barang').data('daterangepicker').setStartDate(mys.toDate(project.start_date_barang));
                    $('#start_end_project_barang').data('daterangepicker').setEndDate(mys.toDate(project.end_date_barang));
                    if (project.nilai_project_barang) {
                        $('#cb_project_barang').click();
                    }
                    $('#nilai_project_jasa').val(project.nilai_project_jasa).blur();
                    $('#start_end_project_jasa').data('daterangepicker').setStartDate(mys.toDate(project.start_date_jasa));
                    $('#start_end_project_jasa').data('daterangepicker').setEndDate(mys.toDate(project.end_date_jasa));
                    if (project.nilai_project_jasa) {
                        $('#cb_project_jasa').click();
                    }
                    $('#no_fs_barang').val(project.no_fs_barang);
                    $('#nilai_fs_barang').val(project.nilai_fs_barang).blur();
                    if (project.no_fs_barang) {
                        $('#cb_fs_barang').click();
                    }
                    $('#no_fs_jasa').val(project.no_fs_jasa);
                    $('#nilai_fs_jasa').val(project.nilai_fs_jasa).blur();
                    if (project.no_fs_jasa) {
                        $('#cb_fs_jasa').click();
                    }
                    $('#id_customer').val(project.id_customer).trigger('change');
                    $('#nama_pic_project').val(project.nama_pic_project);
                    $('#no_hp_pic_project').val(project.no_hp_pic_project);
                    $('#email_pic_project').val(project.email_pic_project);
                    $('#id_sales').val(project.id_sales).trigger('change');
                    $('#tgl_fs_approved').val(mys.toDate(project.tgl_fs_approved));
                    $('#tgl_project_final').val(mys.toDate(project.tgl_project_final));

                var vendor = data.vendor;
                    data_vendor = vendor;
                    reload_tabel_vendor();
                <?= !$ha['update'] ? '$("#btnSimpan").prop("disabled",true);' : '' ?>
                <?= !$ha['update'] ? '$("#form").find("select,input,textarea").prop("disabled",true);' : '' ?>
                    $('#input_pencarian_vendor').prop('disabled',false);
            },
            error:function(data){
                mys.notifikasi("Gagal Mengambil data dari server","error");
            }
        })
        .always(function() {
            mys.unblok();
        });
    }

    function kelola_tender(id){
        mys.blok()
        $.ajax({
            url: mys.base_url+'project/get_data_tender',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id
            },
            success: function(data){
                buka_form_tender();
                
                $('#id_project_tender').val(data.id_project);
                $('#kode_project_tender').val(data.kode_project);
                $('#kode_project_tender').parents('.form-group').show();
                $('#nama_project_tender').val(data.nama_project);
                $('#nama_customer_tender').val(data.nama_customer);
                $('#nilai_project_tender').val(data.nilai_project).blur();
                $('#tipe_pembelian_tender').val(data.tipe_pembelian == 'B'? 'Barang' : data.tipe_pembelian == 'J'? 'Jasa' : 'Barang & Jasa');
                $('#status_project_tender').val(data.status_project).trigger('change');

                if($('#fs_barang_tender').hasClass('col-md-12')){
                    $('#fs_barang_tender').removeClass('col-md-12');
                }
                if($('#fs_jasa_tender').hasClass('col-md-12')){
                    $('#fs_jasa_tender').removeClass('col-md-12');
                }
                if (data.tipe_pembelian != 'BJ') {
                    if (data.tipe_pembelian == 'B') {
                        $('#fs_barang_tender').removeClass('col-md-6');
                        $('#fs_barang_tender').addClass('col-md-12');
                    }

                    if (data.tipe_pembelian == 'J') {
                        $('#fs_jasa_tender').removeClass('col-md-6');
                        $('#fs_jasa_tender').addClass('col-md-12');
                    }
                }else{
                    $('#fs_barang_tender').addClass('col-md-6   ');
                    $('#fs_jasa_tender').addClass('col-md-6 ');
                }
                if (data.tipe_pembelian == 'B' || data.tipe_pembelian == 'BJ') {
                    $('#fs_barang_tender').show();
                    $('#nilai_project_barang_tender').val(data.nilai_project_barang).blur();
                    $('#spk_fs_barang_tender').val(data.spk_fs_barang);
                    $('#tgl_fs_barang_tender').val(data.tgl_fs_barang != null? mys.toDate(data.tgl_fs_barang) : null);

                }
                if (data.tipe_pembelian == 'J' || data.tipe_pembelian == 'BJ') {
                    $('#fs_jasa_tender').show();
                    $('#nilai_project_jasa_tender').val(data.nilai_project_jasa).blur();
                    $('#spk_fs_jasa_tender').val(data.spk_fs_jasa);
                    $('#tgl_fs_jasa_tender').val(data.tgl_fs_jasa != null? mys.toDate(data.tgl_fs_jasa) : null);
                }


                if (data.path_foto_customer){
                    $('#foto_preview_tender').prop('src', mys.base_url+'assets/upload/customer/foto/'+id_customer+'/'+data.path_foto_customer);
                } else{
                    $('#foto_preview_tender').prop('src', mys.base_url+'assets/img/avatar.png');
                }

                <?= !$ha['tender_project']['update'] ? '$("#btnSimpanTender").prop("disabled",true);' : '' ?>
                <?= !$ha['tender_project']['update'] ? '$("#form_tender").find("select,input,textarea").prop("disabled",true);' : '' ?>

            },
            error:function(data){
                mys.notifikasi("Gagal Mengambil data dari server","error");
            }
        })
        .always(function() {
            mys.unblok();
        });
    }

    function kelola_tender_vendor(id){
        mys.blok()
        $.ajax({
            url: mys.base_url+'project/get_data_tender_vendor',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id
            },
            success: function(data){
                buka_form_tender_vendor();
                var project = data.project;
                $('#id_project_tv').val(project.id_project);
                $('#kode_project_tv').val(project.kode_project);
                $('#kode_project_tv').parents('.form-group').show();
                $('#nama_project_tv').val(project.nama_project);
                $('#nama_customer_tv').val(project.nama_customer);
                $('#tgl_fs_approved_tv').val(project.tgl_fs_approved);
                $('#tgl_project_final_tv').val(project.tgl_project_final);
                $('#nilai_project_tv').val(mys.formatMoney(project.nilai_project,0,',','.'));
                $('#tipe_pembelian_tv').val(project.tipe_pembelian == 'B'? 'Barang' : project.tipe_pembelian == 'J'? 'Jasa' : 'Barang & Jasa');
                $('#status_project_tv').val(project.status_project == 'P' ? 'Waiting' : project.status_project == 'W' ? 'Win' : 'Lose');

                data_vendor = data.vendor;
                reload_tabel_list_vendor();

            },
            error:function(data){
                mys.notifikasi("Gagal Mengambil data dari server","error");
            }
        })
        .always(function() {
            mys.unblok();
        });
    }

    function load_tender_vendor(index){
        var data = data_vendor[index];
        mys.blok()
        $.ajax({
            url: mys.base_url+'project/load_tender_vendor',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id_project: data.id_project,
                id_vendor: data.id_vendor,
                tipe_pembelian: data.tipe_pembelian,
            },
            success: function(data_received){
                $('.target_tv').val(null);
                $('.target_tv').removeClass('form-control-danger');
                $('.target_tv').removeClass('form-control-success');
                reset_target_date();

                var tender_vendor = data_received.tender_vendor;
                var tender_vendor_tgl = data_received.tender_vendor_tgl;
                $('#id_vendor_tv').val(data.id_vendor);
                $('#nama_vendor_tv').val(data.nama_vendor);
                $('#vendor_commitment').prop('disabled', false);
                $('#no_po_spk').prop('disabled', false);
                if(tender_vendor.v_commitment_start)
                    $('#vendor_commitment').data('daterangepicker').setStartDate(mys.toDate(tender_vendor.v_commitment_start));
                if(tender_vendor.v_commitment_finish)
                    $('#vendor_commitment').data('daterangepicker').setEndDate(mys.toDate(tender_vendor.v_commitment_finish));
                $('#no_po_spk').val(tender_vendor.no_po_spk);
                $('#no_spk_fps').val(tender_vendor.no_spk_fps);
                $('#no_contract_po').val(tender_vendor.no_contract_po);
                $('#no_fpt_project_vendor').val(tender_vendor.no_fpt);
                $('#tipe_pembelian_tender_vendor_tv').val(data.tipe_pembelian);
                $('#tipe_pembelian_tv_view').val(data.tipe_pembelian == 'B' ? 'Barang' : 'Jasa');
                $('#tipe_project_vendor_tv').val(data.tipe_project_vendor == 'TE' ? 'Tender' : 'Tunjuk Langsung');
                $('#status_tender_vendor').removeClass();
                $('#status_tender_vendor').addClass('badge');
                if (data.status == 'W') {
                    $('#status_tender_vendor').addClass('badge-success');
                    $('#status_tender_vendor').html('WIN');
                } else{
                    $('#status_tender_vendor').addClass('badge-danger');
                    $('#status_tender_vendor').html('LOSE');
                }
                $('#btnSimpanTenderVendor').prop('disabled', false);
                var final_project = moment($('#tgl_project_final_tv').val());
                target_date.fs_approve = moment($('#tgl_fs_approved_tv').val());
                target_date.spl_create = mys.addWeekdays(moment(target_date.fs_approve),1);
                target_date.wbs_create = mys.addWeekdays(moment(target_date.fs_approve),2);
                target_date.fpt_sent = mys.addWeekdays(moment(target_date.fs_approve),3);
                target_date.p3_fpb_cancel =  mys.addWeekdays(moment(final_project),1);
                target_date.cancel_vendor =  mys.addWeekdays(moment(final_project),1);
                tipe_vendor = data.tipe_project_vendor;
                Object.keys(tender_vendor_tgl).forEach((key,index) => {
                    if(tender_vendor_tgl[key]){
                        $('#'+key).val(mys.toDate(tender_vendor_tgl[key]));
                        set_target_date(key);
                        validasi_tanggal(key);
                    }
                })
                set_target_date_view_label();
                
                $('#modal_list_vendor').modal('hide');
                if (tipe_vendor == 'TE') {
                    $('#spl_create').prop('disabled',true);
                    $('#spl_internal_approve').prop('disabled',true);
                    $('#spl_full_approve').prop('disabled',true);
                }
                <?= !$ha['tender_vendor']['update'] ? '$("#btnSimpanTenderVendor").prop("disabled",true);' : '' ?>
                <?= !$ha['tender_vendor']['update'] ? '$("#form_tender_vendor").find("select,input,textarea").prop("disabled",true);' : '' ?>

            },
            error:function(data){
                mys.notifikasi("Gagal Mengambil data dari server","error");
            }
        })
        .always(function() {
            mys.unblok();
        });
        
    }
    function reset_target_date() {
        target_date.fs_approve = null;
        target_date.spl_create = null;
        target_date.spl_internal_approve = null;
        target_date.spl_full_approve = null;
        target_date.fpt_sent = null;
        target_date.tgl_nego = null;
        target_date.wbs_create = null;
        target_date.p3_fps_create = null;
        target_date.p3_fps_release = null;
        target_date.sp_release = null;
        target_date.spk_fps_create = null;
        target_date.spk_fps_approve = null;
        target_date.contract_po_create = null;
        target_date.contract_po_create = null;
        target_date.contract_po_release = null;
        target_date.p3_fpb_cancel = null;
        target_date.cancel_vendor = null;
        tipe_vendor = null;
    }

    function set_target_date_view_label(){
        Object.keys(target_date).forEach((key,index) => {
            // console.log(key, (target_date[key] ? target_date[key].format('DD-MM-YYYY') : '-'));
            $('#target_date-'+key).val( (target_date[key] ? target_date[key].format('DD-MM-YYYY') : '-') );
            if (target_date[key]) {
                $('#'+key).prop('disabled',false);
            } else{
                $('#'+key).prop('disabled',true);
            }

        })
        if (tipe_vendor == 'TE') {
            $('#spl_create').prop('disabled',true);
            $('#spl_internal_approve').prop('disabled',true);
            $('#spl_full_approve').prop('disabled',true);
        }
    }

    function set_target_date(id_input) {
        var value = moment(mys.toDate($('#'+id_input).val()));

        switch(id_input) {
            case 'spl_create':
                target_date.spl_internal_approve = mys.addWeekdays(value,2);
                target_date.spl_full_approve = mys.addWeekdays(value,6);
                break;

            case 'fpt_sent':
                target_date.tgl_nego =mys.addWeekdays(value,6);
                break;

            case 'wbs_create':
                target_date.p3_fps_create = mys.addWeekdays(value,1);
                target_date.p3_fps_release = mys.addWeekdays(value,1);
                break;

            case 'p3_fps_release':
                target_date.sp_release = mys.addWeekdays(value,2);
                break;

            case 'sp_release':
                target_date.spk_fps_create = mys.addWeekdays(value,1);
                var tipe = $('#tipe_pembelian_tender_vendor_tv').val();
                if (tipe =='B') {
                    target_date.contract_po_create = mys.addWeekdays(value,1);
                } else{
                    target_date.contract_po_create = mys.addWeekdays(value,15);
                }
                break;

            case 'spk_fps_create':
                target_date.spk_fps_approve = mys.addWeekdays(value,7);
                break;

            case 'contract_po_create':
                target_date.contract_po_release = mys.addWeekdays(value,1);
                break;

            default:
                
                break;
        }
        set_target_date_view_label();
    }

    function validasi_tanggal(id_input){
        var value = moment(mys.toDate($('#'+id_input).val()));
        // console.log(value.isAfter(target_date[id_input]));
        reset_validasi_tanggal(id_input);
        if(target_date[id_input]){
            if (value.isAfter(target_date[id_input])) {
                $('#'+id_input).addClass('form-control-danger');
            }else{
                $('#'+id_input).addClass('form-control-success');
            }
        }
        if (tipe_vendor == 'TE') {
            $('#spl_create').prop('disabled',true);
            $('#spl_internal_approve').prop('disabled',true);
            $('#spl_full_approve').prop('disabled',true);
        }

        if (id_input == 'spk_fps_create') {
            $('#no_spk_fps').prop('disabled', false);
        }

        if (id_input == 'contract_po_create') {
            $('#no_contract_po').prop('disabled', false);
        }
    }

    function reset_validasi_tanggal(id_input){
        $('#'+id_input).removeClass('form-control-success');
        $('#'+id_input).removeClass('form-control-danger');
    }

    function disable_for_tender(is_tender=false) {
        
    }

    function simpan(){
        var data_post = new FormData();

        var start_end_date = $('#start_end_date').val();
        var date_arr = start_end_date.split(" s.d. ");

        var start_end_project_barang = $('#start_end_project_barang').val();
        var date_arr_barang = start_end_project_barang.split(" s.d. ");

        var start_end_project_jasa = $('#start_end_project_jasa').val();
        var date_arr_jasa = start_end_project_jasa.split(" s.d. ");

        var tipe_pembelian = '';

        if ($('#cb_fs_barang').is(':checked')) {
            tipe_pembelian += 'B';
        }

        if ($('#cb_fs_jasa').is(':checked')) {
            tipe_pembelian += 'J';
        }

        // var menang = data_vendor.findIndex(x => x.status == 'W');
        // var vendor_pemenang = menang != '-1' ? data_vendor[menang] : null;

        var data_project = new Object();
            data_project.id_project = $('#id_project').val();
            data_project.nama_project = $('#nama_project').val();
            data_project.project_year = $('#project_year').val();
            data_project.nama_project = $('#nama_project').val();
            data_project.id_product = $('#id_product').val();
            data_project.project_start_date = mys.toDate(date_arr[0]);
            data_project.project_end_date = mys.toDate(date_arr[1]);
            data_project.project_type = $('#project_type').val();
            data_project.tipe_kontrak_project = $('#tipe_kontrak_project').val();
            data_project.no_fs_barang = $('#no_fs_barang').val()?$('#no_fs_barang').val():null;
            data_project.nilai_fs_barang = $('#nilai_fs_barang').val()?mys.reverse_format_ribuan($('#nilai_fs_barang').val()):null;
            data_project.start_date_barang = mys.toDate(date_arr_barang[0]);
            data_project.end_date_barang = mys.toDate(date_arr_barang[1]);
            data_project.nilai_project_barang = $('#nilai_project_barang').val()?mys.reverse_format_ribuan($('#nilai_project_barang').val()):null;
            data_project.no_fs_jasa = $('#no_fs_jasa').val()?$('#no_fs_jasa').val():null;
            data_project.nilai_fs_jasa = $('#nilai_fs_jasa').val()?mys.reverse_format_ribuan($('#nilai_fs_jasa').val()):null;
            data_project.nilai_project_jasa = $('#nilai_project_jasa').val()?mys.reverse_format_ribuan($('#nilai_project_jasa').val()):null;
            data_project.nilai_project = (data_project.nilai_project_barang? data_project.nilai_project_barang : 0) + (data_project.nilai_project_jasa? data_project.nilai_project_jasa : 0)
            data_project.start_date_jasa = mys.toDate(date_arr_jasa[0]);
            data_project.end_date_jasa = mys.toDate(date_arr_jasa[1]);
            data_project.id_customer = $('#id_customer').val();
            data_project.nama_pic_project = $('#nama_pic_project').val();
            data_project.no_hp_pic_project = $('#no_hp_pic_project').val();
            data_project.email_pic_project = $('#email_pic_project').val();
            data_project.id_sales = $('#id_sales').val();
            data_project.tipe_pembelian = tipe_pembelian != '' ? tipe_pembelian : null;
            data_project.tgl_fs_approved = mys.toDate($('#tgl_fs_approved').val());
            data_project.tgl_project_final = mys.toDate($('#tgl_project_final').val());

        data_post.append('data_project',JSON.stringify(data_project));

        $.each(data_vendor, function(index, val) {
            var d = {};
                d.id_vendor = val.id_vendor; 
                d.nama_vendor = val.nama_vendor; 
                d.no_quotation = val.no_quotation; 
                d.tipe_pembelian = val.tipe_pembelian; 
                d.tipe_project_vendor = val.tipe_project_vendor; 
                d.nilai_quotation_awal = val.nilai_quotation_awal; 
                d.nilai_quotation_akhir = val.nilai_quotation_akhir; 
                d.expired_quotation = val.expired_quotation; 
                d.status = val.status; 
                d.keterangan = val.keterangan; 
                d.lampiran = (val.lampiran != undefined && typeof(val.lampiran) == 'object')? 'upload' : (val.lampiran ? val.lampiran : null); 
            data_post.append('data_vendor[]',JSON.stringify(d));
            if (typeof(val.lampiran) =='object' && val.lampiran != undefined) {
                data_post.append('lampiran_vendor[]',val.lampiran);
                data_post.append('id_vendor_upload[]',val.id_vendor)
            }
        });

        if (!data_vendor) {
            data_post.append('data_vendor[]',null);
            data_post.append('lampiran_vendor[]',null);
            data_post.append('id_vendor_upload[]',null);
        }

        mys.blok()
        $.ajax({
            url: mys.base_url+'project/save',
            type: 'POST',
            dataType: 'JSON',
            data: data_post,
            contentType: false,
            processData: false,
            success: function(data){
                if (data.status && data.error.length == 0) {
                    mys.notifikasi("Data Berhasil Disimpan","success");
                    data_vendor = [];
                    tutup_form();
                } else{
                    mys.notifikasi("Terdapat Kesalahan dalam menyimpan data.","error");
                    if (data.error.length > 0) {
                        $('#alert_project_form').empty();

                        var html_error = '<ol>';
                        $.each(data.error, function(index, val) {
                            html_error +=  '<li>'+val+'</li>';
                        });
                        html_error += '<ol>';

                        $('#alert_project_form').html('<div class="alert alert-warning" role="alert">\
                            <strong>Error!</strong> <p>'+html_error+'</p>\
                        </div>');

                        $('#alert_project_form').fadeIn('slow');
                        setTimeout(function(){
                            $('#alert_project_form').fadeOut('slow');
                            $('#alert_project_form').empty();
                        }, 5000)
                    }
                }
            },
            error:function(data){
                mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.","error");

            }
        })
        .always(function() {
            mys.unblok();
            reload();
        });
    }

    function simpan_tender() {
        mys.blok()
        var data_send = {};
            data_send.id_project = $('#id_project_tender').val();
            data_send.status_project = $('#status_project_tender').val();
            data_send.tgl_pengisian_tender = $('#tgl_pengisian_tender').val();
            data_send.nilai_project = mys.reverse_format_ribuan($('#nilai_project_tender').val());
            data_send.spk_fs_barang = $('#spk_fs_barang_tender').val() !='' ? $('#spk_fs_barang_tender').val() : null;
            data_send.tgl_fs_barang = $('#tgl_fs_barang_tender').val() !='' ? mys.toDate($('#tgl_fs_barang_tender').val()) : null;
            data_send.spk_fs_jasa = $('#spk_fs_jasa_tender').val() !='' ? $('#spk_fs_jasa_tender').val() : null;
            data_send.tgl_fs_jasa = $('#tgl_fs_jasa_tender').val() !='' ? mys.toDate($('#tgl_fs_jasa_tender').val()) : null;
        $.ajax({
            url: mys.base_url+'project/save_tender',
            type: 'POST',
            dataType: 'JSON',
            data: data_send,
            success: function(data){
                if (data.status) {
                    mys.notifikasi("Data Berhasil Disimpan","success");
                    tutup_form();
                } else{
                    mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.","error");
                }
            },
            error:function(data){
                mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.","error");

            }
        })
        .always(function() {
            mys.unblok();
            reload();
        });
    }

    function simpan_tender_vendor() {
        mys.blok()
        var project_vendor = {};
            project_vendor.id_project = $('#id_project_tv').val();
            project_vendor.id_vendor = $('#id_vendor_tv').val();
            project_vendor.tipe_pembelian = $('#tipe_pembelian_tender_vendor_tv').val();

        var date_arr = $('#vendor_commitment').val().split(" s.d. ");
        var tender_vendor = {};
            tender_vendor.v_commitment_start = mys.toDate(date_arr[0]);
            tender_vendor.v_commitment_finish = mys.toDate(date_arr[1]);
            tender_vendor.no_po_spk = $('#no_po_spk').val();
            tender_vendor.spl_create = $('#spl_create').val()? mys.toDate($('#spl_create').val()) : null;
            tender_vendor.spl_internal_approve = $('#spl_internal_approve').val()? mys.toDate($('#spl_internal_approve').val()) : null;
            tender_vendor.spl_full_approve = $('#spl_full_approve').val()? mys.toDate($('#spl_full_approve').val()) : null;
            tender_vendor.fpt_sent = $('#fpt_sent').val()? mys.toDate($('#fpt_sent').val()) : null;
            tender_vendor.tgl_nego = $('#tgl_nego').val()? mys.toDate($('#tgl_nego').val()) : null;
            tender_vendor.wbs_create = $('#wbs_create').val()? mys.toDate($('#wbs_create').val()) : null;
            tender_vendor.p3_fps_create = $('#p3_fps_create').val()? mys.toDate($('#p3_fps_create').val()) : null;
            tender_vendor.p3_fps_release = $('#p3_fps_release').val()? mys.toDate($('#p3_fps_release').val()) : null;
            tender_vendor.sp_release = $('#sp_release').val()? mys.toDate($('#sp_release').val()) : null;
            tender_vendor.spk_fps_create = $('#spk_fps_create').val()? mys.toDate($('#spk_fps_create').val()) : null;
            tender_vendor.no_spk_fps = $('#no_spk_fps').val() ? $('#no_spk_fps').val() : null;
            tender_vendor.spk_fps_approve = $('#spk_fps_approve').val()? mys.toDate($('#spk_fps_approve').val()) : null;
            tender_vendor.contract_po_create = $('#contract_po_create').val()? mys.toDate($('#contract_po_create').val()) : null;
            tender_vendor.no_contract_po = $('#no_contract_po').val() ? $('#no_contract_po').val() : null;
            tender_vendor.contract_po_release = $('#contract_po_release').val()? mys.toDate($('#contract_po_release').val()) : null;
            tender_vendor.p3_fpb_cancel = $('#p3_fpb_cancel').val()? mys.toDate($('#p3_fpb_cancel').val()) : null;
            tender_vendor.cancel_vendor = $('#cancel_vendor').val()? mys.toDate($('#cancel_vendor').val()) : null;

        $.ajax({
            url: mys.base_url+'project/save_tender_vendor',
            type: 'POST',
            dataType: 'JSON',
            data: {
                project_vendor: JSON.stringify(project_vendor),
                tender_vendor: JSON.stringify(tender_vendor),
            },
            success: function(data){
                if (data.status) {
                    mys.notifikasi("Data Berhasil Disimpan","success");
                    tutup_form();
                } else{
                    mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.","error");
                }
            },
            error:function(data){
                mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.","error");

            }
        })
        .always(function() {
            mys.unblok();
            reload();
        });
    }

    function hapus(id){
        mys.blok()
        $.ajax({
            url: mys.base_url+'project/delete',
            type: 'POST',
            dataType: 'JSON',
            data: {
                id: id
            },
            success: function(data){
                if (data.status) {
                    mys.notifikasi("Data Berhasil Dihapus","success");
                } else{
                    mys.notifikasi("Data Gagal Dihapus, Coba Beberapa Saat Lagi.","error");
                }
            },
            error:function(data){
                mys.notifikasi("Data Gagal Dihapus, Coba Beberapa Saat Lagi.","error");
            }
        })
        .always(function() {
            mys.unblok();
            reload();
        });
    }

    function tutup_form() {
        $('#form_card').hide();
        $('#form_tender_card').hide();
        $('#form_tender_vendor_card').hide();
        $('#tabel_card').show();
        var t = $('#tabel').DataTable();
        t.columns.adjust().draw();
    }

    function reset_form() {
        data_vendor = [];
        form_validator.resetForm();
        $('#form')[0].reset();
        $('#form').find('input[type="hidden"]').val('');
        $('#form').find('label,select,input,textarea').removeClass('is-invalid text-red');
        $('#form').find('.cmb_select2').val('').trigger('change');
        $('#kode_project').parents('.form-group').hide();
        <?= !$ha['update'] ? '$("#btnSimpan").prop("disabled",false);' : '' ?>
        <?= !$ha['update'] ? '$("#form").find("select,input,textarea").prop("disabled",false);' : '' ?>
    }

    function reset_form_vendor(){
        form_validator_vendor.resetForm();
        $('#form_vendor')[0].reset();
        $('#form_vendor').find('input[type="hidden"]').val('');
        $('#form_vendor').find('label,select,input,textarea').removeClass('is-invalid text-red');
        $('#form_vendor').find('.cmb_select2').val('').trigger('change');
        $('#btn_pilih_lampiran').prop('disabled', false);
        $('#lampiran_vendor_project').prop('required',false);
        $('#lampiran_vendor_project').prop('disabled', false);
        $('#id_vendor').prop('disabled', false);
        $('#tipe_pembelian').prop('disabled', false);
        $('#tipe_project_vendor').prop('disabled', false);
        $('#btn_insert_vendor').html('(+) Tambahkan');
    }

    function reset_form_tender() {
        form_validator_tender.resetForm();
        $('#form_tender')[0].reset();
        $('#form_tender').find('input[type="hidden"]').val('');
        $('#form_tender').find('label,select,input,textarea').removeClass('is-invalid text-red');
        $('#form_tender').find('.cmb_select2').val('').trigger('change');
        $('#kode_project_tender').parents('.form-group').hide();
        $('#fs_barang_tender').hide();
        $('#fs_jasa_tender').hide();
    }

    function reset_form_tender_vendor() {
        form_validator_tender_vendor.resetForm();
        $('#form_tender_vendor')[0].reset();
        $('#form_tender_vendor').find('input[type="hidden"]').val('');
        $('#form_tender_vendor').find('label,select,input,textarea').removeClass('is-invalid text-red');
        $('#form_tender_vendor').find('.cmb_select2').val('').trigger('change');
        $('#kode_project_tv').parents('.form-group').hide();
        $('#nama_vendor_tv').prop('disabled', true);
        $('#vendor_commitment').prop('disabled', true);
        $('#no_po_spk').prop('disabled', true);
        $('#no_spk_fps').prop('disabled', true);
        $('#no_contract_po').prop('disabled', true);
        $('.target_tv').prop('disabled',true);
        $('.target_tv').val(null);
        $('.target_tv').removeClass('form-control-danger');
        $('.target_tv').removeClass('form-control-success');
        $('#btnSimpanTenderVendor').prop('disabled', true);
    }  

    function reset_form_fs(jenis) {
        $('.fs_'+jenis).val(null);
        $('.fs_'+jenis).parent('div').find('.help-block').empty();
        $('.fs_'+jenis).parent('div').find('label,select,input,textarea').removeClass('is-invalid text-red');
    }

    function reset_form_pr(jenis) {
        $('.project_'+jenis).val(null);
        $('.project_'+jenis).parent('div').find('.help-block').empty();
        $('.project_'+jenis).parent('div').find('label,select,input,textarea').removeClass('is-invalid text-red');
    }

    function reload() {
        var t = $('#tabel').DataTable();
        t.ajax.reload();
    }

    function load_product(){
        $.ajax({
            url: mys.base_url+'project/get_product',
            type: 'POST',
            dataType: 'JSON',
            data: null,
            success: function(data){
                $('#id_product').empty();
                $('#id_product').append('<option></option>');
                $.each(data, function(index, val) {
                    $('#id_product').append('<option value="'+val.id+'">'+ val.name+'</option>');
                });
            },
            error:function(data){
                mys.notifikasi("Gagal Ambil Data.","error");
            }
        })
    }

    function load_customer(){
        $.ajax({
            url: mys.base_url+'project/get_customer',
            type: 'POST',
            dataType: 'JSON',
            data: null,
            success: function(data){
                var select_html = ''
                $('#id_customer').empty();
                select_html += '<option></option>';
                var group = '';
                $.each(data, function(index, val) {
                    if (group != val.group) {
                        if (group != '') {
                            select_html += '</optgroup>';
                        }
                        group = val.group;
                        select_html += '<optgroup label="'+val.group+'">';
                    }
                    select_html += '<option value="'+val.id+'">'+ val.name+'</option>';
                });
                if (group!='') {
                    select_html += '</optgroup>';
                }
                $('#id_customer').html(select_html);
            },
            error:function(data){
                mys.notifikasi("Gagal Ambil Data.","error");
            }
        })
    }

    function load_sales(){
        $.ajax({
            url: mys.base_url+'project/get_sales',
            type: 'POST',
            dataType: 'JSON',
            data: null,
            success: function(data){
                $('#id_sales').empty();
                $('#id_sales').append('<option></option>');
                $.each(data, function(index, val) {
                    $('#id_sales').append('<option value="'+val.id+'">'+ val.name+'</option>');
                });
            },
            error:function(data){
                mys.notifikasi("Gagal Ambil Data.","error");
            }
        })
    }

    function load_vendor(){
        $.ajax({
            url: mys.base_url+'project/get_vendor',
            type: 'POST',
            dataType: 'JSON',
            data: null,
            success: function(data){
                $('#id_vendor').empty();
                $('#id_vendor').append('<option></option>');
                $.each(data, function(index, val) {
                    $('#id_vendor').append('<option value="'+val.id+'">'+ val.name+'</option>');
                });
            },
            error:function(data){
                mys.notifikasi("Gagal Ambil Data.","error");
            }
        })
    }

    function load_detil_customer(id_customer = null) {
        $('#telp_customer').val(null);
        $('#email_customer').val(null);
        mys.blok();
        var data_send = {};
        data_send.id = id_customer;
        $.ajax({
            url: mys.base_url+'project/get_detil_customer',
            type: 'POST',
            dataType: 'JSON',
            data: data_send,
            success: function(data){
                if (data) {
                    if (data.path_foto_customer){
                        $('#foto_preview').prop('src', mys.base_url+'assets/upload/customer/foto/'+id_customer+'/'+data.path_foto_customer);
                    } else{
                        $('#foto_preview').prop('src', mys.base_url+'assets/img/avatar.png');
                    }
                    $('#telp_customer').val(data.telp_customer);
                    $('#email_customer').val(data.email_customer);
                }
            },
            error:function(data){
                mys.notifikasi("Gagal Ambil Data.","error");
            }
        }).always(function(){
            mys.unblok();
        })
    }

    function load_detil_sales(id_sales = null) {
        $('#telp_sales').val(null);
        $('#email_sales').val(null);
        mys.blok();
        var data_send = {};
        data_send.id = id_sales;
        $.ajax({
            url: mys.base_url+'project/get_detil_sales',
            type: 'POST',
            dataType: 'JSON',
            data: data_send,
            success: function(data){
                if (data) {
                    $('#telp_sales').val(data.telp_sales);
                    $('#email_sales').val(data.email_sales);
                }
            },
            error:function(data){
                mys.notifikasi("Gagal Ambil Data.","error");
            }
        }).always(function(){
            mys.unblok();
        })
    }

    function tambah_vendor() {
        //validasi
        var jenis_masukan = $('#jenis_masukan').val();
        var id_vendor = $('#id_vendor').val();
        var tipe_pembelian = $('#tipe_pembelian').val();
        var id_project = $('#id_project').val()=="" ? null : $('#id_project').val();
        var status = $('#status_vendor_project').val();

        if (jenis_masukan == 'new') {
            //insert
            var index = data_vendor.findIndex(x => x.id== (id_vendor+''+tipe_pembelian));

            var menang = data_vendor.findIndex(x => x.status == 'W');

            if (index != '-1') {
                //jika data sudah ada
                $('#alert_vendor_form').empty();
                $('#alert_vendor_form').html('<div class="alert alert-danger" role="alert">\
                    <strong>Error!</strong> Data Vendor sudah dimasukkan...\
                </div>');
                $('#alert_vendor_form').fadeIn('slow');
                setTimeout(function(){
                    $('#alert_vendor_form').fadeOut('slow');
                    $('#alert_vendor_form').empty();
                }, 5000)
                return;
            }

            // if (menang != '-1' && status == 'W') {
            //      //jika pemenang lebih dari 1
            //     $('#alert_vendor_form').empty();
            //     $('#alert_vendor_form').html('<div class="alert alert-danger" role="alert">\
            //         <strong>Error!</strong> Vendor yang berstatus <span class="badge badge-pill badge-success">WIN</span> hanya boleh satu...\
            //     </div>');
            //     $('#alert_vendor_form').fadeIn('slow');
            //     setTimeout(function(){
            //         $('#alert_vendor_form').fadeOut('slow');
            //         $('#alert_vendor_form').empty();
            //     }, 5000)
            //     return;
            // }

            var d = {
                    "id_project" : id_project,
                    "id" : $('#id_vendor').val()+''+$('#tipe_pembelian').val(),
                    "id_vendor" : $('#id_vendor').val(),
                    "nama_vendor" : $('#id_vendor option:selected').text(),
                    "tipe_project_vendor" : $('#tipe_project_vendor').val(),
                    "tipe_pembelian" : $('#tipe_pembelian').val(),
                    "no_quotation" : $('#no_quotation').val(),
                    "nilai_quotation_awal" : parseInt(mys.reverse_format_ribuan($('#nilai_quotation_awal').val())),
                    "nilai_quotation_akhir" : parseInt(mys.reverse_format_ribuan($('#nilai_quotation_akhir').val())),
                    "expired_quotation": mys.toDate($('#expired_quotation').val()),
                    "status": $('#status_vendor_project').val(),
                    "keterangan": $('#keterangan_vendor').val(),
                    "lampiran": $('#lampiran_vendor_project').prop("files")[0],
            }
            data_vendor.push(d);

        } else{
            //update
            var index = data_vendor.findIndex(x => x.id == (id_vendor+''+tipe_pembelian));

            var menang = data_vendor.findIndex(x => x.status == 'W');
            // if (menang != '-1' && menang != index && status == 'W') {
            //      //jika pemenang lebih dari 1
            //     $('#alert_vendor_form').empty();
            //     $('#alert_vendor_form').html('<div class="alert alert-danger" role="alert">\
            //         <strong>Error!</strong> Vendor yang berstatus <span class="badge badge-pill badge-success">WIN</span> hanya boleh satu...\
            //     </div>');
            //     $('#alert_vendor_form').fadeIn('slow');
            //     setTimeout(function(){
            //         $('#alert_vendor_form').fadeOut('slow');
            //         $('#alert_vendor_form').empty();
            //     }, 5000)
            //     return;
            // }

            var d_lama = data_vendor[index];

            var d_baru = {
                "id_project" : id_project,
                "id" : d_lama.id_vendor+''+d_lama.tipe_pembelian,
                "id_vendor" : d_lama.id_vendor,
                "nama_vendor" : d_lama.nama_vendor,
                "tipe_project_vendor" : d_lama.id_project? d_lama.tipe_project_vendor : $('#tipe_project_vendor').val(),
                "tipe_pembelian" : d_lama.tipe_pembelian,
                "no_quotation" : $('#no_quotation').val(),
                "nilai_quotation_awal" : parseInt(mys.reverse_format_ribuan($('#nilai_quotation_awal').val())),
                "nilai_quotation_akhir" : parseInt(mys.reverse_format_ribuan($('#nilai_quotation_akhir').val())),
                "expired_quotation": mys.toDate($('#expired_quotation').val()),
                "status": $('#status_vendor_project').val(),
                "keterangan": $('#keterangan_vendor').val(),
                "lampiran": ($('#lampiran_vendor_project').prop("files")[0] == undefined) ? d_lama.lampiran : $('#lampiran_vendor_project').prop("files")[0],
            }

            data_vendor[index] = d_baru;
        }

        reload_tabel_vendor();
        $('#modal_vendor').modal('toggle');
        
    }

    function ubah_vendor(index) {
        reset_form_vendor();
        var data = data_vendor[index];
        $('#btn_insert_vendor').html('Simpan Perubahan')
        $('#lampiran_vendor_project').prop('required',false);
        //set data vendor on form
        $('#id_vendor').val(data.id_vendor).trigger('change').prop('disabled', true);
        $('#no_quotation').val(data.no_quotation);
        $('#tipe_pembelian').val(data.tipe_pembelian).trigger('change').prop('disabled', true);
        $('#tipe_project_vendor').val(data.tipe_project_vendor).trigger('change');
        if (data.id_project) {
            $('#tipe_project_vendor').prop('disabled',true);
        }
        $('#nilai_quotation_awal').val(data.nilai_quotation_awal).blur();
        $('#nilai_quotation_akhir').val(data.nilai_quotation_akhir).blur();
        $('#expired_quotation').val( mys.toDate(data.expired_quotation));
        $('#status_vendor_project').val( data.status).trigger('change');
        $('#keterangan_vendor').val( data.keterangan);
        var lampiran = data.lampiran;
        $('#file_lampiran_name').val((typeof(lampiran) == "object") ? lampiran.name : lampiran);
        $('#modal_vendor').modal('toggle'); 
    }

    function tampil_keterangan(index) {
        reset_modal_keterangan()
        var data = data_vendor[index];
        $('#nama_vendor_view').html(data.nama_vendor);
        $('#tipe_pembelian_view').html(data.tipe_pembelian == 'B' ? 'Barang' : 'Jasa');
        $('#label_status_view').html( (data.status == 'W' ? '<span class="badge badge-pill badge-success">WIN</span>' : data.status == 'L'? '<span class="badge badge-pill badge-danger">LOSE</span>' : '<span class="badge badge-pill badge-secondary">WAITING</span>') );
        $('#keterangan_vendor_view').html(data.keterangan);
        $('#modal_keterangan').modal('toggle');
    }

    function reset_modal_keterangan() {
        $('#nama_vendor_view').html('');
        $('#label_status_view').html('');
        $('#keterangan_vendor_view').html('');
    }

    function reload_tabel_vendor() {
        tabel_vendor.clear();
        tabel_vendor.rows.add(data_vendor);
        tabel_vendor.draw();
    }

    function reload_tabel_list_vendor() {
        tabel_list_vendor.clear();
        tabel_list_vendor.rows.add(data_vendor);
        tabel_list_vendor.draw();
    }


</script>

<div class="modal fade" id="modal_vendor" role="dialog" aria-labelledby="modal_vendorLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_vendorLabel">Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id="form_vendor" action="javascript:;" method="POST">
                    <input type="hidden" name="jenis_masukan" id="jenis_masukan">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="id_vendor">Nama Vendor</label>
                                <select name="id_vendor" id="id_vendor" class="form-control cmb_select2" required="required">
                                    <option ></option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="tipe_pembelian">Tipe Pembelian</label>
                                <select name="tipe_pembelian" id="tipe_pembelian" class="form-control cmb_select2" required="required">
                                    <option value="B">Barang</option>
                                    <option value="J">Jasa</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="tipe_project_vendor">Tipe Vendor</label>
                                <select name="tipe_project_vendor" id="tipe_project_vendor" class="form-control cmb_select2" required="required">
                                    <option value="TL">Tunjung Langsung</option>
                                    <option value="TE">Tender</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="no_quotation">No Quotation</label>
                                <input type="text" class="form-control" name="no_quotation" id="no_quotation">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="nilai_quotation_awal">Nilai Quotation Awal (Rp)</label>
                                <input type="text" class="form-control autonumeric" name="nilai_quotation_awal" id="nilai_quotation_awal" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="nilai_quotation_akhir">Nilai Quotation Akhir (Rp)</label>
                                <input type="text" class="form-control autonumeric" name="nilai_quotation_akhir" id="nilai_quotation_akhir">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="expired_quotation">Expired Quotation</label>
                                <input type="text" class="form-control tgl" data-target="#expired_quotation" name="expired_quotation" id="expired_quotation">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="status_vendor_project">Status</label>
                                <select name="status_vendor_project" id="status_vendor_project" class="form-control cmb_select2" required="required">
                                    <option value="P">Waiting</option>
                                    <option value="W">Win</option>
                                    <option value="L">Lose</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="keterangan_vendor">Keterangan</label>
                                <textarea name="keterangan_vendor" id="keterangan_vendor" class="form-control" rows="5" ></textarea>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="lampiran_vendor_project">Lampiran</label>
                                <input type="file" id="lampiran_vendor_project" name="lampiran_vendor_project" class="file-upload-default" >
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" id="file_lampiran_name" disabled="" placeholder="File Lampiran">
                                    <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" type="button" id="btn_pilih_lampiran">Pilih File</button>
                                    </span>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div id="alert_vendor_form"></div>
                </form>
            </div>
            <div class="modal-footer ">
                    <button type="button" class="btn btn-primary" id="btn_insert_vendor">(+) Tambahkan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_list_vendor" role="dialog" aria-labelledby="modal_vendorLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_vendorLabel">Daftar Vendor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-1" style="text-align:right;padding-top:7px">
                            Cari :
                        </div>
                        <div class="col-lg-11">
                            <input type="text" id="input_pencarian_list_vendor" class="form-control pull-right" placeholder="ketik disini untuk mencari ...">
                        </div>
                    </div>
                    <table id="tabel_list_vendor" class="table table-inverse table-hover" style="width: 100%">
                        <thead>
                            <tr>
                                <th data-width="5%">No.</th>
                                <th data-width="40%">Nama</th>
                                <th data-width="18%">Tipe Pembelian</th>
                                <th data-width="18%">Tipe Vendor</th>
                                <th data-width="14%">Status</th>
                                <th data-width="5%">Pilih</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="modal_keterangan" role="dialog" aria-labelledby="modal_keteranganLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_keteranganLabel">Keterangan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <table>
                    <tbody>
                        <tr>
                            <td style="width: 30%" class="font-weight-bold">Nama Vendor</td>
                            <td style="width: 5%">&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                            <td style="width: 68%" id="nama_vendor_view"></td>
                        </tr>
                        <tr>
                            <td style="width: 30%" class="font-weight-bold">Tipe</td>
                            <td style="width: 5%">&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                            <td style="width: 68%" id="tipe_pembelian_view"></td>
                        </tr>
                        <tr>
                            <td style="width: 30%" class="font-weight-bold">Status</td>
                            <td style="width: 5%">&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                            <td style="width: 68%" id="label_status_view"></td>
                        </tr>
                        <tr>
                            <td style="width: 30%" class="font-weight-bold">Keterangan</td>
                            <td style="width: 5%">&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                            <td style="width: 68%" id="keterangan_vendor_view"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
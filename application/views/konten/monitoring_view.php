<style type="text/css" media="screen">
    html {
        scroll-behavior: smooth;
    }
</style>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-block">
                <h6 style="font-weight: bold;">Tampilkan Berdasarkan:</h6>
                <div class="row">
                    <div class="col-md-4 pt-25">
                        <ul class="nav nav-pills">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">Navigasi Monitoring <i class="ik ik-chevron-down"></i></a>
                            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 35px, 0px); width: inherit;">
                                <a class="dropdown-item" href="#1">1. List Project Yang Belum Nego (FPT Sudah di-submit ke TRM)</a>
                                <a class="dropdown-item" href="#2">2. List FPT Yang Sudah Nego, Tapi Menunggu Info Status Project Dari AM/sales (Win / Lose)</a>
                                <a class="dropdown-item" href="#3">3. List Project WIN Namun TRM Belum Mengirimkan SP</a>
                                <a class="dropdown-item" href="#4">4. SPK/FPS Created Namun SPK/FPS Belum Full Approved</a>
                                <a class="dropdown-item" href="#5">5. SPK/FPS Full Approved Namun Contract/PO Belum Release</a>
                                <a class="dropdown-item" href="#6">6. List Project WIN, TRM Sudah Release SP, Namun SAMAKTA Belum Release Contract / PO</a>
                                <a class="dropdown-item" href="#7">7. List Project WIN Namun Belum Terima PO dari Customer</a>
                                <a class="dropdown-item" href="#8">8. Latest Project Status</a>
                                <a class="dropdown-item" href="#9">9. PO Monitoring</a>
                                <a class="dropdown-item" href="#10">10. SPK Monitoring</a>
                            </div>
                        </li>
                    </ul>
                    </div>
                    <div class="col-md-2">
                        <label for="fl_tahun">Tahun:</label>
                        <input type="text" name="fl_tahun" id="fl_tahun" data-target="#fl_tahun" class="form-control tahun" value="<?= date('Y') ?>">
                    </div>
                    <div class="col-md-1">
                        <label for="fl_button">&nbsp;</label>
                        <button id="fl_button" type="button" class="btn btn-primary btn-block"><span class="fa fa-search"></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" >
    <div class="col-md-12">
        <div class="card" id="1">
            <div class="card-header"><h3>1. List Project Yang Belum Nego (FPT Sudah di-submit ke TRM)</h3></div>
            <div class="card-body">
                <div class="row clearfix">
                    <div class="col-lg-2" style="text-align:right;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-10">
                        <input type="text" id="input_pencarian-1" class="form-control pull-right" placeholder="ketik disini untuk mencari ...">
                    </div>
                </div>
                <div style="padding: 1%">
                    <table id="tabel-1" class="table table-inverse table-hover" style="width: 110%">
                        <thead>
                            <tr>
                                <th >Bulan</th>
                                <th >Kode</th>
                                <th >No FPT</th>
                                <th >Nama Cust</th>
                                <th >Nama Project</th>
                                <th >Procurement Type</th>
                                <th >Nilai FS</th>
                                <th >Nama Vendor</th>
                                <th >FPT Sent</th>
                                <th >Akun Manager / Sales Name</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" >
    <div class="col-md-12">
        <div class="card" id="2">
            <div class="card-header"><h3>2.List FPT Yang Sudah Nego, Tapi Menunggu Info Status Project Dari AM/sales (Win / Lose)</h3></div>
            <div class="card-body">
                <div class="row clearfix">
                    <div class="col-lg-2" style="text-align:right;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-10">
                        <input type="text" id="input_pencarian-2" class="form-control pull-right" placeholder="ketik disini untuk mencari ...">
                    </div>
                </div>
                <div style="padding: 1%">
                    <table id="tabel-2" class="table table-inverse table-hover" style="width: 100%">
                        <thead>
                            <tr>
                                <th >Bulan</th>
                                <th >Kode</th>
                                <th >No FPT</th>
                                <th >Nama Cust</th>
                                <th >Nama Project</th>
                                <th >Procurement Type</th>
                                <th >Winner Vendor</th>
                                <th >Akun Manager / Sales Name</th>
                                <th >Nilai Quotation Akhir</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card"  id="3">
            <div class="card-header"><h3>3. List Project WIN Namun TRM Belum Mengirimkan SP</h3></div>
            <div class="card-body">
                <div class="row clearfix">
                    <div class="col-lg-2" style="text-align:right;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-10">
                        <input type="text" id="input_pencarian-3" class="form-control pull-right" placeholder="ketik disini untuk mencari ...">
                    </div>
                </div>
                <div style="padding: 1%">
                    <table id="tabel-3" class="table table-inverse table-hover" style="width: 100%">
                        <thead>
                            <tr>
                                <th >Bulan</th>
                                <th >Kode</th>
                                <th >No FPT</th>
                                <th >Nama Cust</th>
                                <th >Nama Project</th>
                                <th >Procurement Type</th>
                                <th >Winner Vendor</th>
                                <th >Tgl Contract/PO Create</th>
                                <th >Tgl P3 Create</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" >
    <div class="col-md-12">
        <div class="card" id="4">
            <div class="card-header"><h3>4. SPK/FPS Created Namun SPK/FPS Belum Full Approved</h3></div>
            <div class="card-body">
                <div class="row clearfix">
                    <div class="col-lg-2" style="text-align:right;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-10">
                        <input type="text" id="input_pencarian-4" class="form-control pull-right" placeholder="ketik disini untuk mencari ...">
                    </div>
                </div>
                <div style="padding: 1%">
                    <table id="tabel-4" class="table table-inverse table-hover" style="width: 100%">
                        <thead>
                            <tr>
                                <th >Bulan</th>
                                <th >Kode</th>
                                <th >No FPT</th>
                                <th >Nama Cust</th>
                                <th >Nama Project</th>
                                <th >Procurement Type</th>
                                <th >Winner Vendor</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" >
    <div class="col-md-12">
        <div class="card" id="5">
            <div class="card-header"><h3>5. SPK/FPS Full Approved Namun Contract/PO Belum Release</h3></div>
            <div class="card-body">
                <div class="row clearfix">
                    <div class="col-lg-2" style="text-align:right;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-10">
                        <input type="text" id="input_pencarian-5" class="form-control pull-right" placeholder="ketik disini untuk mencari ...">
                    </div>
                </div>
                <div style="padding: 1%">
                    <table id="tabel-5" class="table table-inverse table-hover" style="width: 100%">
                        <thead>
                            <tr>
                                <th >Bulan</th>
                                <th >Kode</th>
                                <th >No FPT</th>
                                <th >Nama Cust</th>
                                <th >Nama Project</th>
                                <th >Procurement Type</th>
                                <th >Winner Vendor</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" >
    <div class="col-md-12">
        <div class="card" id="6">
            <div class="card-header"><h3>6. List Project WIN, TRM Sudah Release SP, Namun SAMAKTA Belum Release Contract / PO</h3></div>
            <div class="card-body">
                <div class="row clearfix">
                    <div class="col-lg-2" style="text-align:right;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-10">
                        <input type="text" id="input_pencarian-6" class="form-control pull-right" placeholder="ketik disini untuk mencari ...">
                    </div>
                </div>
                <div style="padding: 1%">
                    <table id="tabel-6" class="table table-inverse table-hover" style="width: 100%">
                        <thead>
                            <tr>
                                <th >Bulan</th>
                                <th >Kode</th>
                                <th >No FPT</th>
                                <th >Nama Cust</th>
                                <th >Nama Project</th>
                                <th >Procurement Type</th>
                                <th >Winner Vendor</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" >
    <div class="col-md-12">
        <div class="card" id="7">
            <div class="card-header"><h3>7. List Project WIN Namun Belum Terima PO dari Customer</h3></div>
            <div class="card-body">
                <div class="row clearfix">
                    <div class="col-lg-2" style="text-align:right;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-10">
                        <input type="text" id="input_pencarian-7" class="form-control pull-right" placeholder="ketik disini untuk mencari ...">
                    </div>
                </div>
                <div style="padding: 1%">
                    <table id="tabel-7" class="table table-inverse table-hover" style="width: 100%">
                        <thead>
                            <tr>
                                <th >Bulan</th>
                                <th >Kode</th>
                                <th >No FPT</th>
                                <th >Nama Cust</th>
                                <th >Nama Project</th>
                                <th >Procurement Type</th>
                                <th >Akun Manager / Sales Name</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" >
    <div class="col-md-12">
        <div class="card" id="8">
            <div class="card-header"><h3>8. Latest Project Status</h3></div>
            <div class="card-body">
                <div class="row clearfix">
                    <div class="col-lg-2" style="text-align:right;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-10">
                        <input type="text" id="input_pencarian-8" class="form-control pull-right" placeholder="ketik disini untuk mencari ...">
                    </div>
                </div>
                <div style="padding: 1%">
                    <table id="tabel-8" class="table table-inverse table-hover" style="width: 100%">
                        <thead>
                            <tr>
                                <th >Bulan</th>
                                <th >Kode</th>
                                <th >No FPT</th>
                                <th >Nama Cust</th>
                                <th >Nama Project</th>
                                <th >Procurement Type</th>
                                <th >List Vendor</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" >
    <div class="col-md-12">
        <div class="card" id="9">
            <div class="card-header"><h3>9. PO Monitoring</h3></div>
            <div class="card-body">
                <div class="row clearfix">
                    <div class="col-lg-2" style="text-align:right;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-10">
                        <input type="text" id="input_pencarian-9" class="form-control pull-right" placeholder="ketik disini untuk mencari ...">
                    </div>
                </div>
                <div style="padding: 1%">
                    <table id="tabel-9" class="table table-inverse table-hover" style="width: 100%">
                        <thead>
                            <tr>
                                <th >Bulan</th>
                                <th >Kode</th>
                                <th >No FPT</th>
                                <th >Nama Cust</th>
                                <th >Nama Project</th>
                                <th >Procurement Type</th>
                                <th >Winner Vendor</th>
                                <th >Vendor Commitment Start</th>
                                <th >Vendor Commitment End</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" >
    <div class="col-md-12">
        <div class="card" id="10">
            <div class="card-header"><h3>10. SPK Monitoring</h3></div>
            <div class="card-body">
                <div class="row clearfix">
                    <div class="col-lg-2" style="text-align:right;padding-top:7px">
                        Cari :
                    </div>
                    <div class="col-lg-10">
                        <input type="text" id="input_pencarian-10" class="form-control pull-right" placeholder="ketik disini untuk mencari ...">
                    </div>
                </div>
                <div style="padding: 1%">
                    <table id="tabel-10" class="table table-inverse table-hover" style="width: 100%">
                        <thead>
                            <tr>
                                <th >Bulan</th>
                                <th >Kode</th>
                                <th >No FPT</th>
                                <th >Nama Cust</th>
                                <th >Nama Project</th>
                                <th >Procurement Type</th>
                                <th >Winner Vendor</th>
                                <th >Vendor Commitment Start</th>
                                <th >Vendor Commitment End</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var mys;
    var form_validator;

    $(document).ready(function() {
        mys = Object.create(myscript_js);
        mys.init('<?= base_url() ?>');
        
        $('#fl_button').on('click', function () {
            let tahun = $('#fl_tahun').val();
            for (let index = 1; index <= 10; index++) {
                $('#tabel-'+index).DataTable().ajax.url(mys.base_url + 'monitoring/get_data_'+index+'?tahun=' + tahun).load();   
            }
        });
        // tabel -1 
        $('#tabel-1').DataTable({
            "scrollCollapse": true,
            "sDom": "t<'row'<'col-md-4'i><'col-md-8'p>>",
            "processing": true,
            "iDisplayLength": 5,
            "scrollX":true,
            "ajax":{
                url : mys.base_url+'monitoring/get_data_1?tahun='+$('#fl_tahun').val(),
                type : 'GET',
            },
            "language": {
                "url": mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            "columnDefs": [
            {"visible" : false, "targets" : []},
            {
                "render": function ( data, type, row ) {
                    return data == 'J'? 'Jasa' : 'Barang';
                },
                "targets": [5]
            },
            {
                "render": function ( data, type, row ) {
                    if (type=='sort') {
                        return data;
                    } return mys.formatMoney(data,0,',','.');
                },
                "targets": [6]
            },
            {
                "render": function ( data, type, row ) {
                    if (type=='sort') {
                        return data;
                    } return moment(data).format('DD-MM-YYYY');
                },
                "targets": [8]
            },
            ],
            "columns": [
            {"width": "3%" },
            {"width": "8%"},
            {"width": "8%"},
            {"width": "15%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "8%"},
            {"width": "15%"},
            {"width": "8%"},
            {"width": "20%"},
            ],
            "order" : [
            [0, "asc"],
            ],
            "fnDrawCallback" : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        $('#input_pencarian-1').on('keyup', function(event) {
            var tabel = $('#tabel-1');
            tabel.dataTable().fnFilter($(this).val());
        });

        //tabel 2
        $('#tabel-2').DataTable({
            "scrollCollapse": true,
            "sDom": "t<'row'<'col-md-4'i><'col-md-8'p>>",
            "processing": true,
            "iDisplayLength": 5,
            "scrollX":true,
            "ajax":{
                url : mys.base_url+'monitoring/get_data_2?tahun='+$('#fl_tahun').val(),
                type : 'GET',
            },
            "language": {
                "url": mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            "columnDefs": [
            {"visible" : false, "targets" : []},
            {
                "render": function ( data, type, row ) {
                    return data == 'J'? 'Jasa' : 'Barang';
                },
                "targets": [5]
            },
            {
                "render": function ( data, type, row ) {
                    if (type=='sort') {
                        return data;
                    } return mys.formatMoney(data,0,',','.');
                },
                "targets": [8]
            },
            ],
            "columns": [
            {"width": "3%" },
            {"width": "8%"},
            {"width": "8%"},
            {"width": "15%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "10%"},
            ],
            "order" : [
            [0, "asc"],
            ],
            "fnDrawCallback" : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        $('#input_pencarian-2').on('keyup', function(event) {
            var tabel = $('#tabel-2');
            tabel.dataTable().fnFilter($(this).val());
        });

        //tabel 3
        $('#tabel-3').DataTable({
            "scrollCollapse": true,
            "sDom": "t<'row'<'col-md-4'i><'col-md-8'p>>",
            "processing": true,
            "iDisplayLength": 5,
            "scrollX":true,
            "ajax":{
                url : mys.base_url+'monitoring/get_data_3?tahun='+$('#fl_tahun').val(),
                type : 'GET',
            },
            "language": {
                "url": mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            "columnDefs": [
            {"visible" : false, "targets" : []},
            {
                "render": function ( data, type, row ) {
                    return data == 'J'? 'Jasa' : 'Barang';
                },
                "targets": [5]
            },
            {
                "render": function ( data, type, row ) {
                    if (type=='sort') {
                        return data;
                    } return data? moment(data).format('DD/MM/YYYY') : '-';
                },
                "targets": [-1,-2]
            },
            ],
            "columns": [
            {"width": "3%" },
            {"width": "8%"},
            {"width": "8%"},
            {"width": "15%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "10%"},
            ],
            "order" : [
            [0, "asc"],
            ],
            "fnDrawCallback" : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        $('#input_pencarian-3').on('keyup', function(event) {
            var tabel = $('#tabel-3');
            tabel.dataTable().fnFilter($(this).val());
        });

        //tabel 4
        $('#tabel-4').DataTable({
            "scrollCollapse": true,
            "sDom": "t<'row'<'col-md-4'i><'col-md-8'p>>",
            "processing": true,
            "iDisplayLength": 5,
            "scrollX":true,
            "ajax":{
                url : mys.base_url+'monitoring/get_data_4?tahun='+$('#fl_tahun').val(),
                type : 'GET',
            },
            "language": {
                "url": mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            "columnDefs": [
            {"visible" : false, "targets" : []},
            {
                "render": function ( data, type, row ) {
                    return data == 'J'? 'Jasa' : 'Barang';
                },
                "targets": [5]
            },
            // {
            //     "render": function ( data, type, row ) {
            //         if (type=='sort') {
            //             return data;
            //         } return data? moment(data).format('DD/MM/YYYY') : '-';
            //     },
            //     "targets": [-1,-2]
            // },
            ],
            "columns": [
            {"width": "3%" },
            {"width": "8%"},
            {"width": "8%"},
            {"width": "15%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "15%"},
            ],
            "order" : [
            [0, "asc"],
            ],
            "fnDrawCallback" : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        $('#input_pencarian-4').on('keyup', function(event) {
            var tabel = $('#tabel-4');
            tabel.dataTable().fnFilter($(this).val());
        });

        //tabel 5
        $('#tabel-5').DataTable({
            "scrollCollapse": true,
            "sDom": "t<'row'<'col-md-4'i><'col-md-8'p>>",
            "processing": true,
            "iDisplayLength": 5,
            "scrollX":true,
            "ajax":{
                url : mys.base_url+'monitoring/get_data_5?tahun='+$('#fl_tahun').val(),
                type : 'GET',
            },
            "language": {
                "url": mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            "columnDefs": [
            {"visible" : false, "targets" : []},
            {
                "render": function ( data, type, row ) {
                    return data == 'J'? 'Jasa' : 'Barang';
                },
                "targets": [5]
            },
            // {
            //     "render": function ( data, type, row ) {
            //         if (type=='sort') {
            //             return data;
            //         } return data? moment(data).format('DD/MM/YYYY') : '-';
            //     },
            //     "targets": [-1,-2]
            // },
            ],
            "columns": [
            {"width": "3%" },
            {"width": "8%"},
            {"width": "8%"},
            {"width": "15%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "15%"},
            ],
            "order" : [
            [0, "asc"],
            ],
            "fnDrawCallback" : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        $('#input_pencarian-5').on('keyup', function(event) {
            var tabel = $('#tabel-5');
            tabel.dataTable().fnFilter($(this).val());
        });

        //tabel 6
        $('#tabel-6').DataTable({
            "scrollCollapse": true,
            "sDom": "t<'row'<'col-md-4'i><'col-md-8'p>>",
            "processing": true,
            "iDisplayLength": 5,
            "scrollX":true,
            "ajax":{
                url : mys.base_url+'monitoring/get_data_6?tahun='+$('#fl_tahun').val(),
                type : 'GET',
            },
            "language": {
                "url": mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            "columnDefs": [
            {"visible" : false, "targets" : []},
            {
                "render": function ( data, type, row ) {
                    return data == 'J'? 'Jasa' : 'Barang';
                },
                "targets": [5]
            },
            // {
            //     "render": function ( data, type, row ) {
            //         if (type=='sort') {
            //             return data;
            //         } return data? moment(data).format('DD/MM/YYYY') : '-';
            //     },
            //     "targets": [-1,-2]
            // },
            ],
            "columns": [
            {"width": "3%" },
            {"width": "8%"},
            {"width": "8%"},
            {"width": "15%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "15%"},
            ],
            "order" : [
            [0, "asc"],
            ],
            "fnDrawCallback" : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        $('#input_pencarian-6').on('keyup', function(event) {
            var tabel = $('#tabel-6');
            tabel.dataTable().fnFilter($(this).val());
        });

        //tabel 7
        $('#tabel-7').DataTable({
            "scrollCollapse": true,
            "sDom": "t<'row'<'col-md-4'i><'col-md-8'p>>",
            "processing": true,
            "iDisplayLength": 5,
            "scrollX":true,
            "ajax":{
                url : mys.base_url+'monitoring/get_data_7?tahun='+$('#fl_tahun').val(),
                type : 'GET',
            },
            "language": {
                "url": mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            "columnDefs": [
            {"visible" : false, "targets" : []},
            {
                "render": function ( data, type, row ) {
                    return data == 'J'? 'Jasa' : 'Barang';
                },
                "targets": [5]
            },
            // {
            //     "render": function ( data, type, row ) {
            //         if (type=='sort') {
            //             return data;
            //         } return data? moment(data).format('DD/MM/YYYY') : '-';
            //     },
            //     "targets": [-1,-2]
            // },
            ],
            "columns": [
            {"width": "3%" },
            {"width": "8%"},
            {"width": "8%"},
            {"width": "15%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "15%"},
            ],
            "order" : [
            [0, "asc"],
            ],
            "fnDrawCallback" : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        $('#input_pencarian-7').on('keyup', function(event) {
            var tabel = $('#tabel-7');
            tabel.dataTable().fnFilter($(this).val());
        });

        //tabel 8
        $('#tabel-8').DataTable({
            "scrollCollapse": true,
            "sDom": "t<'row'<'col-md-4'i><'col-md-8'p>>",
            "processing": true,
            "iDisplayLength": 5,
            "scrollX":true,
            "ajax":{
                url : mys.base_url+'monitoring/get_data_8?tahun='+$('#fl_tahun').val(),
                type : 'GET',
            },
            "language": {
                "url": mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            "columnDefs": [
            {"visible" : false, "targets" : []},
            {
                "render": function ( data, type, row ) {
                    return data == 'J'? 'Jasa' : 'Barang';
                },
                "targets": [5]
            },
            // {
            //     "render": function ( data, type, row ) {
            //         if (type=='sort') {
            //             return data;
            //         } return data? moment(data).format('DD/MM/YYYY') : '-';
            //     },
            //     "targets": [-1,-2]
            // },
            ],
            "columns": [
            {"width": "3%" },
            {"width": "8%"},
            {"width": "8%"},
            {"width": "15%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "15%"},
            ],
            "order" : [
            [0, "asc"],
            ],
            "fnDrawCallback" : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        $('#input_pencarian-8').on('keyup', function(event) {
            var tabel = $('#tabel-8');
            tabel.dataTable().fnFilter($(this).val());
        });

        //tabel 9
        $('#tabel-9').DataTable({
            "scrollCollapse": true,
            "sDom": "t<'row'<'col-md-4'i><'col-md-8'p>>",
            "processing": true,
            "iDisplayLength": 5,
            "scrollX":true,
            "ajax":{
                url : mys.base_url+'monitoring/get_data_9?tahun='+$('#fl_tahun').val(),
                type : 'GET',
            },
            "language": {
                "url": mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            "columnDefs": [
            {"visible" : false, "targets" : []},
            {
                "render": function ( data, type, row ) {
                    return data == 'J'? 'Jasa' : 'Barang';
                },
                "targets": [5]
            },
            {
                "render": function ( data, type, row ) {
                    if (type=='sort') {
                        return data;
                    } return data? moment(data).format('DD/MM/YYYY') : '-';
                },
                "targets": [-1,-2]
            },
            ],
            "columns": [
            {"width": "3%" },
            {"width": "8%"},
            {"width": "8%"},
            {"width": "15%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "10%"},
            ],
            "order" : [
            [0, "asc"],
            ],
            "fnDrawCallback" : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        $('#input_pencarian-9').on('keyup', function(event) {
            var tabel = $('#tabel-9');
            tabel.dataTable().fnFilter($(this).val());
        });

        //tabel 10
        $('#tabel-10').DataTable({
            "scrollCollapse": true,
            "sDom": "t<'row'<'col-md-4'i><'col-md-8'p>>",
            "processing": true,
            "iDisplayLength": 5,
            "scrollX":true,
            "ajax":{
                url : mys.base_url+'monitoring/get_data_10?tahun='+$('#fl_tahun').val(),
                type : 'GET',
            },
            "language": {
                "url": mys.base_url+"assets/plugins/datatables.net/lang/Indonesian.json"
            },
            "columnDefs": [
            {"visible" : false, "targets" : []},
            {
                "render": function ( data, type, row ) {
                    return data == 'J'? 'Jasa' : 'Barang';
                },
                "targets": [5]
            },
            {
                "render": function ( data, type, row ) {
                    if (type=='sort') {
                        return data;
                    } return data? moment(data).format('DD/MM/YYYY') : '-';
                },
                "targets": [-1,-2]
            },
            ],
            "columns": [
            {"width": "3%" },
            {"width": "8%"},
            {"width": "8%"},
            {"width": "15%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "10%"},
            ],
            "order" : [
            [0, "asc"],
            ],
            "fnDrawCallback" : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        $('#input_pencarian-10').on('keyup', function(event) {
            var tabel = $('#tabel-10');
            tabel.dataTable().fnFilter($(this).val());
        });

    });

</script>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h5>Setting Data Perusahaan</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url() ?>"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item" aria-current="page">Setting</li>
                    <li class="breadcrumb-item active" aria-current="page">Setting Data Perusahaan</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 col-md-8">
        <div class="card" id="form_card">
            <div class="card-header"><h3>Form Setting Data Perusahaan</h3></div>
            <div class="card-body">
                <form class="forms-sample" id="form" method="POST" action="javascript:;">
                    <div class="row">
                        <div class="col-12">
                            <div class="text"> 
                                <img src="" id="foto_preview" class="img-thumbnail" width="250">
                                <!-- <h4 class="card-title mt-10">Unggah Foto</h4> --><!-- 
                                <div class="row mb-10 mt-10" id="section_hapus_foto">
                                </div> -->
                                <div class="row text justify-content-md mt-35">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="file" id="upload_foto" name="upload_foto" class="file-upload-default" accept=".png, .jpg, .jpeg">
                                            <div class="input-group col-xs-12">
                                                <input type="text" class="form-control file-upload-info" disabled="" placeholder="File Foto">
                                                <span class="input-group-append">
                                                <button class="file-upload-browse btn btn-primary" type="button">Pilih File</button>
                                                </span>
                                            </div>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama_perusahaan">Nama Perusahaan</label>
                                <input type="text" class="form-control" name="nama_perusahaan" id="nama_perusahaan">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="alamat_perusahaan">Alamat Perusahaan</label>
                                <textarea class="form-control" name="alamat_perusahaan" id="alamat_perusahaan"></textarea>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="email_perusahaan">Email Perusahaan</label>
                                <input type="email" class="form-control" name="email_perusahaan" id="email_perusahaan">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="telepon_perusahaan">Telepon Perusahaan</label>
                                <input type="text" class="form-control" name="telepon_perusahaan" id="telepon_perusahaan">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="fax_perusahaan">Fax Perusahaan</label>
                                <input type="text" class="form-control" name="fax_perusahaan" id="fax_perusahaan">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <button id="btnSimpan" type="submit" class="btn btn-primary mr-2" <?= !$ha['update']? 'disabled' : '' ?>>Simpan Perubahan</button>
                    <!-- <button class="btn btn-default" type="button" id="btnReset">Kembalikan ke Setting Default</button> -->
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var mys;
    var form_validator;

    $(document).ready(function() {
        mys = Object.create(myscript_js);
        mys.init('<?= base_url() ?>');

        form_validator = $('#form').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent("div").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        $('#form').submit(function(event) {
            if (form_validator.form()) {
                simpan();
            }
        });

        $('#upload_foto').change(function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('foto_preview');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        });

        $('#tahun_target').on('change.datetimepicker', function(event) {
            load_target();
        });

        load_setting_perusahaan();
    });

    function reset_all() {
        form_validator.resetForm();
        $('#form')[0].reset();
        $('#form').find('input[type="hidden"]').val('');
        $('#form').find('label,select,input,textarea').removeClass('is-invalid text-red');
        $('#form').find('label,select,input,textarea').removeAttr('disabled');
        $('#form').find('label,select,input,textarea').removeAttr('readonly');
    }


    function load_setting_perusahaan() {
        mys.blok();
        $.ajax({
            url: mys.base_url+'settings/load_setting_perusahaan',
            type: 'GET',
            dataType: 'JSON',
            success:function(data){
                reset_all();
                $('#nama_perusahaan').val(data.nama_perusahaan);
                $('#alamat_perusahaan').val(data.alamat_perusahaan);
                $('#email_perusahaan').val(data.email_perusahaan);
                $('#telepon_perusahaan').val(data.telepon_perusahaan);
                $('#fax_perusahaan').val(data.fax_perusahaan);
                if (data.path_logo_perusahaan) {
                    $('#section_hapus_foto').html('<div class="col-12" >\
                                        <div class="form-check ">\
                                            <label class="custom-control custom-checkbox">\
                                                <input type="checkbox" class="custom-control-input" name="hapus_foto" id="hapus_foto" onclick="status_tombol_foto($(this))">\
                                                <span class="custom-control-label">&nbsp; Centang untuk Menghapus Foto</span>\
                                            </label>\
                                        </div>\
                                    </div>');
                    $('#foto_preview').prop('src', mys.base_url+'assets/img/'+data.path_logo_perusahaan);
                } else{
                    $('#section_hapus_foto').empty();
                    $('#foto_preview').prop('src', mys.base_url+'assets/img/logo.png');
                }
            },
            error:function(data){
                mys.notifikasi("Gagal Mengambil data dari server", "error");
            },
        })
        .always(function() {
            mys.unblok();
        });
    }


    function simpan() {
        mys.blok()
        var formData = new FormData($('#form')[0]);
        $.ajax({
                url: mys.base_url + 'settings/save',
                type: 'POST',
                dataType: 'JSON',
                contentType: false,
                processData: false,
                data: formData,
                success: function(data) {
                    if (data.status) {
                        mys.notifikasi("Data Berhasil Disimpan", "success");
                        load_setting_perusahaan();
                    } else {
                        mys.notifikasi("Beberapa Data Gagal Disimpan, Coba Beberapa Saat Lagi.", "error");
                    }
                },
                error: function(data) {
                    mys.notifikasi("Data Gagal Disimpan, Coba Beberapa Saat Lagi.", "error");

                }
            })
            .always(function() {
                mys.unblok();
            });
    }


</script>
<style type="text/css">
    input:read-only{
        background: transparent !important;
        font-weight: bold !important;
        text-align: right !important;
        pointer-events: none;
    }
</style>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h5>Dashboard</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url() ?>"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="widget">
            <div class="widget-body">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="state">
                        <h6>Customer</h6>
                        <h2 id="jml_customer"></h2>
                    </div>
                    <div class="icon">
                        <i class="ik ik-users"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="widget">
            <div class="widget-body">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="state">
                        <h6>Vendor</h6>
                        <h2 id="jml_vendor"></h2>
                    </div>
                    <div class="icon">
                        <i class="ik ik-link"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="widget">
            <div class="widget-body">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="state">
                        <h6>Sales</h6>
                        <h2 id="jml_sales"></h2>
                    </div>
                    <div class="icon">
                        <i class="ik ik-check-square"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="widget">
            <div class="widget-body">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="state">
                        <h6>Project</h6>
                        <h2 id="jml_project"></h2>
                    </div>
                    <div class="icon">
                        <i class="ik ik-edit"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
<!--             <div class="card-header d-block">
                <h6 style="font-weight: bold;">Target Perusahaan</h6>
            </div> -->
            <div class="card-body">
                <h6 style="font-weight: bold;">Tampilkan Data Berdasarkan:</h6>
                <div class="row">
                    <div class="col-md-3">
                        <label for="fl_tahun_target">Tahun:</label>
                        <input type="text" name="fl_tahun_target" id="fl_tahun_target" data-target="#fl_tahun_target" class="form-control tahun" value="<?= date('Y') ?>">
                    </div>
                    <div class="col-md-1">
                        <label for="fl_button_target">&nbsp;</label>
                        <button id="fl_button_target" type="button" class="btn btn-primary btn-block"><span class="fa fa-search"></span></button>
                    </div>
                </div>
                <br/>
                <div class="row mt-20">
                    <div class="col-md-4">
                        <h6 style="font-weight: bold;"><center>Target Tahunan</center></h6>
                        <br/>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <canvas id="gauge_tahunan" width="350" height="170"></canvas>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span class="badge badge-secondary text-center" id="current_target_tahunan">1.000 / 3.000</span>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <hr>
                    <div class="col-md-4">
                        <h6 style="font-weight: bold;"><center>Target One Time<center></h6>
                        <br/>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <canvas id="gauge_one_time" width="350" height="170"></canvas>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span class="badge badge-secondary text-center" id="current_target_one_time">1.000 / 3.000</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h6 style="font-weight: bold;"><center>Target Recurring<center></h6>
                        <br/>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <canvas id="gauge_recurring" width="350" height="170"></canvas>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span class="badge badge-secondary text-center" id="current_target_recurring">1.000 / 3.000</span>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <h6>Detail Target</h6>
                        <form id="target_tahunan_detail" action="javascript:;">
                            <div class="form-group row">
                                <label for="nilai_target" class="col-sm-4 col-form-label">Nilai Target:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="nilai_target" readonly value="0">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <label for="recurring" class="col-sm-4 col-form-label">Recurring:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="recurring" readonly value="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nilai_one_time" class="col-sm-4 col-form-label">One Time:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="nilai_one_time" readonly value="0">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <label for="pencapaian_target" class="col-sm-4 col-form-label">Total:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="pencapaian_target" readonly value="0">
                                </div>
                            </div>
                        </form>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header d-block">
        <h6 style="font-weight: bold;">Tampilkan Data Berdasarkan:</h6>
        <div class="row">
            <div class="col-md-3">
                <label for="fl_tahun">Tahun:</label>
                <input type="text" name="fl_tahun" id="fl_tahun" data-target="#fl_tahun" class="form-control tahun" value="<?= date('Y') ?>">
            </div>
            <div class="col-md-3">
                <label for="fl_jumlah_tampil">Jumlah Data:</label>
                <select name="fl_jumlah_tampil" id="fl_jumlah_tampil" class="form-control cmb_select2" required="required">
                    <option value="T">10 Teratas</option>
                    <option value="A">Semua</option>
                </select>
            </div>
            <div class="col-md-1">
                <label for="fl_button">&nbsp;</label>
                <button id="fl_button" type="button" class="btn btn-primary btn-block"><span class="fa fa-search"></span></button>
            </div>
        </div>
    </div>
    <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="pills-data-customer-tab" data-toggle="pill" href="#data-customer" role="tab" aria-controls="pills-data-customer" aria-selected="true">Customer</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-data-vendor-tab" data-toggle="pill" href="#data-vendor" role="tab" aria-controls="pills-data-vendor" aria-selected="false">Vendor</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-data-sales-tab" data-toggle="pill" href="#data-sales" role="tab" aria-controls="pills-data-sales" aria-selected="false">Sales</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-data-project-tab" data-toggle="pill" href="#data-project" role="tab" aria-controls="pills-data-project" aria-selected="false">Project</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="data-customer" role="tabpanel" aria-labelledby="pills-data-customer-tab">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h6>Berdasarkan Tipe Customer</h6>
                            </div>
                            <div class="card-body">
                                <canvas id="chart_tipe_customer" width="350" height="150"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h6>Berdasarkan Product</h6>
                            </div>
                            <div class="card-body">
                                <canvas id="chart_product" width="350" height="150"></canvas>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h6>Berdasarkan Tipe Kontrak</h6>
                            </div>
                            <div class="card-body">
                                <canvas id="chart_tipe_kontrak" width="350" height="150"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h6>Berdasarkan Status Project</h6>
                            </div>
                            <div class="card-body">
                                <canvas id="chart_status_project" width="350" height="150"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h6>Berdasarkan Price Project</h6>
                            </div>
                            <div class="card-body">
                                <canvas id="chart_price_project" width="350" height="150"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h6>Berdasarkan Bidang Usaha Customer</h6>
                            </div>
                            <div class="card-body">
                                <canvas id="chart_bidang_usaha" width="350" height="150"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="data-vendor" role="tabpanel" aria-labelledby="pills-data-vendor-tab">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="chart_project_vendor" width="400" height="200"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="data-sales" role="tabpanel" aria-labelledby="pills-data-sales-tab">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="chart_sales_project" width="400" height="200"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="data-project" role="tabpanel" aria-labelledby="pills-data-project-tab">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="chart_project" width="400" height="200"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var mys;
    var chart_tipe_customer;
    var chart_bidang_usaha;
    var chart_product;
    var chart_tipe_kontrak;
    var chart_status_project;
    var chart_project_vendor;
    var chart_sales_project;
    var chart_price_project;
    var chart_project;
    var ctx = {};
    var label_global = [];
    var data_graf = {};
    var chartColors = [
        'rgb(75, 192, 192)',//ijo
        'rgb(255, 99, 132)',//merah
        'rgb(255, 159, 64)',//oren
        'rgb(54, 162, 235)',//biru
        'rgb(255, 205, 86)',//kuning
        'rgb(153, 102, 255)',//ungu
        'rgb(201, 203, 207)',
    ];

    $(document).ready(function() {
        mys = Object.create(myscript_js);
        mys.init('<?= base_url() ?>');

        ctx.chart_tipe_customer = $('#chart_tipe_customer');
        ctx.chart_bidang_usaha = $('#chart_bidang_usaha');
        ctx.chart_product = $('#chart_product');
        ctx.chart_tipe_kontrak = $('#chart_tipe_kontrak');
        ctx.chart_status_project = $('#chart_status_project');
        ctx.chart_project_vendor = $('#chart_project_vendor');
        ctx.chart_sales_project = $('#chart_sales_project');
        ctx.chart_price_project = $('#chart_price_project');
        ctx.chart_project = $('#chart_project');

        $('#fl_button').on('click', function(event) {
            load_data();
        });

        $('#fl_button_target').on('click', function(event) {
            load_target();
        });

        load_statistik();
        load_data();
        load_target();
    });

    function load_statistik(){
        mys.blok()
        $.ajax({
            url: mys.base_url+'dashboard/load_statistik',
            type: 'POST',
            dataType: 'JSON',
            success: function(data){
                Object.keys(data).forEach((key,index) => {
                    if(data[key]){
                        $('#jml_'+key).html(mys.formatMoney(data[key],0,',','.'));
                    }else{
                        $('#jml_'+key).html(0);
                    }
                })
            },
            error:function(data){
                mys.notifikasi("Gagal Mengambil data dari server","error");
            }
        })
        .always(function() {
            mys.unblok();
        });
    }

    function load_target() {
        mys.blok()
        $.ajax({
            url: mys.base_url+'dashboard/load_target',
            type: 'GET',
            dataType: 'JSON',
            data:{
              tahun : $('#fl_tahun_target').val()
            },
            success: function(data){
              var hasil_bagi = parseInt(data.target.nilai_target)/5;
              var current_target_tahunan = parseInt(data.recurring)+parseInt(data.one_time);
              var warna_gauge = ["#34495e","#52b3d9","#22a7f0","#1985fe","#2574a9"];
              var batas_tahunan = {};
                  batas_tahunan.pertama = {};
                  batas_tahunan.pertama.color=warna_gauge[0];
                  batas_tahunan.pertama.min= 0;
                  batas_tahunan.pertama.max= batas_tahunan.pertama.min+hasil_bagi;

                  batas_tahunan.kedua = {};
                  batas_tahunan.kedua.color=warna_gauge[1];
                  batas_tahunan.kedua.min= batas_tahunan.pertama.max;
                  batas_tahunan.kedua.max= batas_tahunan.kedua.min+hasil_bagi;

                  batas_tahunan.ketiga = {};
                  batas_tahunan.ketiga.color=warna_gauge[2];
                  batas_tahunan.ketiga.min= batas_tahunan.kedua.max;
                  batas_tahunan.ketiga.max= batas_tahunan.ketiga.min+hasil_bagi;

                  batas_tahunan.keempat = {};
                  batas_tahunan.keempat.color=warna_gauge[3];
                  batas_tahunan.keempat.min= batas_tahunan.ketiga.max;
                  batas_tahunan.keempat.max= batas_tahunan.keempat.min+hasil_bagi;

                  batas_tahunan.kelima = {};
                  batas_tahunan.kelima.color=warna_gauge[4];
                  batas_tahunan.kelima.min= batas_tahunan.keempat.max;
                  batas_tahunan.kelima.max= parseInt(data.target.nilai_target);
              var opts = {
                angle: 0, // The span of the gauge arc
                lineWidth: 0.35,
                pointer: {
                  length: 0.5,
                  strokeWidth: 0.03,
                  color: '#000000'
                },
                staticZones: [
                   {strokeStyle: batas_tahunan.pertama.color, min: batas_tahunan.pertama.min, max: batas_tahunan.pertama.max},
                   {strokeStyle: batas_tahunan.kedua.color, min: batas_tahunan.kedua.min, max: batas_tahunan.kedua.max},
                   {strokeStyle: batas_tahunan.ketiga.color, min: batas_tahunan.ketiga.min, max: batas_tahunan.ketiga.max},
                   {strokeStyle: batas_tahunan.keempat.color, min: batas_tahunan.keempat.min, max: batas_tahunan.keempat.max},
                   {strokeStyle: batas_tahunan.kelima.color, min: batas_tahunan.kelima.min, max: batas_tahunan.kelima.max}
                ],
                fontsize:35,
                staticLabels: {
                    font: "10px sans-serif",  // Specifies font
                    labels: [batas_tahunan.pertama.min,batas_tahunan.kedua.min,batas_tahunan.ketiga.min,batas_tahunan.keempat.min,batas_tahunan.kelima.min,batas_tahunan.kelima.max],  // Print labels at these values
                    color: "#000000",  // Optional: Label text color
                    fractionDigits: 0  // Optional: Numerical precision. 0=round off.
                  },
                limitMax: false,
                limitMin: false,
                strokeColor: '#E0E0E0',
                highDpiSupport: true    // High resolution support
                
              };

              var target = document.getElementById('gauge_tahunan'); // your canvas element
              var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
              gauge.maxValue = parseInt(data.target.nilai_target); // set max gauge value
              gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
              gauge.animationSpeed = 32; // set animation speed (32 is default value)
              gauge.set(current_target_tahunan); // set actual valueOf()
              var prosentase = parseInt(parseInt(current_target_tahunan)/parseInt(data.target.nilai_target)*100);
              $('#current_target_tahunan').text(prosentase+'%'+' ('+mys.formatMoney(parseInt(current_target_tahunan),0,',','.')+')')


              //init gauge one time
              var hasil_bagi_one_time = parseInt(data.target.nilai_target_one_time)/5;
              var current_target_one_time = parseInt(data.one_time);
              var batas_tahunan_one_time = {};
                  batas_tahunan_one_time.pertama = {};
                  batas_tahunan_one_time.pertama.color=warna_gauge[0];
                  batas_tahunan_one_time.pertama.min= 0;
                  batas_tahunan_one_time.pertama.max= batas_tahunan_one_time.pertama.min+hasil_bagi_one_time;

                  batas_tahunan_one_time.kedua = {};
                  batas_tahunan_one_time.kedua.color=warna_gauge[1];
                  batas_tahunan_one_time.kedua.min= batas_tahunan_one_time.pertama.max;
                  batas_tahunan_one_time.kedua.max= batas_tahunan_one_time.kedua.min+hasil_bagi_one_time;

                  batas_tahunan_one_time.ketiga = {};
                  batas_tahunan_one_time.ketiga.color=warna_gauge[2];
                  batas_tahunan_one_time.ketiga.min= batas_tahunan_one_time.kedua.max;
                  batas_tahunan_one_time.ketiga.max= batas_tahunan_one_time.ketiga.min+hasil_bagi_one_time;

                  batas_tahunan_one_time.keempat = {};
                  batas_tahunan_one_time.keempat.color=warna_gauge[3];
                  batas_tahunan_one_time.keempat.min= batas_tahunan_one_time.ketiga.max;
                  batas_tahunan_one_time.keempat.max= batas_tahunan_one_time.keempat.min+hasil_bagi_one_time;

                  batas_tahunan_one_time.kelima = {};
                  batas_tahunan_one_time.kelima.color=warna_gauge[4];
                  batas_tahunan_one_time.kelima.min= batas_tahunan_one_time.keempat.max;
                  batas_tahunan_one_time.kelima.max= parseInt(data.target.nilai_target_one_time);
              var opts = {
                angle: 0, // The span of the gauge arc
                lineWidth: 0.35,
                pointer: {
                  length: 0.5,
                  strokeWidth: 0.03,
                  color: '#000000'
                },
                staticZones: [
                   {strokeStyle: batas_tahunan_one_time.pertama.color, min: batas_tahunan_one_time.pertama.min, max: batas_tahunan_one_time.pertama.max},
                   {strokeStyle: batas_tahunan_one_time.kedua.color, min: batas_tahunan_one_time.kedua.min, max: batas_tahunan_one_time.kedua.max},
                   {strokeStyle: batas_tahunan_one_time.ketiga.color, min: batas_tahunan_one_time.ketiga.min, max: batas_tahunan_one_time.ketiga.max},
                   {strokeStyle: batas_tahunan_one_time.keempat.color, min: batas_tahunan_one_time.keempat.min, max: batas_tahunan_one_time.keempat.max},
                   {strokeStyle: batas_tahunan_one_time.kelima.color, min: batas_tahunan_one_time.kelima.min, max: batas_tahunan_one_time.kelima.max}
                ],
                fontsize:35,
                staticLabels: {
                    font: "10px sans-serif",  // Specifies font
                    labels: [batas_tahunan_one_time.pertama.min,batas_tahunan_one_time.kedua.min,batas_tahunan_one_time.ketiga.min,batas_tahunan_one_time.keempat.min,batas_tahunan_one_time.kelima.min,batas_tahunan_one_time.kelima.max],  // Print labels at these values
                    color: "#000000",  // Optional: Label text color
                    fractionDigits: 0  // Optional: Numerical precision. 0=round off.
                  },
                limitMax: false,
                limitMin: false,
                strokeColor: '#E0E0E0',
                highDpiSupport: true    // High resolution support
                
              };

              var target = document.getElementById('gauge_one_time'); // your canvas element
              var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
              gauge.maxValue = parseInt(data.target.nilai_target_one_time); // set max gauge value
              gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
              gauge.animationSpeed = 32; // set animation speed (32 is default value)
              gauge.set(current_target_one_time); // set actual valueOf()
              var prosentase = parseInt(parseInt(current_target_one_time)/parseInt(data.target.nilai_target_one_time)*100);
              $('#current_target_one_time').text(prosentase+'%'+' ('+mys.formatMoney(parseInt(current_target_one_time),0,',','.')+')')


              //init gauge recurring
              var hasil_bagi_recurring = parseInt(data.target.nilai_target_recurring)/5;
              var current_target_recurring = parseInt(data.recurring);
              var batas_tahunan_recurring = {};
                  batas_tahunan_recurring.pertama = {};
                  batas_tahunan_recurring.pertama.color=warna_gauge[0];
                  batas_tahunan_recurring.pertama.min= 0;
                  batas_tahunan_recurring.pertama.max= batas_tahunan_recurring.pertama.min+hasil_bagi_recurring;

                  batas_tahunan_recurring.kedua = {};
                  batas_tahunan_recurring.kedua.color=warna_gauge[1];
                  batas_tahunan_recurring.kedua.min= batas_tahunan_recurring.pertama.max;
                  batas_tahunan_recurring.kedua.max= batas_tahunan_recurring.kedua.min+hasil_bagi_recurring;

                  batas_tahunan_recurring.ketiga = {};
                  batas_tahunan_recurring.ketiga.color=warna_gauge[2];
                  batas_tahunan_recurring.ketiga.min= batas_tahunan_recurring.kedua.max;
                  batas_tahunan_recurring.ketiga.max= batas_tahunan_recurring.ketiga.min+hasil_bagi_recurring;

                  batas_tahunan_recurring.keempat = {};
                  batas_tahunan_recurring.keempat.color=warna_gauge[3];
                  batas_tahunan_recurring.keempat.min= batas_tahunan_recurring.ketiga.max;
                  batas_tahunan_recurring.keempat.max= batas_tahunan_recurring.keempat.min+hasil_bagi_recurring;

                  batas_tahunan_recurring.kelima = {};
                  batas_tahunan_recurring.kelima.color=warna_gauge[4];
                  batas_tahunan_recurring.kelima.min= batas_tahunan_recurring.keempat.max;
                  batas_tahunan_recurring.kelima.max= parseInt(data.target.nilai_target_recurring);
              var opts = {
                angle: 0, // The span of the gauge arc
                lineWidth: 0.35,
                pointer: {
                  length: 0.5,
                  strokeWidth: 0.03,
                  color: '#000000'
                },
                staticZones: [
                   {strokeStyle: batas_tahunan_recurring.pertama.color, min: batas_tahunan_recurring.pertama.min, max: batas_tahunan_recurring.pertama.max},
                   {strokeStyle: batas_tahunan_recurring.kedua.color, min: batas_tahunan_recurring.kedua.min, max: batas_tahunan_recurring.kedua.max},
                   {strokeStyle: batas_tahunan_recurring.ketiga.color, min: batas_tahunan_recurring.ketiga.min, max: batas_tahunan_recurring.ketiga.max},
                   {strokeStyle: batas_tahunan_recurring.keempat.color, min: batas_tahunan_recurring.keempat.min, max: batas_tahunan_recurring.keempat.max},
                   {strokeStyle: batas_tahunan_recurring.kelima.color, min: batas_tahunan_recurring.kelima.min, max: batas_tahunan_recurring.kelima.max}
                ],
                fontsize:35,
                staticLabels: {
                    font: "10px sans-serif",  // Specifies font
                    labels: [batas_tahunan_recurring.pertama.min,batas_tahunan_recurring.kedua.min,batas_tahunan_recurring.ketiga.min,batas_tahunan_recurring.keempat.min,batas_tahunan_recurring.kelima.min,batas_tahunan_recurring.kelima.max],  // Print labels at these values
                    color: "#000000",  // Optional: Label text color
                    fractionDigits: 0  // Optional: Numerical precision. 0=round off.
                  },
                limitMax: false,
                limitMin: false,
                strokeColor: '#E0E0E0',
                highDpiSupport: true    // High resolution support
                
              };

              var target = document.getElementById('gauge_recurring'); // your canvas element
              var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
              gauge.maxValue = parseInt(data.target.nilai_target_recurring); // set max gauge value
              gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
              gauge.animationSpeed = 32; // set animation speed (32 is default value)
              gauge.set(current_target_recurring); // set actual valueOf()
              var prosentase = parseInt(parseInt(current_target_recurring)/parseInt(data.target.nilai_target_recurring)*100);
              $('#current_target_recurring').text(prosentase+'%'+' ('+mys.formatMoney(parseInt(current_target_recurring),0,',','.')+')')
              // //set value detail
              // $('#target_tahunan_detail').find('input').val(0);
              // $('#nilai_target').val(mys.formatMoney(parseInt(data.nilai_target),0,',','.'));
              // $('#recurring').val(mys.formatMoney(parseInt(data.recurring),0,',','.'));
              // $('#nilai_one_time').val(mys.formatMoney(parseInt(data.one_time),0,',','.'));
              // $('#pencapaian_target').val(mys.formatMoney(parseInt(current_value),0,',','.'));
              // $('#pencapaian_target').removeClass('form-control-success');
              // $('#pencapaian_target').removeClass('form-control-warning');
              // if (current_value<parseInt(data.nilai_target)) {
              //   $('#pencapaian_target').addClass('form-control-warning');
              // } else{
              //   $('#pencapaian_target').addClass('form-control-success');
              // }
            },
            error:function(data){
                mys.notifikasi("Gagal Mengambil data dari server","error");
            }
        })
        .always(function() {
            mys.unblok();
        });
    }

    function load_data() {
        mys.blok()
        destroy_chart();
        reset_data();
        var data_send = {};
            data_send.jumlah_tampil = $('#fl_jumlah_tampil').val();
            data_send.tahun = $('#fl_tahun').val();
        $.ajax({
            url: mys.base_url+'dashboard/load_data',
            type: 'GET',
            dataType: 'JSON',
            data: data_send,
            success: function(data){
                //init chart tipe customer
                var data_tipe_customer = {
                    data:[],
                    backgroundColor:[],
                    label: 'Tipe Customer'
                };

                $.each(data.tipe_customer, function(index, val) {
                     data_tipe_customer.data.push(val.jumlah);
                     data_tipe_customer.backgroundColor.push(chartColors[index]);
                     data_graf.chart_tipe_customer.labels.push(val.label);
                });


                data_graf.chart_tipe_customer.datasets.push(data_tipe_customer);

                chart_tipe_customer =  new Chart(ctx.chart_tipe_customer,{
                    type: 'pie',
                    data: data_graf.chart_tipe_customer,
                    options: {
                        legend:{
                            position : 'right',
                        },
                        responsive: true,
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    // console.log(tooltipItem);
                                    // console.log(data);
                                    // console.log(data.datasets[0].data[tooltipItem.index]);
                                    let sum = parseInt(data.datasets[0].data.reduce(sum_array));
                                    let current_data = parseInt(data.datasets[0].data[tooltipItem.index]);
                                    let percentage =  current_data / sum *100;
                                    return data.labels[tooltipItem.index]+": "+mys.formatMoney(current_data,0,',','.')+" ("+mys.formatMoney(percentage,0,',','.')+"%)";
                                }
                            }
                        },
                    }
                });

                //init chart tipe customer
                var data_bidang_usaha = {
                    data:[],
                    backgroundColor:[],
                    label: 'Tipe Customer'
                };

                $.each(data.bidang_usaha, function(index, val) {
                     data_bidang_usaha.data.push(val.jumlah);
                     data_bidang_usaha.backgroundColor.push(chartColors[index]);
                     data_graf.chart_bidang_usaha.labels.push(val.label);
                });


                data_graf.chart_bidang_usaha.datasets.push(data_bidang_usaha);

                chart_bidang_usaha =  new Chart(ctx.chart_bidang_usaha,{
                    type: 'pie',
                    data: data_graf.chart_bidang_usaha,
                    options: {
                        legend:{
                            position : 'right',
                        },
                        responsive: true,
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    // console.log(tooltipItem);
                                    // console.log(data);
                                    // console.log(data.datasets[0].data[tooltipItem.index]);
                                    let sum = parseInt(data.datasets[0].data.reduce(sum_array));
                                    let current_data = parseInt(data.datasets[0].data[tooltipItem.index]);
                                    let percentage =  current_data / sum *100;
                                    return data.labels[tooltipItem.index]+": "+mys.formatMoney(current_data,0,',','.')+" ("+mys.formatMoney(percentage,0,',','.')+"%)";
                                }
                            }
                        },
                    }
                });

                //init chart product
                var data_product = {
                    data:[],
                    backgroundColor:[],
                    label: 'Product'
                };

                $.each(data.product, function(index, val) {
                     data_product.data.push(val.jumlah);
                     data_product.backgroundColor.push(chartColors[index]);
                     data_graf.chart_product.labels.push(val.label);
                });


                data_graf.chart_product.datasets.push(data_product);

                chart_product =  new Chart(ctx.chart_product,{
                    type: 'pie',
                    data: data_graf.chart_product,
                    options: {
                        legend:{
                            position : 'right',
                        },
                        responsive: true,
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    // console.log(tooltipItem);
                                    // console.log(data);
                                    // console.log(data.datasets[0].data[tooltipItem.index]);
                                    let sum = parseInt(data.datasets[0].data.reduce(sum_array));
                                    let current_data = parseInt(data.datasets[0].data[tooltipItem.index]);
                                    let percentage =  current_data / sum *100;
                                    return data.labels[tooltipItem.index]+": "+mys.formatMoney(current_data,0,',','.')+" ("+mys.formatMoney(percentage,0,',','.')+"%)";
                                }
                            }
                        },
                    }
                });

                //init chart tipe kontrak
                var data_tipe_kontrak = {
                    data:[],
                    backgroundColor:[],
                    label: 'Tipe Kontrak'
                };

                $.each(data.tipe_kontrak, function(index, val) {
                     data_tipe_kontrak.data.push(val.jumlah);
                     data_tipe_kontrak.backgroundColor.push(chartColors[index]);
                     data_graf.chart_tipe_kontrak.labels.push(val.label);
                });


                data_graf.chart_tipe_kontrak.datasets.push(data_tipe_kontrak);

                chart_tipe_kontrak =  new Chart(ctx.chart_tipe_kontrak,{
                    type: 'pie',
                    data: data_graf.chart_tipe_kontrak,
                    options: {
                        legend:{
                            position : 'right',
                        },
                        responsive: true,
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    // console.log(tooltipItem);
                                    // console.log(data);
                                    // console.log(data.datasets[0].data[tooltipItem.index]);
                                    let sum = parseInt(data.datasets[0].data.reduce(sum_array));
                                    let current_data = parseInt(data.datasets[0].data[tooltipItem.index]);
                                    let percentage =  current_data / sum *100;
                                    return data.labels[tooltipItem.index]+": "+mys.formatMoney(current_data,0,',','.')+" ("+mys.formatMoney(percentage,0,',','.')+"%)";
                                }
                            }
                        },
                    }
                });

                                //init chart tipe kontrak
                var data_price_project = {
                    data:[],
                    backgroundColor:[],
                    label: 'Price Project'
                };

                $.each(data.price_project, function(index, val) {
                     data_price_project.data.push(val.jumlah);
                     data_price_project.backgroundColor.push(chartColors[index]);
                     data_graf.chart_price_project.labels.push(val.label);
                });


                data_graf.chart_price_project.datasets.push(data_price_project);

                chart_price_project =  new Chart(ctx.chart_price_project,{
                    type: 'pie',
                    data: data_graf.chart_price_project,
                    options: {
                        legend:{
                            position : 'right',
                        },
                        responsive: true,
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    
                                    return data.labels[tooltipItem.index]+": Rp "+mys.formatMoney(data.datasets[0].data[tooltipItem.index],0,',','.');
                                }
                            }
                        }
                    }
                });


                //init chart status project
                var data_status_project = {
                    data:[],
                    backgroundColor:[],
                    label: 'Status Project'
                };

                data_status_project.backgroundColor.push('rgb(255, 159, 64)');
                data_status_project.backgroundColor.push('rgb(75, 192, 192)');
                data_status_project.backgroundColor.push('rgb(255, 99, 132)');
                data_status_project.backgroundColor.push('rgb(0, 0, 0)');
                $.each(data.status_project, function(index, val) {
                     data_status_project.data.push(val.jumlah);
                     data_graf.chart_status_project.labels.push(val.label);
                });


                data_graf.chart_status_project.datasets.push(data_status_project);

                chart_status_project =  new Chart(ctx.chart_status_project,{
                    type: 'pie',
                    data: data_graf.chart_status_project,
                    options: {
                        legend:{
                            position : 'right',
                        },
                        responsive: true,
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    // console.log(tooltipItem);
                                    // console.log(data);
                                    // console.log(data.datasets[0].data[tooltipItem.index]);
                                    let sum = parseInt(data.datasets[0].data.reduce(sum_array));
                                    let current_data = parseInt(data.datasets[0].data[tooltipItem.index]);
                                    let percentage =  current_data / sum *100;
                                    return data.labels[tooltipItem.index]+": "+mys.formatMoney(current_data,0,',','.')+" ("+mys.formatMoney(percentage,0,',','.')+"%)";
                                }
                            }
                        },
                    }
                });

                //init project vendor
                var data_project_vendor = {
                    label : 'Jumlah Project',
                    borderColor: 'rgb(54, 162, 235)',
                    borderWidth: 1,
                    backgroundColor: 'rgb(54, 162, 235)',
                    data: []
                };
                $.each(data.project_vendor, function(index, val) {
                    data_graf.chart_project_vendor.labels.push(val.label);
                    data_project_vendor.data.push(val.jumlah);
                });

                data_graf.chart_project_vendor.datasets.push(data_project_vendor);

                chart_project_vendor = new Chart(ctx.chart_project_vendor, {
                    type: 'bar',
                    data: data_graf.chart_project_vendor,
                    options: {
                        legend:{
                            position : 'bottom',
                        },
                        elements: {
                            rectangle: {
                                borderWidth: 2,
                            }
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false
                        },
                        responsive: true,
                    }
                });

                //init sales project
                var data_sales_win = {
                    label : 'WIN',
                    borderColor: 'rgb(75, 192, 192)',
                    borderWidth: 1,
                    backgroundColor: 'rgb(75, 192, 192)',
                    data: []
                };
                var data_sales_lose = {
                    label : 'LOSE',
                    borderColor: 'rgb(255, 99, 132)',
                    borderWidth: 1,
                    backgroundColor: 'rgb(255, 99, 132)',
                    data: []
                };
                var data_sales_pending = {
                    label : 'WAITING',
                    borderColor: 'rgb(255, 159, 64)',
                    borderWidth: 1,
                    backgroundColor: 'rgb(255, 159, 64)',
                    data: []
                };
                var data_sales_cancel = {
                    label : 'CANCEL',
                    borderColor: 'rgb(0, 0, 0)',
                    borderWidth: 1,
                    backgroundColor: 'rgb(0, 0, 0)',
                    data: []
                };
                $.each(data.sales_project, function(index, val) {
                    data_graf.chart_sales_project.labels.push(val.label);
                    data_sales_win.data.push(val.win);
                    data_sales_lose.data.push(val.lose);
                    data_sales_pending.data.push(val.pending);
                    data_sales_cancel.data.push(val.win);
                });

                data_graf.chart_sales_project.datasets.push(data_sales_pending);
                data_graf.chart_sales_project.datasets.push(data_sales_win);
                data_graf.chart_sales_project.datasets.push(data_sales_lose);
                data_graf.chart_sales_project.datasets.push(data_sales_cancel);

                chart_sales_project = new Chart(ctx.chart_sales_project, {
                    type: 'bar',
                    data: data_graf.chart_sales_project,
                    options: {
                        legend:{
                            position : 'bottom',
                        },
                        elements: {
                            rectangle: {
                                borderWidth: 2,
                            }
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false
                        },
                        responsive: true,
                    }
                });

                //init project
                var data_project = {};
                    data_project.win = {
                        label : 'WIN',
                        backgroundColor: 'rgb(75, 192, 192)',
                        data: []
                    };
                    data_project.lose = {
                        label : 'LOSE',
                        backgroundColor: 'rgb(255, 99, 132)',
                        data: []
                    };
                    data_project.waiting = {
                        label : 'WAITING',
                        backgroundColor: 'rgb(255, 159, 64)',
                        data: []
                    };
                    data_project.cancel = {
                        label : 'CANCEL',
                        backgroundColor: 'rgb(0, 0, 0)',
                        data: []
                    };


                $.each(data.project, function(index, val) {
                    var label_waktu = val.tahun+'-'+val.bulan;
                    data_graf.chart_project.labels.push(moment(label_waktu,'YYYY-MM').format('MMM YY'));
                    data_project.win.data.push(val.win);
                    data_project.lose.data.push(val.lose);
                    data_project.waiting.data.push(val.waiting);
                    data_project.cancel.data.push(val.cancel);
                });

                data_graf.chart_project.datasets.push(data_project.win);
                data_graf.chart_project.datasets.push(data_project.lose);
                data_graf.chart_project.datasets.push(data_project.waiting);
                data_graf.chart_project.datasets.push(data_project.cancel);

                chart_project = new Chart(ctx.chart_project, {
                    type: 'bar',
                    data: data_graf.chart_project,
                    options: {
                        legend:{
                            position : 'bottom',
                        },
                        elements: {
                            rectangle: {
                                borderWidth: 2,
                            }
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        }
                    }
                });
            },
            error:function(data){
                mys.notifikasi("Gagal Ambil Data dari Server","error");
            }
        }).always(function(){
            mys.unblok();
        });
    }

    function destroy_chart() {
        if (chart_tipe_customer) {
            chart_tipe_customer.destroy();
        }

        if (chart_bidang_usaha) {
            chart_bidang_usaha.destroy();
        }

        if (chart_product) {
            chart_product.destroy();
        }

        if (chart_tipe_kontrak) {
            chart_tipe_kontrak.destroy();
        }

        if (chart_project_vendor) {
            chart_project_vendor.destroy();
        }

        if (chart_sales_project) {
            chart_sales_project.destroy();
        }
        if (chart_status_project) {
            chart_status_project.destroy();
        }
        if (chart_price_project) {
            chart_price_project.destroy();
        }
        if (chart_project) {
            chart_project.destroy();
        }
    }

    function reset_data() {
        label_global = [];
        data_graf.chart_tipe_customer = {
            labels: [],
            datasets:[]
        };
        data_graf.chart_bidang_usaha = {
            labels: [],
            datasets:[]
        };
        data_graf.chart_product = {
            labels: [],
            datasets:[]
        };
        data_graf.chart_tipe_kontrak = {
            labels: [],
            datasets:[]
        };
        data_graf.chart_project_vendor = {
            labels: [],
            datasets:[]
        };
        data_graf.chart_sales_project = {
            labels: [],
            datasets:[]
        };
        data_graf.chart_status_project = {
            labels: [],
            datasets:[]
        };
        data_graf.chart_price_project = {
            labels: [],
            datasets:[]
        };
        data_graf.chart_project = {
            labels: [],
            datasets:[]
        };
    }

    function sum_array(total, num) {
        return parseInt(total) + parseInt(num);
    }
</script>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	function get_statistik()
	{
		$query = "SELECT (SELECT COUNT(id_customer) FROM customer WHERE status_customer = 'A') AS customer,(SELECT COUNT(id_vendor) FROM vendor WHERE status_vendor = 'A') AS vendor,
			(SELECT COUNT(id_sales) FROM sales WHERE status_sales = 'A') AS sales,(SELECT COUNT(id_project) FROM project where project_deleted = 0) AS project";
		return $this->db->query($query);
	}

	function get_chart_tipe_customer($tahun)
	{
		$query = "SELECT `nama_tipe_customer` as label, (SELECT COUNT(id_project) FROM project sp JOIN customer sc ON sp.id_customer = sc.id_customer WHERE sc.id_tipe_customer = tc.id_tipe_customer AND project_year = ?) AS jumlah 
		FROM tipe_customer tc
		WHERE status_tipe_customer = 'A'
		limit 7";

		return $this->db->query($query,[$tahun]);
	}

	function get_chart_bidang_usaha($tahun)
	{
		$query = "SELECT `nama_bidang_usaha` as label, (SELECT COUNT(id_project) FROM project sp JOIN customer sc ON sp.id_customer = sc.id_customer WHERE sc.id_bidang_usaha = bu.id_bidang_usaha AND project_year = ?) AS jumlah 
		FROM bidang_usaha bu
		WHERE status_bidang_usaha = 'A'
		limit 7";

		return $this->db->query($query,[$tahun]);
	}

	function get_chart_price_project($tahun)
	{
		$query = "SELECT 'Total PO' AS label, SUM(nilai_fs_barang) AS jumlah
		FROM project
		WHERE project_year = ? and project_deleted = 0
		UNION
		SELECT 'Total SPK' AS label, SUM(nilai_fs_jasa) AS jumlah
		FROM project
		WHERE project_year = ? and project_deleted = 0
		";

		return $this->db->query($query,[$tahun,$tahun]);
	}

	function get_chart_product($tahun)
	{
		$query = "SELECT nama_product as label, (SELECT COUNT(id_project) FROM project sp WHERE sp.id_product = p.id_product AND project_year = ?) AS jumlah
		FROM product p 
		WHERE status_product = 'A'
		limit 7";
		return $this->db->query($query,[$tahun]);
	}

	function get_chart_project($tahun)
	{
		$query = "SELECT DISTINCT pr.project_year AS tahun, DATE_FORMAT(pr.created_at,'%m') AS bulan, 
			(SELECT COUNT(id_project) FROM project WHERE status_project = 'W' AND project_year = pr.project_year and date_format(created_at,'%m') = date_format(pr.created_at,'%m') and project_deleted = 0) AS win,
			(SELECT COUNT(id_project) FROM project WHERE status_project = 'L' AND project_year = pr.project_year and date_format(created_at,'%m') = date_format(pr.created_at,'%m') and project_deleted = 0) AS lose,
			(SELECT COUNT(id_project) FROM project WHERE status_project = 'P' AND project_year = pr.project_year and date_format(created_at,'%m') = date_format(pr.created_at,'%m') and project_deleted = 0) AS waiting,
			(SELECT COUNT(id_project) FROM project WHERE status_project = 'C' AND project_year = pr.project_year and date_format(created_at,'%m') = date_format(pr.created_at,'%m') and project_deleted = 0) AS cancel
			FROM project pr
			WHERE project_year = ?
			order by 2 asc";
		return $this->db->query($query,[$tahun]);
	}

	function get_chart_tipe_kontrak($tahun)
	{
		$query = "SELECT 'One Time' as label, COUNT(id_project) AS jumlah
		FROM project
		WHERE project_year = ? AND tipe_kontrak_project = 'O' and project_deleted = 0
		UNION
		SELECT 'Recurring' as label, COUNT(id_project) AS jumlah
		FROM project
		WHERE project_year = ? AND tipe_kontrak_project = 'R' and project_deleted = 0
		;";
		return $this->db->query($query,[$tahun,$tahun]);
	}

	function get_chart_status_project($tahun)
	{
		$query = "SELECT 'WAITING' as label, COUNT(id_project) AS jumlah
		FROM project
		WHERE project_year = ? AND status_project = 'P' and project_deleted = 0
		UNION
		SELECT 'WIN' as label, COUNT(id_project) AS jumlah
		FROM project
		WHERE project_year = ? AND status_project = 'W' and project_deleted = 0
		UNION
		SELECT 'LOSE' as label, COUNT(id_project) AS jumlah
		FROM project
		WHERE project_year = ? AND status_project = 'L' and project_deleted = 0
		UNION
		SELECT 'CANCEL' as label, COUNT(id_project) AS jumlah
		FROM project
		WHERE project_year = ? AND status_project = 'C' and project_deleted = 0";
		return $this->db->query($query,[$tahun,$tahun,$tahun,$tahun]);
	}

	function get_chart_project_vendor($tahun,$jumlah_tampil = 'A')
	{
		$query = "SELECT nama_vendor as label, (SELECT COUNT(DISTINCT spv.id_project) FROM project_vendor spv JOIN project sp ON spv.id_project = sp.id_project WHERE sp.project_year = ? AND spv.id_vendor = v.id_vendor) AS jumlah
			FROM vendor v
			WHERE status_vendor = 'A' AND (SELECT COUNT(DISTINCT spv.id_project) FROM project_vendor spv JOIN project sp ON spv.id_project = sp.id_project WHERE sp.project_year = ? AND spv.id_vendor = v.id_vendor and sp.project_deleted = 0) > 0
			ORDER BY 2 DESC ";
		$query .= ($jumlah_tampil=='T') ? "LIMIT 10" : "";
		return $this->db->query($query,[$tahun,$tahun]);
	}

	function get_chart_sales_project($tahun,$jumlah_tampil = 'A')
	{
		$query = "SELECT nama_sales as label,
			(SELECT COUNT(sp.id_project) FROM project sp WHERE sp.project_year = ? AND sp.id_sales = s.id_sales) AS jumlah, 
			(SELECT COUNT(sp.id_project) FROM project sp WHERE sp.project_year = ? AND sp.id_sales = s.id_sales and sp.status_project = 'P') AS pending,
			(SELECT COUNT(sp.id_project) FROM project sp WHERE sp.project_year = ? AND sp.id_sales = s.id_sales and sp.status_project = 'W') AS win,
			(SELECT COUNT(sp.id_project) FROM project sp WHERE sp.project_year = ? AND sp.id_sales = s.id_sales and sp.status_project = 'L') AS lose,
			(SELECT COUNT(sp.id_project) FROM project sp WHERE sp.project_year = ? AND sp.id_sales = s.id_sales and sp.status_project = 'C') AS cancel
		FROM sales s
		WHERE status_sales = 'A' AND (SELECT COUNT(sp.id_project) FROM project sp WHERE sp.project_year = ? AND sp.id_sales = s.id_sales and sp.project_deleted = 0) > 0
		ORDER BY 2 DESC ";
		$query .= ($jumlah_tampil=='T') ? "LIMIT 10" : "";
		return $this->db->query($query,[$tahun,$tahun,$tahun,$tahun,$tahun,$tahun]);
	}

	function get_target($tahun)
	{
		$nilai_target = $this->db->select('nilai_target,nilai_target_one_time,nilai_target_recurring')->where('tahun_target', $tahun)->get('target_perusahaan');
		return $nilai_target->num_rows() > 0 ? $nilai_target->row_array() : ['nilai_target'=> 0, 'nilai_target_one_time' => 0,'nilai_target_recurring'=>0];
	}

	function get_total_nilai($tahun,$tipe)
	{
		$this->db->select_sum('nilai_project');
		$this->db->where('project_year', $tahun);
		$this->db->where('tipe_kontrak_project', $tipe);
		$this->db->where('status_project', 'W');
		$this->db->where('project_deleted', 0);
		$query = $this->db->get('project');
		return $query->row()->nilai_project? $query->row()->nilai_project : 0;
	}

}

/* End of file Dashboard_model.php */
/* Location: ./application/models/Dashboard_model.php */
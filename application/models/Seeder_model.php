<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seeder_model extends CI_Model {

	function save($data,$tabel)
	{
		$insert = $this->db->insert($tabel, $data);

		return $insert? 1 : 0;
	}

	function get_id_bidang_usaha_list()
	{
		$this->db->select('id_bidang_usaha');
		$this->db->where('status_bidang_usaha', 'A');
		$bidang_usaha = $this->db->get('bidang_usaha')->result();

		$return = [];
		foreach ($bidang_usaha as $value) {
			array_push($return, $value->id_bidang_usaha);
		}
		return $return;
	}

	function get_id_jabatan_list()
	{
		$this->db->select('id_jabatan');
		$this->db->where('status_jabatan', 'A');
		$jabatan = $this->db->get('jabatan')->result();

		$return = [];
		foreach ($jabatan as $value) {
			array_push($return, $value->id_jabatan);
		}
		return $return;
	}

	function get_id_tipe_customer_list()
	{
		$this->db->select('id_tipe_customer');
		$this->db->where('status_tipe_customer', 'A');
		$tipe_customer = $this->db->get('tipe_customer')->result();

		$return = [];
		foreach ($tipe_customer as $value) {
			array_push($return, $value->id_tipe_customer);
		}
		return $return;
	}

	function get_id_sales_list()
	{
		$this->db->select('id_sales');
		$this->db->where('status_sales', 'A');
		$sales = $this->db->get('sales')->result();

		$return = [];
		foreach ($sales as $value) {
			array_push($return, $value->id_sales);
		}
		return $return;
	}

	function get_city_id_list()
	{
		$this->db->select('city_id');
		$city = $this->db->get('city')->result();

		$return = [];
		foreach ($city as $value) {
			array_push($return, $value->city_id);
		}
		return $return;
	}

	function get_bidang_usaha_list()
	{
		$bidang_usaha = [
			'Pertanian',
			'Pertambangan',
			'Pabrikasi',
			'Kontruksi',
			'Perdagangan',
			'Jasa Keuangan',
			'Jasa Perorangan',
			'Jasa Umum',
			'Jasa Wisata',
		];

		return $bidang_usaha;

	}

}

/* End of file Seeder_model.php */
/* Location: ./application/models/Seeder_model.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring_model extends CI_Model {

	function get_data_1($tahun)
	{
		$this->db->select("date_format(p.created_at,'%m') as bulan,p.kode_project,(select no_fpt from project_vendor spv where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian) as no_fpt,c.nama_customer,p.nama_project,tv.tipe_pembelian as tipe_pembelian_vendor, case when tv.tipe_pembelian = 'B' then p.nilai_fs_barang else p.nilai_fs_jasa end as nilai_fs,v.nama_vendor, s.nama_sales, tv.fpt_sent");
		$this->db->from('tender_vendor tv');
		$this->db->join('project p', 'tv.id_project = p.id_project');
		$this->db->join('customer c', 'p.id_customer = c.id_customer');
		$this->db->join('vendor v', 'tv.id_vendor = v.id_vendor');
		$this->db->join('sales s', 'p.id_sales = s.id_sales');
		$this->db->where('p.project_year', $tahun);
		$this->db->where('p.project_deleted', 0);
		$this->db->where('tv.fpt_sent !=', null);
		$this->db->where('tv.tgl_nego', null);
		return $this->db->get();
	}

	function get_data_2($tahun)
	{
		$this->db->select("date_format(p.created_at,'%m') as bulan,p.kode_project,(select no_fpt from project_vendor spv where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian) as no_fpt,c.nama_customer,p.nama_project,tv.tipe_pembelian as tipe_pembelian_vendor, (select nama_vendor from project_vendor spv join vendor sv on spv.id_vendor = sv.id_vendor where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian and spv.status = 'W' limit 1) as winner_vendor, s.nama_sales, (select nilai_quotation_akhir from project_vendor spv where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian and spv.status = 'W' limit 1) as nilai_quotation_akhir");
		$this->db->from('tender_vendor tv');
		$this->db->join('project p', 'tv.id_project = p.id_project');
		$this->db->join('customer c', 'p.id_customer = c.id_customer');
		$this->db->join('sales s', 'p.id_sales = s.id_sales');
		$this->db->where('p.project_year', $tahun);
		$this->db->where('tv.tgl_nego !=', null);
		$this->db->where('p.status_project', 'P');
		$this->db->where('p.project_deleted', 0);
		return $this->db->get();
	}

	function get_data_3($tahun)
	{
		$this->db->select("date_format(p.created_at,'%m') as bulan,p.kode_project,(select no_fpt from project_vendor spv where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian) as no_fpt,c.nama_customer,p.nama_project,tv.tipe_pembelian as tipe_pembelian_vendor, (select nama_vendor from project_vendor spv join vendor sv on spv.id_vendor = sv.id_vendor where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian and spv.status = 'W' limit 1) as winner_vendor, tv.contract_po_create, tv.p3_fps_create");
		$this->db->from('tender_vendor tv');
		$this->db->join('project p', 'tv.id_project = p.id_project');
		$this->db->join('customer c', 'p.id_customer = c.id_customer');
		$this->db->join('sales s', 'p.id_sales = s.id_sales');
		$this->db->where('p.project_year', $tahun);
		$this->db->where('tv.sp_release', null);
		$this->db->where('p.status_project', 'W');
		$this->db->where('p.project_deleted', 0);
		return $this->db->get();
	}

	function get_data_4($tahun)
	{
		$this->db->select("date_format(p.created_at,'%m') as bulan,p.kode_project,(select no_fpt from project_vendor spv where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian) as no_fpt,c.nama_customer,p.nama_project,tv.tipe_pembelian as tipe_pembelian_vendor, (select nama_vendor from project_vendor spv join vendor sv on spv.id_vendor = sv.id_vendor where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian and spv.status = 'W' limit 1) as winner_vendor, tv.contract_po_create, tv.p3_fps_create");
		$this->db->from('tender_vendor tv');
		$this->db->join('project p', 'tv.id_project = p.id_project');
		$this->db->join('customer c', 'p.id_customer = c.id_customer');
		$this->db->join('sales s', 'p.id_sales = s.id_sales');
		$this->db->where('p.project_year', $tahun);
		$this->db->where('tv.spk_fps_approve', null);
		$this->db->where('p.status_project', 'W');
		$this->db->where('p.project_deleted', 0);
		return $this->db->get();
	}

	function get_data_5($tahun)
	{
		$this->db->select("date_format(p.created_at,'%m') as bulan,p.kode_project,(select no_fpt from project_vendor spv where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian) as no_fpt,c.nama_customer,p.nama_project,tv.tipe_pembelian as tipe_pembelian_vendor, (select nama_vendor from project_vendor spv join vendor sv on spv.id_vendor = sv.id_vendor where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian and spv.status = 'W' limit 1) as winner_vendor, tv.contract_po_create, tv.p3_fps_create");
		$this->db->from('tender_vendor tv');
		$this->db->join('project p', 'tv.id_project = p.id_project');
		$this->db->join('customer c', 'p.id_customer = c.id_customer');
		$this->db->join('sales s', 'p.id_sales = s.id_sales');
		$this->db->where('p.project_year', $tahun);
		$this->db->where('tv.contract_po_release', null);
		$this->db->where('p.project_deleted', 0);
		// $this->db->where('p.status_project', 'W');
		return $this->db->get();
	}

	function get_data_6($tahun)
	{
		$this->db->select("date_format(p.created_at,'%m') as bulan,p.kode_project,(select no_fpt from project_vendor spv where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian) as no_fpt,c.nama_customer,p.nama_project,tv.tipe_pembelian as tipe_pembelian_vendor, (select nama_vendor from project_vendor spv join vendor sv on spv.id_vendor = sv.id_vendor where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian and spv.status = 'W' limit 1) as winner_vendor, tv.contract_po_create, tv.p3_fps_create");
		$this->db->from('tender_vendor tv');
		$this->db->join('project p', 'tv.id_project = p.id_project');
		$this->db->join('customer c', 'p.id_customer = c.id_customer');
		$this->db->join('sales s', 'p.id_sales = s.id_sales');
		$this->db->where('p.project_year', $tahun);
		$this->db->where('tv.sp_release !=', null);
		$this->db->where('tv.contract_po_release', null);
		$this->db->where('p.project_deleted', 0);
		// $this->db->where('p.status_project', 'W');
		return $this->db->get();
	}

	function get_data_7($tahun)
	{
		$this->db->select("date_format(p.created_at,'%m') as bulan,p.kode_project,(select no_fpt from project_vendor spv where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian) as no_fpt,c.nama_customer,p.nama_project,tv.tipe_pembelian as tipe_pembelian_vendor, s.nama_sales");
		$this->db->from('tender_vendor tv');
		$this->db->join('project p', 'tv.id_project = p.id_project');
		$this->db->join('customer c', 'p.id_customer = c.id_customer');
		$this->db->join('sales s', 'p.id_sales = s.id_sales');
		$this->db->where('p.project_year', $tahun);
		$this->db->where('tv.no_spk_fps', null);
		$this->db->where('p.project_deleted', 0);
		// $this->db->where('p.status_project', 'W');
		return $this->db->get();
	}

	function get_data_8($tahun)
	{
		$this->db->select("date_format(p.created_at,'%m') as bulan,p.kode_project,(select no_fpt from project_vendor spv where spv.id_project = tv.id_project and spv.id_vendor = tv.id_vendor and spv.tipe_pembelian = tv.tipe_pembelian) as no_fpt,c.nama_customer,p.nama_project,tv.tipe_pembelian as tipe_pembelian_vendor, v.nama_vendor");
		$this->db->from('tender_vendor tv');
		$this->db->join('project p', 'tv.id_project = p.id_project');
		$this->db->join('customer c', 'p.id_customer = c.id_customer');
		$this->db->join('vendor v', 'tv.id_vendor = v.id_vendor');
		// $this->db->join('sales s', 'p.id_sales = s.id_sales');
		$this->db->where('p.project_year', $tahun);
		$this->db->where('p.status_project', 'P');
		$this->db->where('p.project_deleted', 0);
		return $this->db->get();
	}

	function get_data_9($tahun)
	{
		$this->db->select("date_format(p.created_at,'%m') as bulan,p.kode_project,pv.no_fpt,c.nama_customer,p.nama_project,tv.tipe_pembelian as tipe_pembelian_vendor, v.nama_vendor,tv.v_commitment_start, tv.v_commitment_finish");
		$this->db->from('tender_vendor tv');
		$this->db->join('project_vendor pv', 'pv.id_vendor = tv.id_vendor and tv.id_project = pv.id_project and tv.tipe_pembelian = pv.tipe_pembelian');
		$this->db->join('project p', 'tv.id_project = p.id_project');
		$this->db->join('customer c', 'p.id_customer = c.id_customer');
		$this->db->join('vendor v', 'tv.id_vendor = v.id_vendor');
		$this->db->where('p.project_year', $tahun);
		$this->db->where('tv.tipe_pembelian', 'B');
		$this->db->where('pv.status', 'W');
		$this->db->where('tv.contract_po_create !=', null);
		$this->db->where('p.project_deleted', 0);
		return $this->db->get();
	}

	function get_data_10($tahun)
	{
		$this->db->select("date_format(p.created_at,'%m') as bulan,p.kode_project,pv.no_fpt,c.nama_customer,p.nama_project,tv.tipe_pembelian as tipe_pembelian_vendor, v.nama_vendor,tv.v_commitment_start, tv.v_commitment_finish");
		$this->db->from('tender_vendor tv');
		$this->db->join('project_vendor pv', 'pv.id_vendor = tv.id_vendor and tv.id_project = pv.id_project and tv.tipe_pembelian = pv.tipe_pembelian');
		$this->db->join('project p', 'tv.id_project = p.id_project');
		$this->db->join('customer c', 'p.id_customer = c.id_customer');
		$this->db->join('vendor v', 'tv.id_vendor = v.id_vendor');
		$this->db->where('p.project_year', $tahun);
		$this->db->where('tv.tipe_pembelian', 'J');
		$this->db->where('pv.status', 'W');
		$this->db->where('tv.contract_po_create !=', null);
		$this->db->where('p.project_deleted', 0);
		return $this->db->get();
	}

}

/* End of file Monitoring_model.php */
/* Location: ./application/models/Monitoring_model.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bidang_usaha_model extends CI_Model {

	private $table= 'bidang_usaha';

	function get_data()
	{
		$this->db->order_by('nama_bidang_usaha', 'asc');
		return $this->db->get('bidang_usaha');
	}

	function get_by_id($id)
	{
		$this->db->where('id_bidang_usaha', $id);
		return $this->db->get($this->table);
	}

	function save($data){
		$insert = $this->db->insert($this->table, $data);
		return $insert;
	}

	function update($where,$data){
		$this->db->where($where);
		$update = $this->db->update($this->table, $data);
		return $update;
	}

	function delete($where)
	{
		$this->db->where($where);
		$delete = $this->db->delete($this->table);
		return $delete;
	}

}

/* End of file Bidang_usaha_model.php */
/* Location: ./application/models/Bidang_usaha_model.php */
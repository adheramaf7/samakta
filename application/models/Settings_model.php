<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_Model {

	function get_settings($id_settings,$value_only=false)
	{
		$this->db->where('id_settings', $id_settings);
		$result = $this->db->get('settings');
		if ($result->num_rows() > 0) {
			$res_row = $result->row();
			if ($value_only==true) {
				return $res_row->setting_value;
			}
			return $res_row->setting_value? $res_row->setting_value : $res_row->setting_default;
		}
		return null;
	}

	function save_settings($where,$data)
	{
		$this->db->where($where);
		return $this->db->update('settings', $data);
	}

	function save_target($data)
	{
		$query = "insert into target_perusahaan (tahun_target, nilai_target,nilai_target_one_time,nilai_target_recurring) values (?,?,?,?) on duplicate key update nilai_target = ?, nilai_target_one_time = ?, nilai_target_recurring = ?";
		return $this->db->query($query,[
			$data['tahun_target'],
			$data['nilai_target'],
			$data['nilai_target_one_time'],
			$data['nilai_target_recurring'],
			$data['nilai_target'],
			$data['nilai_target_one_time'],
			$data['nilai_target_recurring'],
		]);
	}

	function load_target($tahun)
	{
		$this->db->where('tahun_target', $tahun);
		return $this->db->get('target_perusahaan');
	}

	function get_data_target()
	{
		$this->db->select("target_perusahaan.*, get_sum_target(tahun_target,'O') as pencapaian_one_time, get_sum_target(tahun_target,'R') pencapaian_recurring");
		$this->db->from('target_perusahaan');
		$this->db->where('tahun_target >= YEAR(CURRENT_TIMESTAMP)-1 and tahun_target <= YEAR(CURRENT_TIMESTAMP)');
		return $this->db->get();
	}

}

/* End of file Settings_model.php */
/* Location: ./application/models/Settings_model.php */
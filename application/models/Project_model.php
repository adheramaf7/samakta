<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project_model extends CI_Model {

	private $table= 'project';

	function get_data($bulan = 'A', $tahun = null)
	{
		$this->db->select('kode_project,nama_project,nama_customer, project_start_date,project_end_date,status_project, (select count(distinct id_vendor) from project_vendor where id_project = project.id_project) as jumlah_vendor, id_project,tgl_pengisian_tender');
		$this->db->join('customer', 'customer.id_customer = project.id_customer');
		$this->db->where('project_deleted', 0);
		if ($bulan!= 'A' && $bulan != null) {
			$this->db->where("date_format(project.created_at,'%Y-%m')", $tahun.'-'.$bulan);
		}
		if ($bulan == 'A' || $bulan == null && $tahun != null) {
			$this->db->where("date_format(project.created_at,'%Y')", $tahun);
		}
		$this->db->order_by('created_at', 'asc');
		return $this->db->get('project');
	}

	function get_data_project_vendor($id_project)
	{
		$this->db->select("pv.id_project,pv.id_vendor,pv.tipe_pembelian,pv.tipe_project_vendor, CONCAT(pv.id_vendor,'',pv.tipe_pembelian) as id, v.nama_vendor, pv.no_quotation, pv.nilai_quotation_awal,pv.nilai_quotation_akhir,expired_quotation,pv.status,pv.keterangan,pv.lampiran");
		$this->db->from('project_vendor pv');
		$this->db->join('vendor v', 'pv.id_vendor = v.id_vendor','left');
		$this->db->where('id_project', $id_project);
		return $this->db->get();
	}


	function get_by_id($id)
	{
		$this->db->select('project.*');
		$this->db->where('id_project', $id);
		return $this->db->get($this->table);
	}


	function save($data){
		$insert = $this->db->insert($this->table, $data);
		return $insert;
	}

	function update($where,$data){
		$this->db->where($where);
		$update = $this->db->update($this->table, $data);
		return $update;
	}

	function delete($where)
	{
		$this->db->where($where);
		$delete = $this->db->delete($this->table);
		return $delete;
	}

	function delete_vendor($where)
	{
		$this->db->where($where);
		$delete = $this->db->delete('project_vendor');
		return $delete;
	}

	function delete_tender($where)
	{
		$this->db->where($where);
		$delete = $this->db->delete('tender_vendor');
		return $delete;
	}

	function delete_project($where)
	{
		$data = [
			'project_deleted' => 1
		];
		$this->db->where($where);
		$update = $this->db->update($this->table, $data);
		return $update;
	}

	function insert_vendor($data)
	{
		$query = 'insert into project_vendor (id_project,id_vendor,tipe_pembelian,tipe_project_vendor,no_quotation,nilai_quotation_awal,nilai_quotation_akhir,expired_quotation,status,keterangan,lampiran) values (?,?,?,?,?,?,?,?,?,?,?) on duplicate key update no_quotation = ?, nilai_quotation_awal = ?, nilai_quotation_akhir = ?, expired_quotation = ?, status = ?, keterangan = ?, lampiran = ?';
		$jalankan = $this->db->query($query,
			[
				$data['id_project'],
				$data['id_vendor'],
				$data['tipe_pembelian'],
				$data['tipe_project_vendor'],
				$data['no_quotation'],
				$data['nilai_quotation_awal'],
				$data['nilai_quotation_akhir'],
				$data['expired_quotation'],
				$data['status'],
				$data['keterangan'],
				$data['lampiran'],
				$data['no_quotation'],
				$data['nilai_quotation_awal'],
				$data['nilai_quotation_akhir'],
				$data['expired_quotation'],
				$data['status'],
				$data['keterangan'],
				$data['lampiran']
			]);
		return $jalankan;
	}

	function sinkron_project_vendor($id_project,$not_in)
	{
		$this->db->where('id_project',$id_project);
		$this->db->where_not_in('CONCAT(id_vendor,tipe_pembelian)', $not_in);
		$delete = $this->db->delete('project_vendor');
		return $delete;
	}

	function insert_tender_vendor($data)
	{
		$query = "insert ignore into tender_vendor (id_project,id_vendor,tipe_pembelian) values (?,?,?)";
		return $this->db->query($query,[$data['id_project'],$data['id_vendor'],$data['tipe_pembelian']]);
	}

	function update_tender_vendor($where,$data)
	{
		$this->db->where($where);
		$update = $this->db->update('tender_vendor', $data);
		return $update;
	}


	function sinkron_tender_project_vendor($id_project,$not_in)
	{
		$this->db->where('id_project',$id_project);
		$this->db->where_not_in('CONCAT(id_vendor,tipe_pembelian)', $not_in);
		$delete = $this->db->delete('tender_vendor');
		return $delete;
	}

	function get_data_product()
	{
		$this->db->where('status_product', 'A');
		$this->db->order_by('nama_product');
		return $this->db->get('product');
	}

	function get_data_vendor()
	{
		$this->db->where('status_vendor', 'A');
		$this->db->order_by('nama_vendor');
		return $this->db->get('vendor');
	}

	function get_data_sales()
	{
		$this->db->where('status_sales', 'A');
		$this->db->order_by('nik_sales', 'asc');
		return $this->db->get('sales');
	}

	function get_data_customer()
	{
		$this->db->select('id_customer,nama_customer,nama_tipe_customer');
		$this->db->join('tipe_customer', 'tipe_customer.id_tipe_customer = customer.id_tipe_customer');
		$this->db->where('status_customer', 'A');
		$this->db->order_by('nama_tipe_customer', 'asc');
		$this->db->order_by('nama_customer', 'asc');
		return $this->db->get('customer');
	}


	function get_detil_customer($id_customer = null)
	{
		$this->db->select('email_customer, telp_customer, path_foto_customer');
		$this->db->where('id_customer', $id_customer);
		return $this->db->get('customer');
	}

	function get_detil_sales($id_sales = null)
	{
		$this->db->where('id_sales', $id_sales);
		return $this->db->get('sales');
	}

	function get_data_tender($id_project)
	{
		$this->db->select('id_project,kode_project,nama_project,nama_customer, project_start_date,project_end_date,status_project,tipe_pembelian,no_fs_barang,spk_fs_barang,tgl_fs_barang,no_fs_jasa,spk_fs_jasa,tgl_fs_jasa,path_foto_customer,tgl_pengisian_tender,nilai_project,tgl_fs_approved,tgl_project_final,nilai_project_barang,nilai_project_jasa');
		$this->db->join('customer', 'customer.id_customer = project.id_customer');		
		$this->db->where('id_project', $id_project);
		return $this->db->get('project');
	}

	function get_data_tender_vendor($data)
	{
		$this->db->select('id_project,id_vendor,tipe_pembelian,no_po_spk,v_commitment_start,v_commitment_finish, (select no_fpt from project_vendor where id_project = tv.id_project and id_vendor = tv.id_vendor and tipe_pembelian = tv.tipe_pembelian) as no_fpt,(select tipe_project_vendor from project_vendor where id_project = tv.id_project and id_vendor = tv.id_vendor and tipe_pembelian = tv.tipe_pembelian) as tipe_project_vendor,no_spk_fps,no_contract_po');
		$this->db->where('id_project', $data['id_project']);
		$this->db->where('id_vendor', $data['id_vendor']);
		$this->db->where('tipe_pembelian', $data['tipe_pembelian']);
		return $this->db->get('tender_vendor tv');
	}

	function get_data_tgl_tender_vendor($data)
	{
		$this->db->select('spl_create,spl_internal_approve,spl_full_approve,fpt_sent,tgl_nego,wbs_create,p3_fps_create,p3_fps_release,sp_release,spk_fps_create,spk_fps_approve,contract_po_create,contract_po_release,p3_fpb_cancel,cancel_vendor');
		$this->db->where('id_project', $data['id_project']);
		$this->db->where('id_vendor', $data['id_vendor']);
		$this->db->where('tipe_pembelian', $data['tipe_pembelian']);
		return $this->db->get('tender_vendor tv');
	}

	function get_list_vendor_cetak($id_project,$tipe_pembelian)
	{
		$this->db->select('nama_vendor,p3_fps_release,sp_release,spk_fps_approve,v_commitment_start,v_commitment_finish,nilai_quotation_akhir,no_contract_po,no_spk_fps');
		$this->db->from('project_vendor a');
		$this->db->join('tender_vendor b', '(a.id_project = b.id_project and a.id_vendor = b.id_vendor and a.tipe_pembelian = b.tipe_pembelian)',);
		$this->db->join('vendor c', 'a.id_vendor = c.id_vendor', 'left');
		$this->db->where('a.id_project', $id_project);
		$this->db->where('a.tipe_pembelian', $tipe_pembelian);
		return $this->db->get();
	}


}

/* End of file Project_model.php */
/* Location: ./application/models/Project_model.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('my_helper'))
{
	function cap_each_word($string)
	{
		return ucwords(strtolower($string));
	}

	function encrypt_str($string) {
	    $output = false;
	    /*
	    * read security.ini file & get encryption_key | iv | encryption_mechanism value for generating encryption code
	    */        
	    $security       = parse_ini_file("security.ini");
	    $secret_key     = $security["encryption_key"];
	    $secret_iv      = $security["iv"];
	    $encrypt_method = $security["encryption_mechanism"];
	    // hash
	    $key    = hash("sha256", $secret_key);
	    // iv – encrypt method AES-256-CBC expects 16 bytes – else you will get a warning
	    $iv     = substr(hash("sha256", $secret_iv), 0, 16);
	    //do the encryption given text/string/number
	    $result = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
	    $output = base64_encode($result);
	    return $output;
	}
	
	function decrypt_str($string) {
	    $output = false;
	    /*
	    * read security.ini file & get encryption_key | iv | encryption_mechanism value for generating encryption code
	    */
	    $security       = parse_ini_file("security.ini");
	    $secret_key     = $security["encryption_key"];
	    $secret_iv      = $security["iv"];
	    $encrypt_method = $security["encryption_mechanism"];
	    // hash
	    $key    = hash("sha256", $secret_key);
	    // iv – encrypt method AES-256-CBC expects 16 bytes – else you will get a warning
	    $iv = substr(hash("sha256", $secret_iv), 0, 16);
	    //do the decryption given text/string/number
	    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	    return $output;
	}

	function tanggal_full($tgl)
	{
		$bulanIndo = array('01' => 'Januari' ,
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember' );
		$dd = date_format(date_create($tgl),"d");
		$mm = date_format(date_create($tgl),"m");
		$yy = date_format(date_create($tgl),"Y");
		return $dd.' '.$bulanIndo[$mm].' '.$yy; 
	}

	function list_bulan()
	{
		$bulanIndo = [];
		array_push($bulanIndo,["id" => "01","name" => "Januari"]);
		array_push($bulanIndo,["id" => "02","name" => "Februari"]);
		array_push($bulanIndo,["id" => "03","name" => "Maret"]);
		array_push($bulanIndo,["id" => "04","name" => "April"]);
		array_push($bulanIndo,["id" => "05","name" => "Mei"]);
		array_push($bulanIndo,["id" => "06","name" => "Juni"]);
		array_push($bulanIndo,["id" => "07","name" => "Juli"]);
		array_push($bulanIndo,["id" => "08","name" => "Agustus"]);
		array_push($bulanIndo,["id" => "09","name" => "September"]);
		array_push($bulanIndo,["id" => "10","name" => "Oktober"]);
		array_push($bulanIndo,["id" => "11","name" => "November"]);
		array_push($bulanIndo,["id" => "12","name" => "Desember"]);
		return $bulanIndo;
	}

	function to_date_format_mysql($tgl)
	{
		$str = explode("-", $tgl);
		return $str[2].'-'.$str[1].'-'.$str[0];
	}

	function get_tahun($date)
	{
		return substr($date,0,4);
	}

	function get_nama_bulan($date)
	{
		$bulan = substr($date,5,2);
		$bulanIndo = array('01' => 'Januari' ,
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember' );
		return $bulanIndo[$bulan];
	}

	function get_nama_hari($tanggal)
	{
		$nama_hari = array(
			'0' => 'Minggu',
			'1' => 'Senin',
			'2' => 'Selasa',
			'3' => 'Rabu',
			'4' => 'Kamis',
			'5' => 'Jumat',
			'6' => 'Sabtu'
		);

		$hari = date_format(date_create($tanggal),'w');
		return $nama_hari[$hari];
	}

	function terbilang($satuan){ 
		$huruf = array ( 0 => "", 1=> "Satu", 2 => "Dua", 3 => "Tiga", 4 => "Empat", 5=>"Lima",6=>"Enam",7=>"Tujuh",8=>"Delapan",9=>"Sembilan",10=>"Sepuluh",11=>"Sebelas"); 

		$hasil=""; 
		if($satuan < 12) {
			$hasil = $hasil . $huruf[$satuan]; 
		}else if($satuan < 20) {
			$hasil = $hasil . terbilang($satuan-10)." belas"; 
		}else if($satuan < 100) {
			$hasil= $hasil . terbilang($satuan/10)." puluh " . terbilang($satuan%10); 
		}else if($satuan < 200) {
			$hasil= $hasil . "Seratus " . terbilang($satuan-100); 
		}else if($satuan < 1000) {
			$hasil = $hasil . terbilang($satuan/100). " ratus " . terbilang($satuan%100); 
		}else if($satuan < 2000){
			$hasil = $hasil . "Seribu " . terbilang($satuan-1000); 
		}else if($satuan < 1000000){
			$hasil = $hasil . terbilang($satuan/1000) . " ribu " . terbilang($satuan % 1000); 
		}else if($satuan < 1000000000) {
			$hasil = $hasil . terbilang($satuan/1000000) . " juta " . terbilang($satuan % 1000000); 
		}else if($satuan >= 1000000000){
			$hasil = "Angka terlalu besar, harus kurang dari 1 milyar!"; 
		}
		return $hasil; 
	}

	function acakangkahuruf($panjang)
	{
		$karakter= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		$string = '';
		for ($i = 0; $i < $panjang; $i++) {
			$pos = rand(0, strlen($karakter)-1);
			$string .= $karakter{$pos};
		}
		return $string;
	}

	function acakangka($panjang)
	{
		$karakter= '0123456789';
		$string = '';
		for ($i = 0; $i < $panjang; $i++) {
			$pos = rand(0, strlen($karakter)-1);
			$string .= $karakter{$pos};
		}
		return $string;
	}

	function format_ribuan_indo($nilai, $dibelakang_koma)
	{
		return number_format($nilai,$dibelakang_koma,',','.');
	}

	function integer_to_roman($integer)
	{
	 // Convert the integer into an integer (just to make sure)
		 $integer = intval($integer);
		 $result = '';
		 
		 // Create a lookup array that contains all of the Roman numerals.
		 $lookup = array('M' => 1000,
		 'CM' => 900,
		 'D' => 500,
		 'CD' => 400,
		 'C' => 100,
		 'XC' => 90,
		 'L' => 50,
		 'XL' => 40,
		 'X' => 10,
		 'IX' => 9,
		 'V' => 5,
		 'IV' => 4,
		 'I' => 1);
		 
		 foreach($lookup as $roman => $value){
		  // Determine the number of matches
		  $matches = intval($integer/$value);
		 
		  // Add the same number of characters to the string
		  $result .= str_repeat($roman,$matches);
		 
		  // Set the integer to be the remainder of the integer and the value
		  $integer = $integer % $value;
		 }
		 
		 // The Roman numeral should be built, return it
		 return $result;
	}

	function roman_to_integer($roman)
	{
		$romans = array(
		    'M' => 1000,
		    'CM' => 900,
		    'D' => 500,
		    'CD' => 400,
		    'C' => 100,
		    'XC' => 90,
		    'L' => 50,
		    'XL' => 40,
		    'X' => 10,
		    'IX' => 9,
		    'V' => 5,
		    'IV' => 4,
		    'I' => 1,
		);
		$result = 0;

		foreach ($romans as $key => $value) {
		    while (strpos($roman, $key) === 0) {
		        $result += $value;
		        $roman = substr($roman, strlen($key));
		    }
		}
		return $result;
	}

}
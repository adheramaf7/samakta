<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class Excel {

	public function Spreadsheet(){
        $spreadsheet = new Spreadsheet();

		return $spreadsheet;
	}

	public function Xlsx($spreadsheet){
        $writer = new Xlsx($spreadsheet);

        return $writer;
	}

	public function Worksheet($spreadsheet,$name){
		$worksheet= new Worksheet($spreadsheet,$name);

		return $worksheet;
	}

}
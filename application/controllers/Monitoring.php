<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('id')) redirect('login');
		$this->load->model('monitoring_model', 'model');
	}

	function get_data_1()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$tahun = $this->input->get('tahun');
		$list = $this->model->get_data_1($tahun);

		// var_dump($this->db->last_query());

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = $value['bulan'];
				$data['data'][$key][] = $value['kode_project'];
				$data['data'][$key][] = $value['no_fpt'];
				$data['data'][$key][] = $value['nama_customer'];
				$data['data'][$key][] = $value['nama_project'];
				$data['data'][$key][] = $value['tipe_pembelian_vendor'];
				$data['data'][$key][] = $value['nilai_fs'];
				$data['data'][$key][] = $value['nama_vendor'];
				$data['data'][$key][] = $value['fpt_sent'];
				$data['data'][$key][] = $value['nama_sales'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_2()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$tahun = $this->input->get('tahun');
		$list = $this->model->get_data_2($tahun);

		// var_dump($this->db->last_query());

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = $value['bulan'];
				$data['data'][$key][] = $value['kode_project'];
				$data['data'][$key][] = $value['no_fpt'];
				$data['data'][$key][] = $value['nama_customer'];
				$data['data'][$key][] = $value['nama_project'];
				$data['data'][$key][] = $value['tipe_pembelian_vendor'];
				$data['data'][$key][] = $value['winner_vendor'];
				$data['data'][$key][] = $value['nama_sales'];
				$data['data'][$key][] = $value['nilai_quotation_akhir'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_3()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$tahun = $this->input->get('tahun');
		$list = $this->model->get_data_3($tahun);

		// var_dump($this->db->last_query());

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = $value['bulan'];
				$data['data'][$key][] = $value['kode_project'];
				$data['data'][$key][] = $value['no_fpt'];
				$data['data'][$key][] = $value['nama_customer'];
				$data['data'][$key][] = $value['nama_project'];
				$data['data'][$key][] = $value['tipe_pembelian_vendor'];
				$data['data'][$key][] = $value['winner_vendor'];
				$data['data'][$key][] = $value['contract_po_create'];
				$data['data'][$key][] = $value['p3_fps_create'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_4()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$tahun = $this->input->get('tahun');
		$list = $this->model->get_data_4($tahun);

		// var_dump($this->db->last_query());

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = $value['bulan'];
				$data['data'][$key][] = $value['kode_project'];
				$data['data'][$key][] = $value['no_fpt'];
				$data['data'][$key][] = $value['nama_customer'];
				$data['data'][$key][] = $value['nama_project'];
				$data['data'][$key][] = $value['tipe_pembelian_vendor'];
				$data['data'][$key][] = $value['winner_vendor'];
				// $data['data'][$key][] = $value['contract_po_create'];
				// $data['data'][$key][] = $value['p3_fps_create'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_5()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$tahun = $this->input->get('tahun');
		$list = $this->model->get_data_5($tahun);

		// var_dump($this->db->last_query());

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = $value['bulan'];
				$data['data'][$key][] = $value['kode_project'];
				$data['data'][$key][] = $value['no_fpt'];
				$data['data'][$key][] = $value['nama_customer'];
				$data['data'][$key][] = $value['nama_project'];
				$data['data'][$key][] = $value['tipe_pembelian_vendor'];
				$data['data'][$key][] = $value['winner_vendor'];
				// $data['data'][$key][] = $value['contract_po_create'];
				// $data['data'][$key][] = $value['p3_fps_create'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_6()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$tahun = $this->input->get('tahun');
		$list = $this->model->get_data_6($tahun);

		// var_dump($this->db->last_query());

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = $value['bulan'];
				$data['data'][$key][] = $value['kode_project'];
				$data['data'][$key][] = $value['no_fpt'];
				$data['data'][$key][] = $value['nama_customer'];
				$data['data'][$key][] = $value['nama_project'];
				$data['data'][$key][] = $value['tipe_pembelian_vendor'];
				$data['data'][$key][] = $value['winner_vendor'];
				// $data['data'][$key][] = $value['contract_po_create'];
				// $data['data'][$key][] = $value['p3_fps_create'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_7()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$tahun = $this->input->get('tahun');
		$list = $this->model->get_data_7($tahun);

		// var_dump($this->db->last_query());

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = $value['bulan'];
				$data['data'][$key][] = $value['kode_project'];
				$data['data'][$key][] = $value['no_fpt'];
				$data['data'][$key][] = $value['nama_customer'];
				$data['data'][$key][] = $value['nama_project'];
				$data['data'][$key][] = $value['tipe_pembelian_vendor'];
				$data['data'][$key][] = $value['nama_sales'];
				// $data['data'][$key][] = $value['contract_po_create'];
				// $data['data'][$key][] = $value['p3_fps_create'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_8()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$tahun = $this->input->get('tahun');
		$list = $this->model->get_data_8($tahun);

		// var_dump($this->db->last_query());

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = $value['bulan'];
				$data['data'][$key][] = $value['kode_project'];
				$data['data'][$key][] = $value['no_fpt'];
				$data['data'][$key][] = $value['nama_customer'];
				$data['data'][$key][] = $value['nama_project'];
				$data['data'][$key][] = $value['tipe_pembelian_vendor'];
				$data['data'][$key][] = $value['nama_vendor'];
				// $data['data'][$key][] = $value['contract_po_create'];
				// $data['data'][$key][] = $value['p3_fps_create'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_9()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$tahun = $this->input->get('tahun');
		$list = $this->model->get_data_9($tahun);

		// var_dump($this->db->last_query());

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = $value['bulan'];
				$data['data'][$key][] = $value['kode_project'];
				$data['data'][$key][] = $value['no_fpt'];
				$data['data'][$key][] = $value['nama_customer'];
				$data['data'][$key][] = $value['nama_project'];
				$data['data'][$key][] = $value['tipe_pembelian_vendor'];
				$data['data'][$key][] = $value['nama_vendor'];
				$data['data'][$key][] = $value['v_commitment_start'];
				$data['data'][$key][] = $value['v_commitment_finish'];
				// $data['data'][$key][] = $value['contract_po_create'];
				// $data['data'][$key][] = $value['p3_fps_create'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_10()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$tahun = $this->input->get('tahun');
		$list = $this->model->get_data_10($tahun);

		// var_dump($this->db->last_query());

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = $value['bulan'];
				$data['data'][$key][] = $value['kode_project'];
				$data['data'][$key][] = $value['no_fpt'];
				$data['data'][$key][] = $value['nama_customer'];
				$data['data'][$key][] = $value['nama_project'];
				$data['data'][$key][] = $value['tipe_pembelian_vendor'];
				$data['data'][$key][] = $value['nama_vendor'];
				$data['data'][$key][] = $value['v_commitment_start'];
				$data['data'][$key][] = $value['v_commitment_finish'];
				// $data['data'][$key][] = $value['contract_po_create'];
				// $data['data'][$key][] = $value['p3_fps_create'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

}

/* End of file Monitoring.php */
/* Location: ./application/controllers/Monitoring.php */
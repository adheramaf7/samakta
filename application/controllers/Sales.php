<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {
	
	private $path_upload_foto = 'assets/upload/sales/foto/';

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('id')) redirect('login');
		$this->load->model('sales_model','model');
	}

	function get_data()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$list = $this->model->get_data();

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = ($key + 1) . '.';
				$data['data'][$key][] = $value['nik_sales'];
				$data['data'][$key][] = $value['nama_sales'];
				$data['data'][$key][] = $value['telp_sales'];
				$data['data'][$key][] = $value['email_sales'];
				$data['data'][$key][] = $value['nama_jabatan'];
				$data['data'][$key][] = $value['status_sales'];
				$data['data'][$key][] = $value['id_sales'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_by_id()
	{
		if(!$this->input->is_ajax_request()) redirect();
		
		$id = $this->input->post('id');
		$data = $this->model->get_by_id($id)->row();

		echo json_encode($data);
	}

	function save()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$response =	[
						'status' => false,
						'pesan' => '',
					];
			
		
		$data_array = array(
			'id_sales' => $this->input->post('id_sales') ? $this->input->post('id_sales') : null,
			'id_jabatan' => $this->input->post('id_jabatan'),
			'nik_sales' => $this->input->post('nik_sales'),
			'nama_sales' => $this->input->post('nama_sales'),
			'telp_sales' => $this->input->post('telp_sales'),
			'email_sales' => $this->input->post('email_sales'),
			'status_sales' => $this->input->post('status_sales'),
		);

		if ($data_array['id_sales']) {
			$nik_exist = $this->model->nik_exist($data_array['nik_sales'],"update",$data_array['id_sales']);

			if (!$nik_exist) {
				if ($this->input->post('hapus_foto')) {
					$data_array['path_foto_sales'] = null;
					$folder = $this->path_upload_foto.$data_array['id_sales'];
					$this->delete_all_files($folder);
				}

				if (!empty($_FILES['upload_foto']['name'])) {
					$folder = $this->path_upload_foto.$data_array['id_sales'];
					$this->delete_all_files($folder);
					$upload_action = $this->_do_upload_foto($data_array['id_sales'],'Foto sales-'.$data_array['id_sales'].'');
					if (!$upload_action['status']) {
						echo json_encode($upload_action);
						exit;
					}
					$data_array['path_foto_sales'] = $upload_action['pesan'];
				}

				$exec = $this->model->update(['id_sales' => $data_array['id_sales']],$data_array);
				$response['status'] = $exec;
				$response['pesan'] = '';
			}
			$response['pesan'] = 'NIK sudah digunakan';
		} else{
			$nik_exist = $this->model->nik_exist($data_array['nik_sales']);
			if (!$nik_exist) {
				$exec = $this->model->save($data_array);
				$id_sales = $this->db->insert_id();

				if (!empty($_FILES['upload_foto']['name'])) {
					$upload_action = $this->_do_upload_foto($id_sales,'Foto sales-'.$id_sales.'');
					if (!$upload_action['status']) {
						echo json_encode($upload_action);
						exit;
					}
					$data_upload['path_foto_sales'] = $upload_action['pesan'];
					$exec = $this->model->update(['id_sales' => $id_sales], $data_upload);
				}
				$response['status'] = $exec;
				$response['pesan'] = '';
			}
			$response['pesan'] = 'NIK sudah terdaftar';
		}


		echo json_encode($response);
	}

	function delete()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$exec = $this->model->delete(['id_sales' => $this->input->post('id')]);

		if ($exec) {
			$folder_foto = $this->path_upload_foto.$this->input->post('id');
			if (file_exists($folder_foto)) {
	    		$this->delete_all_files($folder_foto);
	    		rmdir($folder_foto);
    		}
		}

		echo json_encode(
			['status' => $exec]
		);
		
	}


	function get_jabatan()
	{
		if(!$this->input->is_ajax_request()) redirect();
		
		$list = $this->model->get_data_jabatan();

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['id_jabatan'];
				$data[$key]['name'] = $value['nama_jabatan'];
			}
		}

		echo json_encode($data);
	}

	private function _do_upload_foto($id_sales, $filename)
	{
		$path = $this->path_upload_foto . $id_sales;
		$result_upload = [
			'status' => false,
			'pesan' => '',
		];

		if (!file_exists($path))
			mkdir($path, 0777, true);

		$config['upload_path']          = $path;
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['max_size']             = 10240; //set max size allowed in Kilobyte
		$config['max_width']            = 10000; // set max width image allowed
		$config['max_height']           = 10000; // set max height allowed
		$config['overwrite']           	= true; // set max height allowed
		$config['file_name']            = round(microtime(true)*1000); //just milisecond timestamp fot unique name

		$this->load->library('upload');
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_foto')) {
			$result_upload['pesan'] = 'Upload Foto error: ' . $this->upload->display_errors('', '');
			return $result_upload;
		}
		$result_upload['status'] = true;
		$result_upload['pesan'] = $this->upload->data('file_name');
		return $result_upload;
	}

	private function delete_all_files($folder)
	{
		//Get a list of all of the file names in the folder.
		$files = glob($folder . '/*');
		 
		//Loop through the file list.
		foreach($files as $file){
		    //Make sure that this is a file and not a directory.
		    if(is_file($file)){
		        //Use the unlink function to delete the file.
		        unlink($file);
		    }
		}
	}

}

/* End of file Sales.php */
/* Location: ./application/controllers/Sales.php */
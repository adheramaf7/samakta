<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	private $jenis_file = 'jpg|png|jpeg|pdf';
	private $path_upload = 'assets/upload/project/';


	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('id')) redirect('login');
		$this->load->model('project_model','model');
	}

	function get_data()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$bulan = $this->input->get('bulan');
		$tahun = $this->input->get('tahun');
		$list = $this->model->get_data($bulan,$tahun);

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = ($key + 1) . '.';
				$data['data'][$key][] = $value['kode_project'];
				$data['data'][$key][] = $value['nama_project'];
				$data['data'][$key][] = $value['nama_customer'];
				$data['data'][$key][] = $value['project_start_date'];
				$data['data'][$key][] = $value['project_end_date'];
				$data['data'][$key][] = $value['status_project'];
				$data['data'][$key][] = $value['jumlah_vendor'];
				$data['data'][$key][] = $value['id_project'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_by_id()
	{
		if(!$this->input->is_ajax_request()) redirect();
		
		$id = $this->input->post('id');
		
		$data = [
		    'project' => $this->model->get_by_id($id)->row(),
		    'vendor'  => $this->model->get_data_project_vendor($id)->result_array(),
		];

		echo json_encode($data);
	}

	function get_data_tender()
	{
		if(!$this->input->is_ajax_request()) redirect();
		
		$id = $this->input->post('id');
		
		$data = $this->model->get_data_tender($id)->row();

		echo json_encode($data);
	}

	function get_data_tender_vendor()
	{
		if(!$this->input->is_ajax_request()) redirect();
		
		$id = $this->input->post('id');

		$data = [
		    'project' => $this->model->get_data_tender($id)->row(),
		    'vendor'  => $this->model->get_data_project_vendor($id)->result_array(),
		];

		echo json_encode($data);
	}


	function load_tender_vendor()
	{
		if(!$this->input->is_ajax_request()) redirect();
		
		$data_kueri = [
			'id_project' => $this->input->post('id_project'),
			'id_vendor' => $this->input->post('id_vendor'),
			'tipe_pembelian' => $this->input->post('tipe_pembelian'),
		];

		$result = $this->model->get_data_tender_vendor($data_kueri)->row();
		$result2 = $this->model->get_data_tgl_tender_vendor($data_kueri)->row();

		echo json_encode([
			'tender_vendor' => $result,
			'tender_vendor_tgl' => $result2,
		]);
	}

	function save()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$exec = false;
		$error = [];
		
		$data_project = (array) json_decode($this->input->post('data_project'));
		$data_vendor = [];
		if ($this->input->post('data_vendor')) {
			foreach ($this->input->post('data_vendor') as $index => $value) {
				array_push($data_vendor, (array) json_decode($value));
			}
		}

		$this->db->trans_begin();

		$project = [
		    'id_project' => $data_project['id_project']? $data_project['id_project'] : null,
		    'nama_project' => $data_project['nama_project'],
		    'project_year' => $data_project['project_year'],
		    'id_product' => $data_project['id_product'],
		    'project_type' => $data_project['project_type'],
		    'tipe_kontrak_project' => $data_project['tipe_kontrak_project'],
		    'project_start_date' => $data_project['project_start_date'],
		    'project_end_date' => $data_project['project_end_date'],
		    'tgl_fs_approved' => $data_project['tgl_fs_approved'],
		    'tgl_project_final' => $data_project['tgl_project_final'],
		    'no_fs_barang' => $data_project['no_fs_barang'],
		    'nilai_fs_barang' => $data_project['nilai_fs_barang'],
		    'nilai_project_barang' => $data_project['nilai_project_barang'],
		    'start_date_barang' => $data_project['start_date_barang'],
		    'end_date_barang' => $data_project['end_date_barang'],
		    'no_fs_jasa' => $data_project['no_fs_jasa'],
		    'nilai_fs_jasa' => $data_project['nilai_fs_jasa'],
		    'nilai_project_jasa' => $data_project['nilai_project_jasa'],
		    'start_date_jasa' => $data_project['start_date_jasa'],
		    'end_date_jasa' => $data_project['end_date_jasa'],
		    'tipe_pembelian' => $data_project['tipe_pembelian'],
		    'id_customer' => $data_project['id_customer'],
		    'nama_pic_project' => $data_project['nama_pic_project'],
		    'no_hp_pic_project' => $data_project['no_hp_pic_project'],
		    'email_pic_project' => $data_project['email_pic_project'],
		    'id_sales' => $data_project['id_sales'],
		];

		if ($data_project['id_project']) {

			$upload_lampiran_vendor = [];
			$lampiran_vendor = null;
			if ($this->input->post('id_vendor_upload')) {
				$lampiran_vendor = $_FILES['lampiran_vendor'];
				
				foreach ($this->input->post('id_vendor_upload') as $index => $value) {
					array_push($upload_lampiran_vendor, $value);
				}
			}

			//update
			$project['modified_by'] = $this->session->username;
			$id_project = $project['id_project'];
			$update_project = $this->model->update(['id_project' => $id_project],$project);

			$path = $this->path_upload . $id_project;
			if (!file_exists($path))
				mkdir($path, 0777, true);

			// $delete_vendor = $this->model->delete_vendor(['id_project' => $id_project]);
			if (sizeof($data_vendor) > 0) {
				$kondisi_sinkron = []; //list project vendor yang masih ada
				foreach ($data_vendor as $index => $value) {
					$vendor = [
					    'id_project' => $id_project,
					    'id_vendor' => $value['id_vendor'],
					    'tipe_pembelian' => $value['tipe_pembelian'],
					    'tipe_project_vendor' => $value['tipe_project_vendor'],
					    'no_quotation' => $value['no_quotation'],
					    'nilai_quotation_awal' => $value['nilai_quotation_awal'],
					    'nilai_quotation_akhir' => $value['nilai_quotation_akhir'],
					    'expired_quotation' => $value['expired_quotation'],
					    'status' => $value['status'],
					    'keterangan' => $value['keterangan'],
					    'lampiran' => $value['lampiran']
					];
					if ($value['lampiran'] == 'upload') {

						$index_lampiran = array_search($value['id_vendor'], $upload_lampiran_vendor);

						$upload = $this->upload_lampiran_vendor($lampiran_vendor,$index_lampiran,$id_project,$vendor,$value['nama_vendor']);
						
						if($upload['status']){
	                    	$vendor['lampiran'] = $upload['pesan'];
						} else{
							array_push($error, $upload['pesan']);
						}
					}

					$insert_vendor = $this->model->insert_vendor($vendor);
					array_push($kondisi_sinkron, ($vendor['id_vendor'].$vendor['tipe_pembelian']));

					$tender_vendor = [
					    'id_project' => $id_project,
					    'id_vendor' => $value['id_vendor'],
					    'tipe_pembelian' => $value['tipe_pembelian'],
					];
                    $insert_tender_vendor = $this->model->insert_tender_vendor($tender_vendor);

				}
				$sinkron_pv = $this->model->sinkron_project_vendor($id_project,$kondisi_sinkron);
                $sinkron_tpv= $this->model->sinkron_tender_project_vendor($id_project,$kondisi_sinkron);
			}

		} else{
			$upload_lampiran_vendor = [];
			$lampiran_vendor = null;
			if ($this->input->post('id_vendor_upload')) {
				$lampiran_vendor = $_FILES['lampiran_vendor'];
				
				foreach ($this->input->post('id_vendor_upload') as $index => $value) {
					array_push($upload_lampiran_vendor, $value);
				}
			}
			//insert

			$project['created_by'] = $this->session->username;
			$project['created_at'] = date('Y-m-d H:i:s');

			$save_project = $this->model->save($project);
			$id_project = $this->db->insert_id();

			$path = $this->path_upload . $id_project;
			if (!file_exists($path))
				mkdir($path, 0777, true);

			if (sizeof($data_vendor) > 0) {
				foreach ($data_vendor as $index => $value) {
					$vendor = [
					    'id_project' => $id_project,
					    'id_vendor' => $value['id_vendor'],
					    'tipe_pembelian' => $value['tipe_pembelian'],
					    'tipe_project_vendor' => $value['tipe_project_vendor'],
					    'no_quotation' => $value['no_quotation'],
					    'nilai_quotation_awal' => $value['nilai_quotation_awal'],
					    'nilai_quotation_akhir' => $value['nilai_quotation_akhir'],
					    'expired_quotation' => $value['expired_quotation'],
					    'status' => $value['status'],
					    'keterangan' => $value['keterangan'],
					    'lampiran' => null
					];
					if ($value['lampiran'] == 'upload') {
						$upload = $this->upload_lampiran_vendor($lampiran_vendor,$index,$id_project,$vendor,$value['nama_vendor']);

						if($upload['status']){
	                    	$vendor['lampiran'] = $upload['pesan'];
						} else{
							array_push($error, $upload['pesan']);
						}
					}
                    $insert_vendor	= $this->model->insert_vendor($vendor);

					$tender_vendor = [
					    'id_project' => $id_project,
					    'id_vendor' => $value['id_vendor'],
					    'tipe_pembelian' => $value['tipe_pembelian'],
					];
                    $insert_tender_vendor = $this->model->insert_tender_vendor($tender_vendor);
				}
			}

		}

		$exec = $this->db->trans_status();

		if ($exec === FALSE && sizeof($error) > 0)
		{
		        $this->db->trans_rollback();
		}
		else
		{
		        $this->db->trans_commit();
		}

		echo json_encode(
			[
				'status' => $exec,
				'error' => $error
			]
		);
	}

	function save_tender()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$exec = false;
		
		$data_array = array(
			'id_project' => $this->input->post('id_project'),
			'spk_fs_barang' => $this->input->post('spk_fs_barang'),
			'tgl_fs_barang' => $this->input->post('tgl_fs_barang'),
			'spk_fs_jasa' => $this->input->post('spk_fs_jasa'),
			'tgl_fs_jasa' => $this->input->post('tgl_fs_jasa'),
			'nilai_project' => $this->input->post('nilai_project'),
			'status_project' => $this->input->post('status_project'),
		);
		if (!$this->input->post('tgl_pengisian_tender')) {
			$data_array['tgl_pengisian_tender'] = date('Y-m-d H:i:s');
		}

		if ($data_array['id_project']) {
			$exec = $this->model->update(['id_project' => $data_array['id_project']],$data_array);
		} else{
			$exec = $this->model->save($data_array);
		}


		echo json_encode(
			['status' => $exec]
		);
	}

	function save_tender_vendor()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$exec = false;

		$where = (array) json_decode($this->input->post('project_vendor'));
		$data_update = (array) json_decode($this->input->post('tender_vendor'));

		$exec = $this->model->update_tender_vendor($where,$data_update);

		echo json_encode(
			['status' => $exec]
		);
	}

	function delete()
	{
		if(!$this->input->is_ajax_request()) redirect();


		$this->db->trans_begin();

		$del1 = $this->model->delete_vendor(['id_project' => $this->input->post('id')]);
		$del2 = $this->model->delete_tender(['id_project' => $this->input->post('id')]);
		$del3 = $this->model->delete(['id_project' => $this->input->post('id')]);

		// $del = $this->model->delete_project(['id_project' => $this->input->post('id')]);

		$exec = $this->db->trans_status();

		if ($exec === FALSE)
		{
		        $this->db->trans_rollback();
		}
		else
		{
	        $this->db->trans_commit();
    		// $folder = $this->path_upload.$this->input->post('id');
    		// $this->delete_all_files($folder);
    		// rmdir($folder);
		}


		echo json_encode(
			['status' => $exec]
		);
		
	}

	function get_product()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$list = $this->model->get_data_product();

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['id_product'];
				$data[$key]['name'] = $value['nama_product'];
			}
		}

		echo json_encode($data);
	}

	function get_vendor()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$list = $this->model->get_data_vendor();

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['id_vendor'];
				$data[$key]['name'] = $value['nama_vendor'];
			}
		}

		echo json_encode($data);
	}

	function get_customer()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$list = $this->model->get_data_customer();

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['id_customer'];
				$data[$key]['name'] = $value['nama_customer'];
				$data[$key]['group'] = $value['nama_tipe_customer'];
			}
		}

		echo json_encode($data);
	}

	function get_sales()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$list = $this->model->get_data_sales();

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['id_sales'];
				$data[$key]['name'] = $value['nik_sales'] . ' - ' . $value['nama_sales'];
			}
		}

		echo json_encode($data);
	}

	function get_detil_customer()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$id_customer = $this->input->post('id');

		$data = $this->model->get_detil_customer($id_customer)->row();

		echo json_encode($data);
	}

	function get_detil_sales()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$id_sales = $this->input->post('id');

		$data = $this->model->get_detil_sales($id_sales)->row();

		echo json_encode($data);
	}

	function upload_lampiran_vendor($lampiran_vendor,$index,$id_project,$vendor,$nama_vendor)
	{
		$return = [
			'status' => false,
			'pesan' => 'file empty',
		];

		$path = $this->path_upload . $id_project;

		if( !empty($lampiran_vendor['tmp_name'][ $index ]) && is_uploaded_file($lampiran_vendor['tmp_name'][ $index ]) )
        {
			
			$nama_file = 'attach-'.$id_project.'-'.$vendor['id_vendor'].'-'.$nama_vendor;
            $this->load->library('upload');
			$config['upload_path']          = $path;
			$config['allowed_types']        = $this->jenis_file;
			$config['max_size']             = 10240; //set max size allowed in Kilobyte
			$config['max_width']            = 10000; // set max width image allowed
			$config['max_height']           = 10000; // set max height allowed
			$config['overwrite']           	= true; // set max height allowed
			$config['file_name']            = str_replace(' ', '_', $nama_file); //just milisecond timestamp fot unique name

            $this->upload->initialize($config);

            $_FILES['upload_lampiran_vendor']['name']=$lampiran_vendor['name'][ $index ];
            $_FILES['upload_lampiran_vendor']['type']= $lampiran_vendor['type'][ $index ];
            $_FILES['upload_lampiran_vendor']['tmp_name']= $lampiran_vendor['tmp_name'][ $index ];
            $_FILES['upload_lampiran_vendor']['error']= $lampiran_vendor['error'][ $index ];
            $_FILES['upload_lampiran_vendor']['size']= $lampiran_vendor['size'][ $index ];

            if ( ! $this->upload->do_upload('upload_lampiran_vendor') ){
            	$return['pesan'] = '<b>File: '. $_FILES['upload_lampiran_vendor']['name'].'</b> - '.$this->upload->display_errors('', '');
            }else{
            	$return['status'] = true;
            	$return['pesan'] = $this->upload->data('file_name');
            }
        }

        return $return;
	}

	function download_lampiran()
	{
		$id_vendor = $this->input->get('project');
		$nama_file = $this->input->get('file');
		$path = $this->path_upload . $id_vendor;
		force_download($path . '/' . $nama_file, NULL);
	}

	private function delete_all_files($folder)
	{
		//Get a list of all of the file names in the folder.
		$files = glob($folder . '/*');
		 
		//Loop through the file list.
		foreach($files as $file){
		    //Make sure that this is a file and not a directory.
		    if(is_file($file)){
		        //Use the unlink function to delete the file.
		        unlink($file);
		    }
		}
	}

	function cetak_detil()
	{
		$id_project = $this->input->post('id_project');
		$this->load->library('Pdfgenerator');

		$project = $this->model->get_by_id($id_project)->row();

		$html = '';


		$html .= '<br>Nama Project: <strong>'.$project->nama_project.'</strong><br>';
		$html .= 'Kode Project: <strong>'.$project->kode_project.'</strong><br><br>';

		// $list_jasa = $this->model->get_vendor_barang($id_project);
		$html.= '<table style="border-collapse: collapse;width:100%;" border="1px solid">';
		$html.=		'<thead>
						<tr style="background-color:lightgrey">
							<th class="data-center" style="width:auto" rowspan="2">Tipe</th>
							<th class="data-center" style="width:auto" rowspan="2">Nilai Project</th>
							<th class="data-center" style="width:auto" rowspan="2">No. Dokumen</th>
							<th class="data-center" style="width:auto" rowspan="2">Tanggal Diterima</th>
							<th class="data-center" style="width:auto" rowspan="1" colspan="2">Project</th>
							<th class="data-center" style="width:auto" rowspan="2">Nama Vendor</th>
							<th class="data-center" style="width:auto" rowspan="1" colspan="3"	>Actual Date</th>
							<th class="data-center" style="width:auto" rowspan="1" colspan="2"	>Project</th>
							<th class="data-center" style="width:auto" rowspan="2">No Dokumen</th>
							<th class="data-center" style="width:auto" rowspan="2">Nilai PO/SPK</th>
						</tr>
						<tr style="background-color:lightgrey">
							<th>Start Date</th>
							<th>End Date</th>
							<th>FPB</th>
							<th>SP</th>
							<th>PO/SPK</th>
							<th>Start Date</th>
							<th>End Date</th>
						</tr>
					</thead>';

		$html.= 	'<tbody>';

		$total_nilai_project = ($project->nilai_project_barang? $project->nilai_project_barang : 0) + ($project->nilai_project_jasa? $project->nilai_project_jasa : 0);
		$total_quot_akhir = 0;

		$default_td = '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>';
		//row barang
		$list_barang = $this->model->get_list_vendor_cetak($id_project,'B');
		$rowspan = $list_barang->num_rows();
		if ($rowspan > 0) {
			foreach ($list_barang->result_array() as $key => $value) {
			$html .= '<tr>';
			if ($key == 0) {
				$html .='<td rowspan="'.$rowspan.'">Barang</td>
					<td rowspan="'.$rowspan.'" class="data-right">'.format_ribuan_indo($project->nilai_project_barang,0).'</td>
					<td rowspan="'.$rowspan.'">'.$project->spk_fs_barang.'</td>
					<td rowspan="'.$rowspan.'">'.($project->tgl_fs_barang? to_date_format_mysql($project->tgl_fs_barang) : '-').'</td>
					<td rowspan="'.$rowspan.'">'.($project->start_date_barang? to_date_format_mysql($project->start_date_barang) : '-').'</td>
					<td rowspan="'.$rowspan.'">'.($project->end_date_barang? to_date_format_mysql($project->end_date_barang) : '-').'</td>';
			}
			$quot_akhir = $value['nilai_quotation_akhir'] ? $value['nilai_quotation_akhir'] : 0;
			$total_quot_akhir += $quot_akhir;
			$html .= '<td>'.$value['nama_vendor'].'</td>
			<td>'.($value['p3_fps_release'] ? to_date_format_mysql($value['p3_fps_release']) : '-').'</td>
			<td>'.($value['sp_release'] ? to_date_format_mysql($value['sp_release']) : '-').'</td>
			<td>'.($value['spk_fps_approve'] ? to_date_format_mysql($value['spk_fps_approve']) : '-').'</td>
			<td>'.($value['v_commitment_start'] ? to_date_format_mysql($value['v_commitment_start']) : '-').'</td>
			<td>'.($value['v_commitment_finish'] ? to_date_format_mysql($value['v_commitment_finish']) : '-').'</td>
			<td>'.$value['no_contract_po'].'</td>
			<td class="data-right">'.format_ribuan_indo($quot_akhir,0).'</td>';
			$html .='</tr>';	
			}
		} else{
			$html .= '<tr>
				<td rowspan="'.$rowspan.'">Barang</td>
				<td rowspan="'.$rowspan.'" class="data-right">'.format_ribuan_indo($project->nilai_project_barang,0).'</td>
				<td rowspan="'.$rowspan.'">'.$project->spk_fs_barang.'</td>
				<td rowspan="'.$rowspan.'">'.($project->tgl_fs_barang? to_date_format_mysql($project->tgl_fs_barang) : '-').'</td>
				<td rowspan="'.$rowspan.'">'.($project->start_date_barang? to_date_format_mysql($project->start_date_barang) : '-').'</td>
				<td rowspan="'.$rowspan.'">'.($project->end_date_barang? to_date_format_mysql($project->end_date_barang) : '-').'</td>
				'.$default_td.'
			</tr>';

		}

		//row jasa
		$list_jasa = $this->model->get_list_vendor_cetak($id_project,'J');
		$rowspan = $list_jasa->num_rows();
		if ($rowspan > 0) {
			foreach ($list_jasa->result_array() as $key => $value) {
			$html .= '<tr>';
			if ($key == 0) {
				$html .='<td rowspan="'.$rowspan.'">Jasa</td>
						<td rowspan="'.$rowspan.'" class="data-right">'.format_ribuan_indo($project->nilai_project_jasa,0).'</td>
						<td rowspan="'.$rowspan.'">'.$project->spk_fs_jasa.'</td>
						<td rowspan="'.$rowspan.'">'.($project->tgl_fs_jasa ?  to_date_format_mysql($project->tgl_fs_jasa) : '-' ).'</td>
						<td rowspan="'.$rowspan.'">'.($project->start_date_jasa ?  to_date_format_mysql($project->start_date_jasa) : '-' ).'</td>
						<td rowspan="'.$rowspan.'">'.($project->end_date_jasa ?  to_date_format_mysql($project->end_date_jasa) : '-' ).'</td>';
			}
			$quot_akhir = $value['nilai_quotation_akhir'] ? $value['nilai_quotation_akhir'] : 0;
			$total_quot_akhir += $quot_akhir;
			$html .= '<td>'.$value['nama_vendor'].'</td>
			<td>'.($value['p3_fps_release'] ? to_date_format_mysql($value['p3_fps_release']) : '-').'</td>
			<td>'.($value['sp_release'] ? to_date_format_mysql($value['sp_release']) : '-').'</td>
			<td>'.($value['spk_fps_approve'] ? to_date_format_mysql($value['spk_fps_approve']) : '-').'</td>
			<td>'.($value['v_commitment_start'] ? to_date_format_mysql($value['v_commitment_start']) : '-').'</td>
			<td>'.($value['v_commitment_finish'] ? to_date_format_mysql($value['v_commitment_finish']) : '-').'</td>
			<td>'.$value['no_spk_fps'].'</td>
			<td class="data-right">'.format_ribuan_indo($quot_akhir,0).'</td>';
			$html .='</tr>';	
			}
		} else{
			$html .= '<tr>
				<td rowspan="'.$rowspan.'">Jasa</td>
			<td rowspan="'.$rowspan.'" class="data-right">'.format_ribuan_indo($project->nilai_project_jasa,0).'</td>
			<td rowspan="'.$rowspan.'">'.$project->spk_fs_jasa.'</td>
			<td rowspan="'.$rowspan.'">'.($project->tgl_fs_jasa ?  to_date_format_mysql($project->tgl_fs_jasa) : '-' ).'</td>
			<td rowspan="'.$rowspan.'">'.($project->start_date_jasa ?  to_date_format_mysql($project->start_date_jasa) : '-' ).'</td>
			<td rowspan="'.$rowspan.'">'.($project->end_date_jasa ?  to_date_format_mysql($project->end_date_jasa) : '-' ).'</td>
				'.$default_td.'
			</tr>';

		}

		$html.= 	'</tbody>';

		$html.= '<tfoot>
			<tr>
				<th>Total</th>
				<th class="data-right">'.format_ribuan_indo($total_nilai_project,0).'</th>
				<th colspan="10"></th>
				<th>Total</th>
				<th class="data-right">'.format_ribuan_indo($total_quot_akhir,0).'</th>
			</tr>
		</tfoot>';
		$html.= '</table>';

		$data = [];
		$data['html'] = $html;
        $this->pdfgenerator->setPaper('A4', 'landscape');
        $this->pdfgenerator->filename = "laporan_per_project.pdf";
        $this->pdfgenerator->load_view('format_laporan', $data);

	}

}

/* End of file Project.php */
/* Location: ./application/controllers/Project.php */
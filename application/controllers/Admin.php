<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
	private $path_upload_foto = 'assets/upload/admin/foto/';

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('id')) redirect('login');
		$this->load->model('admin_model', 'model');
	}

	function get_data()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$list = $this->model->get_data();

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = ($key + 1) . '.';
				$data['data'][$key][] = $value['username'];
				$data['data'][$key][] = $value['nama_admin'];
				$data['data'][$key][] = $value['level_admin'];
				$data['data'][$key][] = $value['status_admin'];
				$data['data'][$key][] = $value['id_admin'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_by_id()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$id = $this->input->post('id');
		$data = $this->model->get_by_id($id)->row();

		echo json_encode($data);
	}

	function save()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$exec = false;

		$data_array = array(
			'id_admin' => $this->input->post('id_admin') ? $this->input->post('id_admin') : null,
			'nama_admin' => $this->input->post('nama_admin'),
			'level_admin' => $this->input->post('level_admin'),
			'status_admin' => $this->input->post('status_admin'),
		);

		if ($data_array['id_admin']) {
			if ($this->input->post('ubah_password'))
				$data_array['password_admin'] = password_hash($this->input->post('password_admin'), PASSWORD_DEFAULT);

			if ($this->input->post('hapus_foto')) {
				$data_array['path_foto_admin'] = null;
				$folder = $this->path_upload_foto.$data_array['id_admin'];
				$this->delete_all_files($folder);
			}

			if (!empty($_FILES['upload_foto']['name'])) {
				$folder = $this->path_upload_foto.$data_array['id_admin'];
				$this->delete_all_files($folder);
				$upload_action = $this->_do_upload_foto($data_array['id_admin'],'Foto admin-'.$data_array['id_admin'].'');
				if (!$upload_action['status']) {
					echo json_encode($upload_action);
					exit;
				}
				$data_array['path_foto_admin'] = $upload_action['pesan'];
			}

			$exec = $this->model->update(['id_admin' => $data_array['id_admin']], $data_array);
		} else {
			$data_array['username'] = $this->input->post('username');
			$data_array['password_admin'] = password_hash($this->input->post('password_admin'), PASSWORD_DEFAULT);
			$exec = $this->model->save($data_array);
			$id_admin = $this->db->insert_id();

			if (!empty($_FILES['upload_foto']['name'])) {
				$upload_action = $this->_do_upload_foto($id_admin,'Foto admin-'.$id_admin.'');
				if (!$upload_action['status']) {
					echo json_encode($upload_action);
					exit;
				}
				$data_upload['path_foto_admin'] = $upload_action['pesan'];
				$exec = $this->model->update(['id_admin' => $id_admin], $data_upload);
			}
		}


		echo json_encode(
			['status' => $exec]
		);
	}

	function delete()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$exec = $this->model->delete(['id_admin' => $this->input->post('id')]);

		if ($exec) {
			$folder_foto = $this->path_upload_foto.$this->input->post('id');
			if (file_exists($folder_foto)) {
	    		$this->delete_all_files($folder_foto);
	    		rmdir($folder_foto);
    		}
		}

		echo json_encode(
			['status' => $exec]
		);
	}

	private function _do_upload_foto($id_admin, $filename)
	{
		$path = $this->path_upload_foto . $id_admin;
		$result_upload = [
			'status' => false,
			'pesan' => '',
		];

		if (!file_exists($path))
			mkdir($path, 0777, true);

		$config['upload_path']          = $path;
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['max_size']             = 10240; //set max size allowed in Kilobyte
		$config['max_width']            = 10000; // set max width image allowed
		$config['max_height']           = 10000; // set max height allowed
		$config['overwrite']           	= true; // set max height allowed
		$config['file_name']            = $filename.'-'.round(microtime(true)*1000); //just milisecond timestamp fot unique name

		$this->load->library('upload');
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_foto')) {
			$result_upload['pesan'] = 'Upload Foto error: ' . $this->upload->display_errors('', '');
			return $result_upload;
		}
		$result_upload['status'] = true;
		$result_upload['pesan'] = $this->upload->data('file_name');
		return $result_upload;
	}

	private function delete_all_files($folder)
	{
		//Get a list of all of the file names in the folder.
		$files = glob($folder . '/*');
		 
		//Loop through the file list.
		foreach($files as $file){
		    //Make sure that this is a file and not a directory.
		    if(is_file($file)){
		        //Use the unlink function to delete the file.
		        unlink($file);
		    }
		}
	}
}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seeder extends CI_Controller {

	protected $faker;
	protected $locale = 'id_ID';
	protected $timezone = 'Asia/Jakarta';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('seeder_model','model');
	}

	function save()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$tabel = $this->input->post('val_tabel');
		$jumlah = $this->input->post('val_jumlah');

		$return['sukses'] = 0;
		$return['gagal'] = 0;

		switch ($tabel) {
			case 'JS':
				$return['sukses'] =  $this->seed_jabatan($jumlah);
				$return['gagal'] = $jumlah - $return['sukses'];
				break;

			case 'S':
				$return['sukses'] =  $this->seed_sales($jumlah);
				$return['gagal'] = $jumlah - $return['sukses'];
				break;

			case 'C':
				$return['sukses'] =  $this->seed_customer($jumlah);
				$return['gagal'] = $jumlah - $return['sukses'];
				break;

			case 'V':
				$return['sukses'] =  $this->seed_vendor($jumlah);
				$return['gagal'] = $jumlah - $return['sukses'];
				break;
			
			default:
				// code...
				break;
		}

		echo json_encode($return);

	}

	public function seed_jabatan($jumlah)
	{
		$this->faker = \Faker\Factory::create();

		$generated = 0;
		for ($i = 0; $i < $jumlah; $i++) {

			$data['nama_jabatan']      = $this->faker->jobTitle;
			$data['status_jabatan']    = 'A';

			$generated += $this->model->save($data,'jabatan');
		}

		return $generated;
	}

	public function seed_sales($jumlah)
	{
		$this->faker = \Faker\Factory::create($this->locale);

		$generated = 0;
		$id_jabatan_array = $this->model->get_id_jabatan_list();
		for ($i = 0; $i < $jumlah; $i++) {

			$data['nik_sales']      = rand(111111, 999999);
			$data['id_jabatan']      = $this->faker->randomElement($id_jabatan_array);
			$data['nama_sales']      = $this->faker->name;
			$data['telp_sales']      = '08'.rand(1, 9).rand(11111111,999999999);
			$data['email_sales']      = $this->faker->companyEmail;

			$generated += $this->model->save($data,'sales');
		}

		return $generated;
	}

	public function seed_customer($jumlah)
	{
		$this->faker = \Faker\Factory::create($this->locale);

		$generated = 0;
		$id_tipe_customer_array = $this->model->get_id_tipe_customer_list();
		$id_sales_array = $this->model->get_id_sales_list();
		$city_id_array = $this->model->get_city_id_list();
		$bidang_usaha_array = $this->model->get_id_bidang_usaha_list();

		for ($i = 0; $i < $jumlah; $i++) {

			$isi_sales = rand(0,1);
			$personal = rand(0,1);
			$npwp = $this->generate_npwp();

			$data['id_tipe_customer']   = $this->faker->randomElement($id_tipe_customer_array);
			$data['nama_customer']      = $personal == 1 ? $this->faker->name : $this->faker->company;
			$data['npwp_customer']      = $npwp;
			$data['alamat_customer']      = $this->faker->streetAddress;
			$data['city_id']      = $this->faker->randomElement($city_id_array);
			$data['kode_pos_customer']      = $this->faker->postcode;
			$data['telp_customer']      = '08'.rand(1, 9).rand(11111111,999999999);
			$data['email_customer']     = $personal == 1 ? $this->faker->freeEmail : $this->faker->companyEmail;
			$data['website_customer']     = 'https://'.$this->faker->domainName;
			$data['id_bidang_usaha']     = $this->faker->randomElement($bidang_usaha_array);
			$data['pic_customer']      = $this->faker->name;
			$data['telp_pic_customer']      = '08'.rand(1, 9).rand(11111111,999999999);
			$data['id_sales']      = $isi_sales == 1 ? $this->faker->randomElement($id_sales_array) : null;

			$generated += $this->model->save($data,'customer');
		}

		return $generated;
	}

	public function seed_vendor($jumlah)
	{
		$this->faker = \Faker\Factory::create($this->locale);

		$generated = 0;
		$city_id_array = $this->model->get_city_id_list();
		$bidang_usaha_array = $this->model->get_bidang_usaha_list();

		for ($i = 0; $i < $jumlah; $i++) {

			$npwp = $this->generate_npwp();

			$data['nama_vendor']      = $this->faker->company;
			$data['npwp_vendor']      = $npwp;
			$data['alamat_vendor']      = $this->faker->streetAddress;
			$data['city_id']      = $this->faker->randomElement($city_id_array);
			$data['kode_pos_vendor']      = $this->faker->postcode;
			$data['telp_vendor']      = '08'.rand(1, 9).rand(11111111,999999999);
			$data['email_vendor']     = $this->faker->companyEmail;
			$data['website_vendor']     = 'https://'.$this->faker->domainName;
			$data['bidang_usaha_vendor']     = $this->faker->randomElement($bidang_usaha_array);
			$data['pic_vendor']      = $this->faker->name;
			$data['telp_pic_vendor']      = '08'.rand(1, 9).rand(11111111,999999999);

			$generated += $this->model->save($data,'vendor');
		}

		return $generated;
	}

	function generate_npwp()
	{
		$this->faker = \Faker\Factory::create($this->locale);

		$dua_awal = ['01','03','04','06','05','07','09'];
		$enam_kedua = rand(111111,999999);
		$satu_ketiga = rand(0,9);
		$tiga_keempat = rand(100,999);

		return $this->faker->randomElement($dua_awal).$enam_kedua.$satu_ketiga.$tiga_keempat;
		
	}


}

/* End of file Seeder.php */
/* Location: ./application/controllers/Seeder.php */
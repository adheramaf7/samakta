<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	private $path_upload_foto = 'assets/img/';

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('id')) redirect('login');
		$this->load->model('settings_model', 'model');
	}

	function get_data_target()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$list = $this->model->get_data_target();
		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = $value['tahun_target'];
				$data['data'][$key][] = $value['nilai_target'];	
				$data['data'][$key][] = $value['pencapaian_one_time']+$value['pencapaian_recurring'];
				$data['data'][$key][] = $value['nilai_target_one_time'];	
				$data['data'][$key][] = $value['pencapaian_one_time'];
				$data['data'][$key][] = $value['nilai_target_recurring'];	
				$data['data'][$key][] = $value['pencapaian_recurring'];
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function load_setting_perusahaan()
	{
		if (!$this->input->is_ajax_request()) redirect();
		$data = [];
		$data['nama_perusahaan'] = $this->model->get_settings('nama_perusahaan');
		$data['alamat_perusahaan'] = $this->model->get_settings('alamat_perusahaan');
		$data['email_perusahaan'] = $this->model->get_settings('email_perusahaan');
		$data['telepon_perusahaan'] = $this->model->get_settings('telepon_perusahaan');
		$data['fax_perusahaan'] = $this->model->get_settings('fax_perusahaan');
		$data['path_logo_perusahaan'] = $this->model->get_settings('path_logo_perusahaan');
		echo json_encode($data);
	}

	function load_target()
	{
		if (!$this->input->is_ajax_request()) redirect();
		$tahun_target = $this->input->get('tahun_target');
		$data = $this->model->load_target($tahun_target)->row();
		echo json_encode($data);
	}

	function save()
	{
		if (!$this->input->is_ajax_request()) redirect();
		
		$list_diupdate = ['nama_perusahaan','alamat_perusahaan','email_perusahaan','telepon_perusahaan','fax_perusahaan','path_logo_perusahaan'];
		$response = [
			'status' => false,
			'pesan' => "",
		];
		$sukses = 0;
		foreach ($list_diupdate as $value) {
			if ($value == 'path_logo_perusahaan') {
				if (!empty($_FILES['upload_foto']['name'])) {
					$upload = $this->_do_upload_foto();
					if ($upload['status']) {
						$where['id_settings'] = $value;
						$data_update['setting_value'] = $upload['pesan'];
						$update = $this->model->save_settings($where,$data_update);
						$sukses += $update? 1:0;
						continue;	
					}
					$response['pesan'] = $upload['pesan'];
					continue;
				}else{
					$sukses++;
					continue;
				}
			}
			$where['id_settings'] = $value;
			$data_update['setting_value'] = $this->input->post($value)?$this->input->post($value):null;
			$update = $this->model->save_settings($where,$data_update);
			$sukses += $update? 1:0;
		}
		$response['status'] = $sukses == sizeof($list_diupdate) ? true:false;
		// $response['pesan'] = $sukses;
		echo json_encode($response);
	}

	function save_target()
	{
		if (!$this->input->is_ajax_request()) redirect();
		
		$tahun_target = $this->input->post('tahun_target');
		$nilai_target = $this->input->post('nilai_target');
		$nilai_target_one_time = $this->input->post('nilai_target_one_time');
		$nilai_target_recurring = $this->input->post('nilai_target_recurring');

		$data = [
			'tahun_target' => $tahun_target,
			'nilai_target' => $nilai_target,
			'nilai_target_one_time' => $nilai_target_one_time,
			'nilai_target_recurring' => $nilai_target_recurring,
		];

		$save = $this->model->save_target($data);
		echo json_encode([
			'status' => $save
		]);
	}

	private function _do_upload_foto()
	{
		$path = $this->path_upload_foto;
		$result_upload = [
			'status' => false,
			'pesan' => '',
		];

		if (!file_exists($path))
			mkdir($path, 0777, true);

		$config['upload_path']          = $path;
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['max_size']             = 10240; //set max size allowed in Kilobyte
		$config['max_width']            = 10000; // set max width image allowed
		$config['max_height']           = 10000; // set max height allowed
		$config['overwrite']           	= true; // set max height allowed
		$config['file_name']            = 'logo_upload_'.round(microtime(true)*1000); //just milisecond timestamp fot unique name

		$this->load->library('upload');
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_foto')) {
			$result_upload['pesan'] = 'Upload Foto error: ' . $this->upload->display_errors('', '');
			return $result_upload;
		}
		$result_upload['status'] = true;
		$result_upload['pesan'] = $this->upload->data('file_name');
		return $result_upload;
	}


}

/* End of file Settings.php */
/* Location: ./application/controllers/Settings.php */
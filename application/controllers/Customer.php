<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer extends CI_Controller
{

	private $path_upload = 'assets/upload/customer/lampiran/';
	private $path_upload_foto = 'assets/upload/customer/foto/';
	private $jenis_file = 'gif|jpg|png|jpeg|pdf|doc|docx|xls|xlsx|ppt|pptx';

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('id')) redirect('login');
		$this->load->model('customer_model', 'model');
	}

	function get_data()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$tipe_customer = $this->input->get('tipe_customer');

		$list = $this->model->get_data($tipe_customer);


		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = ($key + 1) . '.';
				$data['data'][$key][] = $value['nama_customer'];
				$data['data'][$key][] = $value['nama_tipe_customer'];
				$data['data'][$key][] = $value['alamat_customer'] . ', ' . cap_each_word($value['city_name']) . ', ' . cap_each_word($value['province_name']);
				$data['data'][$key][] = $value['status_customer'];
				$data['data'][$key][] = $value['lampiran'];
				$data['data'][$key][] = $value['id_customer'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_lampiran()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$id_customer = $this->input->get('customer');

		$list = $this->model->get_data_lampiran($id_customer);

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = ($key + 1) . '.';
				$data['data'][$key][] = $value['file_lampiran'];
				$data['data'][$key][] = $value['id_customer'];
				$data['data'][$key][] = $value['id_jenis_lampiran'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_by_id()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$id = $this->input->post('id');
		$data = $this->model->get_by_id($id)->row();

		echo json_encode($data);
	}

	function get_tipe_customer()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$list = $this->model->get_data_tipe_customer();

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['id_tipe_customer'];
				$data[$key]['name'] = $value['nama_tipe_customer'];
			}
		}

		echo json_encode($data);
	}

	function get_bidang_usaha()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$list = $this->model->get_data_bidang_usaha();

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['id_bidang_usaha'];
				$data[$key]['name'] = $value['nama_bidang_usaha'];
			}
		}

		echo json_encode($data);
	}

	function get_sales()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$list = $this->model->get_data_sales();

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['id_sales'];
				$data[$key]['name'] = $value['nik_sales'] . ' - ' . $value['nama_sales'];
			}
		}

		echo json_encode($data);
	}

	function get_jenis_lampiran()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$list = $this->model->get_data_jenis_lampiran();

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['id_jenis_lampiran'];
				$data[$key]['name'] = $value['nama_jenis_lampiran'];
			}
		}

		echo json_encode($data);
	}

	function get_province()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$list = $this->model->get_data_province();

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['province_id'];
				$data[$key]['name'] = $value['province_name'];
			}
		}

		echo json_encode($data);
	}

	function get_city()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$province_id = $this->input->post('province_id');
		$list = $this->model->get_data_city($province_id);

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['city_id'];
				$data[$key]['name'] = $value['city_name'];
			}
		}

		echo json_encode($data);
	}

	function save()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$exec = false;

		$data_array = array(
			'id_customer' => $this->input->post('id_customer') ? $this->input->post('id_customer') : null,
			'nama_customer' => $this->input->post('nama_customer'),
			'id_tipe_customer' => $this->input->post('tipe_customer'),
			'npwp_customer' => $this->input->post('npwp_customer'),
			'alamat_customer' => $this->input->post('alamat_customer'),
			'city_id' => $this->input->post('kota'),
			'kode_pos_customer' => $this->input->post('kode_pos_customer'),
			'telp_customer' => $this->input->post('telp_customer'),
			'email_customer' => $this->input->post('email_customer'),
			'website_customer' => $this->input->post('website_customer'),
			'id_bidang_usaha' => $this->input->post('id_bidang_usaha'),
			'pic_customer' => $this->input->post('pic_customer'),
			'telp_pic_customer' => $this->input->post('telp_pic_customer'),
			'id_sales' => $this->input->post('id_sales') ? $this->input->post('id_sales') : null,
			'status_customer' => $this->input->post('status_customer'),
		);

		if ($data_array['id_customer']) {
			if ($this->input->post('hapus_foto')) {
				$data_array['path_foto_customer'] = null;
				$folder = $this->path_upload_foto.$data_array['id_customer'];
				$this->delete_all_files($folder);
			}

			if (!empty($_FILES['upload_foto']['name'])) {
				$folder = $this->path_upload_foto.$data_array['id_customer'];
				$this->delete_all_files($folder);
				$upload_action = $this->_do_upload_foto($data_array['id_customer'],'Foto Customer-'.$data_array['id_customer'].'');
				if (!$upload_action['status']) {
					echo json_encode($upload_action);
					exit;
				}
				$data_array['path_foto_customer'] = $upload_action['pesan'];
			}

			$exec = $this->model->update(['id_customer' => $data_array['id_customer']], $data_array);

		} else {
			$exec = $this->model->save($data_array);
			$id_customer = $this->db->insert_id();

			if (!empty($_FILES['upload_foto']['name'])) {
				$upload_action = $this->_do_upload_foto($id_customer,'Foto Customer-'.$id_customer.'');
				if (!$upload_action['status']) {
					echo json_encode($upload_action);
					exit;
				}
				$data_upload['path_foto_customer'] = $upload_action['pesan'];
				$exec = $this->model->update(['id_customer' => $id_customer], $data_upload);
			}
		}

		echo json_encode(
			['status' => $exec]
		);
	}

	function save_lampiran()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$exec = false;

		$data_array = array(
			'id_customer' => $this->input->post('id_customer_lampiran'),
			'id_jenis_lampiran' => $this->input->post('jenis_lampiran'),
		);

		$nama_jenis_lampiran = $this->model->get_nama_jenis_lampiran($data_array['id_jenis_lampiran']);

		$nama_lampiran = $nama_jenis_lampiran . '-' . $data_array['id_customer'];

		$upload_action = $this->_do_upload($data_array['id_customer'], $nama_lampiran);

		if (!$upload_action['status']) {
			echo json_encode($upload_action);
			exit;
		}

		$data_array['file_lampiran'] = $upload_action['pesan'];

		$exec = $this->model->save_lampiran($data_array);

		echo json_encode(
			['status' => $exec]
		);
	}

	function delete()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$this->db->trans_begin();

		$del1 = $this->model->delete_lampiran(['id_customer' => $this->input->post('id')]);
		$del2 = $this->model->delete(['id_customer' => $this->input->post('id')]);

		$exec = $this->db->trans_status();

		if ($exec === FALSE)
		{
	        $this->db->trans_rollback();
		}
		
		else
		{
	        $this->db->trans_commit();
    		$folder = $this->path_upload.$this->input->post('id');
    		if (file_exists($folder)) {
	    		$this->delete_all_files($folder);
	    		rmdir($folder);
    		}
    		
    		$folder_foto = $this->path_upload_foto.$this->input->post('id');
			if (file_exists($folder_foto)) {
	    		$this->delete_all_files($folder_foto);
	    		rmdir($folder_foto);
    		}
		}

		echo json_encode(
			['status' => $exec]
		);
	}

	function delete_lampiran()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$exec = $this->model->delete_lampiran(
			[
				'id_customer' => $this->input->post('id_customer'),
				'id_jenis_lampiran' => $this->input->post('id_jenis_lampiran')
			]
		);

		$path = $this->path_upload . $this->input->post('id_customer');

		if (file_exists($path . '/' . $this->input->post('nama_lampiran')))
			unlink($path . '/' . $this->input->post('nama_lampiran'));

		echo json_encode(
			['status' => $exec]
		);
	}

	function download_lampiran()
	{
		$id_customer = $this->input->get('customer');
		$nama_file = $this->input->get('file');
		$path = $this->path_upload . $id_customer;
		force_download($path . '/' . $nama_file, NULL);
	}



	private function _do_upload($id_customer, $filename)
	{
		$path = $this->path_upload . $id_customer;
		$result_upload = [
			'status' => false,
			'pesan' => '',
		];

		if (!file_exists($path))
			mkdir($path, 0777, true);

		$config['upload_path']          = $path;
		$config['allowed_types']        = $this->jenis_file;
		$config['max_size']             = 10240; //set max size allowed in Kilobyte
		$config['max_width']            = 10000; // set max width image allowed
		$config['max_height']           = 10000; // set max height allowed
		$config['overwrite']           	= true; // set max height allowed
		$config['file_name']            = str_replace(' ', '_', $filename); //just milisecond timestamp fot unique name

		$this->load->library('upload');
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('file_lampiran')) {
			$result_upload['pesan'] = 'Upload error: ' . $this->upload->display_errors('', '');
			return $result_upload;
		}
		$result_upload['status'] = true;
		$result_upload['pesan'] = $this->upload->data('file_name');
		return $result_upload;
	}

	private function _do_upload_foto($id_customer, $filename)
	{
		$path = $this->path_upload_foto . $id_customer;
		$result_upload = [
			'status' => false,
			'pesan' => '',
		];

		if (!file_exists($path))
			mkdir($path, 0777, true);

		$config['upload_path']          = $path;
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['max_size']             = 10240; //set max size allowed in Kilobyte
		$config['max_width']            = 10000; // set max width image allowed
		$config['max_height']           = 10000; // set max height allowed
		$config['overwrite']           	= true; // set max height allowed
		$config['file_name']            = round(microtime(true)*1000); //just milisecond timestamp fot unique name

		$this->load->library('upload');
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_foto')) {
			$result_upload['pesan'] = 'Upload Foto error: ' . $this->upload->display_errors('', '');
			return $result_upload;
		}
		$result_upload['status'] = true;
		$result_upload['pesan'] = $this->upload->data('file_name');
		return $result_upload;
	}

	private function delete_all_files($folder)
	{
		//Get a list of all of the file names in the folder.
		$files = glob($folder . '/*');
		 
		//Loop through the file list.
		foreach($files as $file){
		    //Make sure that this is a file and not a directory.
		    if(is_file($file)){
		        //Use the unlink function to delete the file.
		        unlink($file);
		    }
		}
	}


	function cetak_detil()
	{
		// echo '<h1>YES</h1>'; die;
		$id_customer = $this->input->post('id_customer');
		$this->load->library('Pdfgenerator');

		$customer = $this->model->get_by_id($id_customer)->row();

		$html = '';

		$foto = ($customer->path_foto_customer) ? $this->path_upload_foto.$customer->id_customer.'/'.$customer->path_foto_customer : 'assets/img/avatar.png';

		$html .= '<p style="font-weight:bold; font-size:14px; text-align:center;">Detil Data Customer</p>';

		$html .= 	'<div style="width:30%;float:left; text-align:center;">

						<img src="'.$foto.'" style="height:auto;width:100px;margin-top:20px; ">
					</div>';


		$html .= 	'<div style="width:70%;float:right";>
						<table>
							<tr>
								<th style="width:100px;font-size:14px">Nama</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$customer->nama_customer.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Tipe</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$customer->nama_tipe_customer.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">NPWP</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$customer->npwp_customer.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Alamat</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$customer->alamat_customer.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Kota / Kab</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.cap_each_word($customer->city_name).'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Provinsi</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.cap_each_word($customer->province_name).'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Kode Pos</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$customer->kode_pos_customer.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">No. Telepon</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$customer->telp_customer.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Email</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$customer->email_customer.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Website</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$customer->website_customer.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Bidang Usaha</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$customer->nama_bidang_usaha.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">PIC</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$customer->pic_customer.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">No. Telp. PIC</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$customer->telp_pic_customer.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Nama Sales</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.($customer->nama_sales? $customer->nama_sales : '-').'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Status Aktif</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.($customer->status_customer == 'A' ? 'Aktif' : 'Tidak Aktif').'</td>
							</tr>
						</table>
					</div>';

		$data = [];
		$data['html'] = $html;
        $this->pdfgenerator->setPaper('A4', 'portrait');
        $this->pdfgenerator->filename = "laporan.pdf";
        $this->pdfgenerator->load_view('format_laporan', $data);
	}

	function export_pdf()
	{
		// echo '<h1>YES</h1>'; die;
		$tipe_customer = $this->input->post('tipe_customer');
		$this->load->library('Pdfgenerator');

		$list = $this->model->get_data($tipe_customer);

		$html = '';

		$html .= '<p style="font-weight:bold; font-size:16px; text-align:center;">Data Customer</p>';

		if ($list->num_rows()>0) {
			$html.= '<table style="border-collapse: collapse;width:100%;" border="1px solid">';
			$html.=		'<thead>
							<tr style="background-color:lightgrey">
								<th class="data-center" style="width:5%">No.</th>
								<th class="data-center" style="width:10%">Tipe</th>
								<th class="data-center" style="width:15%">Nama</th>
								<th class="data-center" style="width:20%">Alamat</th>
								<th class="data-center" style="width:15%">Telp.</th>
								<th class="data-center" style="width:15%">Email</th>
								<th class="data-center" style="width:10%">Status</th>
							</tr>
						</thead>';
			$html.= 	'<tbody>';
			foreach ($list->result_array() as $key => $value) {
				$html.= '<tr>
							<td>'.($key+1).'.</td>
							<td>'.$value['nama_tipe_customer'].'</td>
							<td>'.$value['nama_customer'].'</td>
							<td>'.$value['alamat_customer'].'</td>
							<td>'.$value['telp_customer'].'</td>
							<td>'.$value['email_customer'].'</td>
							<td>'.($value['status_customer']=='A'? 'Aktif' :'Tidak Aktif').'</td>
						</tr>';
			}
			$html.= 	'</tbody>';
			$html.= '</table>';
		}

		$data = [];
		$data['html'] = $html;
        $this->pdfgenerator->setPaper('A4', 'portrait');
        $this->pdfgenerator->filename = "laporan.pdf";
        $this->pdfgenerator->load_view('format_laporan', $data);
	}


	function export_excel()
	{
		$tipe_customer = $this->input->get('tipe_customer');
		$this->load->library('Excel');

		$list = $this->model->get_data($tipe_customer);
		$spreadsheet = $this->excel->Spreadsheet();
		// Set document properties
		$spreadsheet->getProperties()->setCreator('SamaktaMitra Admin Area')
		->setLastModifiedBy('SamaktaMitra')
		->setTitle('Data Customer')
		->setSubject('Data Customer')
		->setDescription('Laporan data customer')
		->setKeywords('excel,customer,data')
		->setCategory('Report Excel');

		// Add some data
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A1', 'DATA CUSTOMER')->mergeCells('A1:O1');

		$spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray( [
			'font' => [ 
				'bold' => TRUE
			],
			'alignment' => [
				'horizontal' => 'center'
			]
		]);

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A3', 'NO.')
		->setCellValue('B3', 'TIPE CUSTOMER')
		->setCellValue('C3', 'NAMA CUSTOMER')
		->setCellValue('D3', 'NPWP CUSTOMER')
		->setCellValue('E3', 'ALAMAT')
		->setCellValue('F3', 'KOTA/KAB')
		->setCellValue('G3', 'PROVINSI')
		->setCellValue('H3', 'KODE POS')
		->setCellValue('I3', 'TELEPON')
		->setCellValue('J3', 'EMAIL')
		->setCellValue('K3', 'WEBSITE')
		->setCellValue('L3', 'PIC')
		->setCellValue('M3', 'TELP. PIC')
		->setCellValue('N3', 'SALES')
		->setCellValue('O3', 'STATUS AKTIF')
		;

		$spreadsheet->getActiveSheet()->getStyle('A3:O3')->getFont()->applyFromArray( [ 'bold' => TRUE] );



		// Fill data
		$i=4;
		$last_row=4;
		foreach ($list->result_array() as $key => $value) {
			$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A'.$i, ($key + 1) . '.')
			->setCellValue('B'.$i, $value['nama_tipe_customer'])
			->setCellValue('C'.$i, $value['nama_customer'])
			->setCellValue('D'.$i, $value['npwp_customer'])
			->setCellValue('E'.$i, $value['alamat_customer'])
			->setCellValue('F'.$i, cap_each_word($value['city_name']) )
			->setCellValue('G'.$i, cap_each_word($value['province_name']) )
			->setCellValue('H'.$i, $value['kode_pos_customer'])
			->setCellValue('I'.$i, $value['telp_customer'])
			->setCellValue('J'.$i, $value['email_customer'])
			->setCellValue('K'.$i, $value['website_customer'])
			->setCellValue('L'.$i, $value['pic_customer'])
			->setCellValue('M'.$i, $value['telp_pic_customer'])
			->setCellValue('N'.$i, $value['nama_sales'])
			->setCellValue('O'.$i, $value['status_customer'] == 'A' ? 'Aktif' : 'Tidak Aktif')
			;
			$i++;
		}
		$last_row = $i-1;

		$width = [
			'A' => 5,
			'B' => 20,
			'C' => 25,
			'D' => 18,
			'E' => 35,
			'F' => 25,
			'G' => 25,
			'H' => 13,
			'I' => 20,
			'J' => 25,
			'K' => 25,
			'L' => 15,
			'M' => 15,
			'N' => 15,
			'O' => 13,
		];

		//loop for set Auto Size Columns
		foreach(range('A','O') as $v){
			$spreadsheet->getActiveSheet()->getColumnDimension($v)->setWidth($width[$v]);
		}

		//style table header
		$spreadsheet->getActiveSheet()->getStyle('A3:O3')->applyFromArray([
			'borders' => [
              	'allBorders' => [
                      'borderStyle' => 'thin',
                      'color' => [
                          'rgb' => '000000'
                      ]
                  ]
             ],
             'fill' => [
             	'fillType' => 'solid',
             	'startColor' => [ 'rgb' => 'D3D3D3' ],
             	'endColor' => [ 'rgb' => 'D3D3D3' ],
             ]
         ]);

		//style tabel body
		$spreadsheet->getActiveSheet()->getStyle('A4:O'.$last_row)->applyFromArray([
			'borders' => [
              	'allBorders' => [
                      'borderStyle' => 'thin',
                      'color' => [
                          'rgb' => '000000'
                      ]
                  ]
             ],
         ]);

		//move cursor to cell A1
		$spreadsheet->getActiveSheet()->getStyle('A1');
		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Data Customer');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Data Customer.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = $this->excel->Xlsx($spreadsheet);
		$writer->save('php://output');
		exit;
	}
}

/* End of file Customer.php */
/* Location: ./application/controllers/Customer.php */

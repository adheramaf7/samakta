<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bidang_usaha extends CI_Controller {

public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('id')) redirect('login');
		$this->load->model('bidang_usaha_model','model');
	}

	function get_data()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$list = $this->model->get_data();

		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = ($key + 1) . '.';
				$data['data'][$key][] = $value['nama_bidang_usaha'];
				$data['data'][$key][] = $value['status_bidang_usaha'];
				$data['data'][$key][] = $value['id_bidang_usaha'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_by_id()
	{
		if(!$this->input->is_ajax_request()) redirect();
		
		$id = $this->input->post('id');
		$data = $this->model->get_by_id($id)->row();

		echo json_encode($data);
	}

	function save()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$exec = false;
		
		$data_array = array(
			'id_bidang_usaha' => $this->input->post('id_bidang_usaha') ? $this->input->post('id_bidang_usaha') : null,
			'nama_bidang_usaha' => $this->input->post('nama_bidang_usaha'),
			'status_bidang_usaha' => $this->input->post('status_bidang_usaha'),
		);

		if ($data_array['id_bidang_usaha']) {
			$exec = $this->model->update(['id_bidang_usaha' => $data_array['id_bidang_usaha']],$data_array);
		} else{
			$exec = $this->model->save($data_array);
		}


		echo json_encode(
			['status' => $exec]
		);
	}

	function delete()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$exec = $this->model->delete(['id_bidang_usaha' => $this->input->post('id')]);

		echo json_encode(
			['status' => $exec]
		);
		
	}

}

/* End of file Bidang_usaha.php */
/* Location: ./application/controllers/Bidang_usaha.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

	private $path_upload_foto = 'assets/upload/admin/foto/';

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('id')) redirect('login');
		$this->load->model('admin_model', 'model');
	}

	function load_profil()
	{
		if (!$this->input->is_ajax_request()) redirect();
		$id = $this->session->id;

		$data = $this->model->get_by_id($id)->row();

		echo json_encode($data);
	}

	function update_foto()
	{
		if (!$this->input->is_ajax_request()) redirect();
		$id = $this->session->id;

		$data_array = [];
		
		if ($this->input->post('hapus_foto')) {
			$data_array['path_foto_admin'] = null;
			$folder = $this->path_upload_foto.$id;
			$this->delete_all_files($folder);
		}

		if (!empty($_FILES['upload_foto']['name'])) {
			$folder = $this->path_upload_foto.$id;
			$this->delete_all_files($folder);
			$upload_action = $this->_do_upload_foto($id,'Foto admin-'.$id.'');
			if (!$upload_action['status']) {
				echo json_encode($upload_action);
				exit;
			}
			$data_array['path_foto_admin'] = $upload_action['pesan'];
		}

		$exec = $this->model->update(['id_admin' => $id], $data_array);

		echo json_encode(
			['status' => $exec]
		);
	}

	function update_profil()
	{
		if (!$this->input->is_ajax_request()) redirect();
		$id = $this->session->id;
		
		$data_array = [
			'nama_admin' => $this->input->post('nama_admin')
		];

		$exec = $this->model->update(['id_admin' => $id], $data_array);

		echo json_encode(
			['status' => $exec]
		);
	}

	function ganti_password()
	{
		if (!$this->input->is_ajax_request()) redirect();
		$id = $this->session->id;

		$data = $this->model->get_by_id($id)->row();

		$password_lama = $this->input->post('password_lama');
		$password_baru = $this->input->post('password_baru');

		if (!password_verify($password_lama, $data->password_admin)) {
			echo json_encode(
				[
					'status' => false,
					'pesan'	=> 'Password Lama Salah'
				]
			);
			exit;
		}

		$data_array = [
			'password_admin' => password_hash($password_baru, PASSWORD_DEFAULT)
		];

		$exec = $this->model->update(['id_admin' => $id], $data_array);

		echo json_encode(
			['status' => $exec]
		);
	}

	private function _do_upload_foto($id_admin, $filename)
	{
		$path = $this->path_upload_foto . $id_admin;
		$result_upload = [
			'status' => false,
			'pesan' => '',
		];

		if (!file_exists($path))
			mkdir($path, 0777, true);

		$config['upload_path']          = $path;
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['max_size']             = 10240; //set max size allowed in Kilobyte
		$config['max_width']            = 10000; // set max width image allowed
		$config['max_height']           = 10000; // set max height allowed
		$config['overwrite']           	= true; // set max height allowed
		$config['file_name']            = round(microtime(true)*1000); //just milisecond timestamp fot unique name

		$this->load->library('upload');
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_foto')) {
			$result_upload['pesan'] = 'Upload Foto error: ' . $this->upload->display_errors('', '');
			return $result_upload;
		}
		$result_upload['status'] = true;
		$result_upload['pesan'] = $this->upload->data('file_name');
		return $result_upload;
	}

	private function delete_all_files($folder)
	{
		//Get a list of all of the file names in the folder.
		$files = glob($folder . '/*');
		 
		//Loop through the file list.
		foreach($files as $file){
		    //Make sure that this is a file and not a directory.
		    if(is_file($file)){
		        //Use the unlink function to delete the file.
		        unlink($file);
		    }
		}
	}
}

/* End of file Profil.php */
/* Location: ./application/controllers/Profil.php */
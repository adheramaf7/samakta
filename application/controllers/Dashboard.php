<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('id')) redirect('login');
		$this->load->model('dashboard_model', 'model');
	}

	function load_statistik()
	{
		if(!$this->input->is_ajax_request()) redirect();
		$statistik = $this->model->get_statistik()->row();
		echo json_encode($statistik);
	}

	function load_target()
	{
		if(!$this->input->is_ajax_request()) redirect();
		$tahun = $this->input->get('tahun');
		$data = [
			'target' => $this->model->get_target($tahun),
			'one_time' => $this->model->get_total_nilai($tahun,'O'),
			'recurring' => $this->model->get_total_nilai($tahun,'R'),
		];
		echo json_encode($data);
	}

	function load_data()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$jumlah_tampil = $this->input->get('jumlah_tampil');
		$tahun = $this->input->get('tahun');

		$data = [];
		$data['tipe_customer'] = $this->model->get_chart_tipe_customer($tahun)->result_array();
		$data['bidang_usaha'] = $this->model->get_chart_bidang_usaha($tahun)->result_array();
		$data['price_project'] = $this->model->get_chart_price_project($tahun)->result_array();
		$data['status_project'] = $this->model->get_chart_status_project($tahun)->result_array();
		$data['product'] = $this->model->get_chart_product($tahun)->result_array();
		$data['project'] = $this->model->get_chart_project($tahun)->result_array();
		$data['tipe_kontrak'] = $this->model->get_chart_tipe_kontrak($tahun)->result_array();
		$data['project_vendor'] = $this->model->get_chart_project_vendor($tahun,$jumlah_tampil)->result_array();
		$data['sales_project'] = $this->model->get_chart_sales_project($tahun,$jumlah_tampil)->result_array();

		echo json_encode($data);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {

	private $path_upload = 'assets/upload/vendor/lampiran/';
	private $path_upload_foto = 'assets/upload/vendor/foto/';
	private $jenis_file = 'gif|jpg|png|jpeg|pdf|doc|docx|xls|xlsx|ppt|pptx';

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('id')) redirect('login');
		$this->load->model('vendor_model','model');
	}

	function get_data()
	{
		if(!$this->input->is_ajax_request()) redirect();


		$list = $this->model->get_data();
				
		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = ($key + 1) . '.';
				$data['data'][$key][] = $value['nama_vendor'];
				$data['data'][$key][] = $value['alamat_vendor'].', '.cap_each_word($value['city_name']).', '.cap_each_word($value['province_name']);
				$data['data'][$key][] = $value['status_vendor'];
				$data['data'][$key][] = $value['lampiran'];
				$data['data'][$key][] = $value['id_vendor'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_lampiran()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$id_vendor = $this->input->get('vendor');

		$list = $this->model->get_data_lampiran($id_vendor);
		
		$data['data']    = [];
		$data['total']   = 0;
		$data['success'] = false;

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data['data'][$key][] = ($key + 1) . '.';
				$data['data'][$key][] = $value['file_lampiran'];
				$data['data'][$key][] = $value['id_vendor'];
				$data['data'][$key][] = $value['id_jenis_lampiran'];
				$data['total'] = $key + 1;
			}

			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function get_data_by_id()
	{
		if(!$this->input->is_ajax_request()) redirect();
		
		$id = $this->input->post('id');
		$data = $this->model->get_by_id($id)->row();

		echo json_encode($data);
	}


	function get_jenis_lampiran()
	{
		if(!$this->input->is_ajax_request()) redirect();
		
		$list = $this->model->get_data_jenis_lampiran();

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['id_jenis_lampiran'];
				$data[$key]['name'] = $value['nama_jenis_lampiran'];
			}
		}

		echo json_encode($data);
	}

	function get_province()
	{
		if(!$this->input->is_ajax_request()) redirect();
		
		$list = $this->model->get_data_province();

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['province_id'];
				$data[$key]['name'] = $value['province_name'];
			}
		}

		echo json_encode($data);
	}

	function get_city()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$province_id = $this->input->post('province_id');
		$list = $this->model->get_data_city($province_id);

		$data = [];

		if ($list->num_rows() > 0) {
			foreach ($list->result_array() as $key => $value) {
				$data[$key]['id']   = $value['city_id'];
				$data[$key]['name'] = $value['city_name'];
			}
		}

		echo json_encode($data);
	}

	function save()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$exec = false;
		
		$data_array = array(
			'id_vendor' => $this->input->post('id_vendor') ? $this->input->post('id_vendor') : null,
			'nama_vendor' => $this->input->post('nama_vendor'),
			'npwp_vendor' => $this->input->post('npwp_vendor'),
			'alamat_vendor' => $this->input->post('alamat_vendor'),
			'city_id' => $this->input->post('kota'),
			'kode_pos_vendor' => $this->input->post('kode_pos_vendor'),
			'telp_vendor' => $this->input->post('telp_vendor'),
			'email_vendor' => $this->input->post('email_vendor'),
			'website_vendor' => $this->input->post('website_vendor'),
			'bidang_usaha_vendor' => $this->input->post('bidang_usaha_vendor'),
			'pic_vendor' => $this->input->post('pic_vendor'),
			'telp_pic_vendor' => $this->input->post('telp_pic_vendor'),
			'status_vendor' => $this->input->post('status_vendor'),
		);

		if ($data_array['id_vendor']) {
			if ($this->input->post('hapus_foto')) {
				$data_array['path_foto_vendor'] = null;
				$folder = $this->path_upload_foto.$data_array['id_vendor'];
				$this->delete_all_files($folder);
			}

			if (!empty($_FILES['upload_foto']['name'])) {
				$folder = $this->path_upload_foto.$data_array['id_vendor'];
				$this->delete_all_files($folder);
				$upload_action = $this->_do_upload_foto($data_array['id_vendor'],'Foto Vendor-'.$data_array['id_vendor'].'');
				if (!$upload_action['status']) {
					echo json_encode($upload_action);
					exit;
				}
				$data_array['path_foto_vendor'] = $upload_action['pesan'];
			}

			$exec = $this->model->update(['id_vendor' => $data_array['id_vendor']], $data_array);
		} else{
			$exec = $this->model->save($data_array);
			$id_vendor = $this->db->insert_id();

			if (!empty($_FILES['upload_foto']['name'])) {
				$upload_action = $this->_do_upload_foto($id_vendor,'Foto Vendor-'.$id_vendor.'');
				if (!$upload_action['status']) {
					echo json_encode($upload_action);
					exit;
				}
				$data_upload['path_foto_vendor'] = $upload_action['pesan'];
				$exec = $this->model->update(['id_vendor' => $id_vendor], $data_upload);
			}
		}


		echo json_encode(
			['status' => $exec]
		);
	}

	function save_lampiran()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$exec = false;
		
		$data_array = array(
			'id_vendor' => $this->input->post('id_vendor_lampiran'),
			'id_jenis_lampiran' => $this->input->post('jenis_lampiran'),
		);

		$nama_jenis_lampiran = $this->model->get_nama_jenis_lampiran($data_array['id_jenis_lampiran']);

		$nama_lampiran = $nama_jenis_lampiran.'-'.$data_array['id_vendor'];

		$upload_action = $this->_do_upload($data_array['id_vendor'],$nama_lampiran);

		if (!$upload_action['status']) {
			echo json_encode($upload_action);
			exit;
		}

		$data_array['file_lampiran'] = $upload_action['pesan'];

		$exec = $this->model->save_lampiran($data_array);

		echo json_encode(
			['status' => $exec]
		);
	}

	function delete()
	{
		if (!$this->input->is_ajax_request()) redirect();

		$this->db->trans_begin();

		$del1 = $this->model->delete_lampiran(['id_vendor' => $this->input->post('id')]);
		$del2 = $this->model->delete(['id_vendor' => $this->input->post('id')]);

		$exec = $this->db->trans_status();

		if ($exec === FALSE)
		{
	        $this->db->trans_rollback();
		}
		
		else
		{
	        $this->db->trans_commit();
    		$folder = $this->path_upload.$this->input->post('id');
    		if (file_exists($folder)) {
	    		$this->delete_all_files($folder);
	    		rmdir($folder);
    		}
    		
    		$folder_foto = $this->path_upload_foto.$this->input->post('id');
			if (file_exists($folder_foto)) {
	    		$this->delete_all_files($folder_foto);
	    		rmdir($folder_foto);
    		}
		}

		echo json_encode(
			['status' => $exec]
		);

		
	}

	function delete_lampiran()
	{
		if(!$this->input->is_ajax_request()) redirect();

		$exec = $this->model->delete_lampiran(
			['id_vendor' => $this->input->post('id_vendor'),
			'id_jenis_lampiran' => $this->input->post('id_jenis_lampiran')
		]);

		$path = $this->path_upload.$this->input->post('id_vendor');
		
		if(file_exists($path.'/'.$this->input->post('nama_lampiran')))
			unlink($path.'/'.$this->input->post('nama_lampiran'));

		echo json_encode(
			['status' => $exec]
		);

	}

	function download_lampiran()
	{
		$id_vendor = $this->input->get('vendor');
		$nama_file = $this->input->get('file');
		$path = $this->path_upload.$id_vendor;
		force_download($path.'/'.$nama_file,NULL);
	}



	private function _do_upload($id_vendor,$filename)
	{
		$path = $this->path_upload.$id_vendor;
		$result_upload = [
			'status' => false,
			'pesan' => '',
		];

        if(!file_exists($path))
            mkdir($path, 0777, true);

		$config['upload_path']          = $path;
        $config['allowed_types']        = $this->jenis_file;
        $config['max_size']             = 10240; //set max size allowed in Kilobyte
        $config['max_width']            = 10000; // set max width image allowed
        $config['max_height']           = 10000; // set max height allowed
        $config['overwrite']           	= true; // set max height allowed
        $config['file_name']            = str_replace(' ', '_', $filename); //just milisecond timestamp fot unique name

        $this->load->library('upload');
        $this->upload->initialize($config);

        if(!$this->upload->do_upload('file_lampiran'))
        {
			$result_upload['pesan'] = 'Upload error: '.$this->upload->display_errors('','');
			return $result_upload;
		}
		$result_upload['status']= true;
		$result_upload['pesan']= $this->upload->data('file_name');
		return $result_upload;
	}

	private function _do_upload_foto($vendor, $filename)
	{
		$path = $this->path_upload_foto . $vendor;
		$result_upload = [
			'status' => false,
			'pesan' => '',
		];

		if (!file_exists($path))
			mkdir($path, 0777, true);

		$config['upload_path']          = $path;
		$config['allowed_types']        = 'jpg|png|jpeg';
		$config['max_size']             = 10240; //set max size allowed in Kilobyte
		$config['max_width']            = 10000; // set max width image allowed
		$config['max_height']           = 10000; // set max height allowed
		$config['overwrite']           	= true; // set max height allowed
		$config['file_name']            = round(microtime(true)*1000); //just milisecond timestamp fot unique name

		$this->load->library('upload');
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_foto')) {
			$result_upload['pesan'] = 'Upload Foto error: ' . $this->upload->display_errors('', '');
			return $result_upload;
		}
		$result_upload['status'] = true;
		$result_upload['pesan'] = $this->upload->data('file_name');
		return $result_upload;
	}

	private function delete_all_files($folder)
	{
		//Get a list of all of the file names in the folder.
		$files = glob($folder . '/*');
		 
		//Loop through the file list.
		foreach($files as $file){
		    //Make sure that this is a file and not a directory.
		    if(is_file($file)){
		        //Use the unlink function to delete the file.
		        unlink($file);
		    }
		}
	}

	function cetak_detil()
	{
		// echo '<h1>YES</h1>'; die;
		$id_vendor = $this->input->post('id_vendor');
		$this->load->library('Pdfgenerator');

		$vendor = $this->model->get_by_id($id_vendor)->row();
		
		$foto = ($vendor->path_foto_vendor) ? $this->path_upload_foto.$vendor->id_vendor.'/'.$vendor->path_foto_vendor : 'assets/img/avatar.png';

		$html = '';

		$html .= '<p style="font-weight:bold; font-size:16px; text-align:center;">Detil Data Vendor</p>';

		$html .= 	'<div style="width:30%;float:left; text-align:center;">

						<img src="'.$foto.'" style="height:auto;width:100px;margin-top:20px; ">
					</div>';


		$html .= 	'<div style="width:70%;float:right";>
						<table>
							<tr>
								<th style="width:100px;font-size:14px">Nama</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$vendor->nama_vendor.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">NPWP</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$vendor->npwp_vendor.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Alamat</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$vendor->alamat_vendor.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Kota / Kab</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.cap_each_word($vendor->city_name).'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Provinsi</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.cap_each_word($vendor->province_name).'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Kode Pos</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$vendor->kode_pos_vendor.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">No. Telepon</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$vendor->telp_vendor.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Email</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$vendor->email_vendor.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Website</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$vendor->website_vendor.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Bidang Usaha</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$vendor->bidang_usaha_vendor.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">PIC</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$vendor->pic_vendor.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">No. Telp. PIC</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.$vendor->telp_pic_vendor.'</td>
							</tr>
							<tr>
								<th style="width:100px;font-size:14px">Status Aktif</th>
								<td style="width:5px;font-size:14px">:</td>
								<td style="font-size:14px">'.($vendor->status_vendor == 'A' ? 'Aktif' : 'Tidak Aktif').'</td>
							</tr>
						</table>
					</div>';

		$data = [];
		$data['html'] = $html;
        $this->pdfgenerator->setPaper('A4', 'portrait');
        $this->pdfgenerator->filename = "laporan.pdf";
        $this->pdfgenerator->load_view('format_laporan', $data);
	}

	function export_pdf()
	{
		// echo '<h1>YES</h1>'; die;
		$this->load->library('Pdfgenerator');

		$list = $this->model->get_data();

		$html = '';

		$html .= '<p style="font-weight:bold; font-size:16px; text-align:center;">Data Vendor</p>';

		if ($list->num_rows()>0) {
			$html.= '<table style="border-collapse: collapse; table-layout:fixed;" border="1px solid" width="100%">';
			$html.=		'<thead>
							<tr style="background-color:lightgrey">
								<th class="data-center" style="width:5%">No.</th>
								<th class="data-center" style="width:18%">Nama</th>
								<th class="data-center" style="width:12%">NPWP</th>
								<th class="data-center" style="width:22%">Alamat</th>
								<th class="data-center" style="width:15%">Telp.</th>
								<th class="data-center" style="width:18%">Email</th>
								<th class="data-center" style="width:10%">Status</th>
							</tr>
						</thead>';
			$html.= 	'<tbody>';
			foreach ($list->result_array() as $key => $value) {
				$html.= '<tr>
							<td>'.($key+1).'.</td>
							<td>'.$value['nama_vendor'].'</td>
							<td>'.$value['npwp_vendor'].'</td>
							<td>'.$value['alamat_vendor'].'</td>
							<td>'.$value['telp_vendor'].'</td>
							<td style="">'.$value['email_vendor'].'</td>
							<td>'.($value['status_vendor']=='A'? 'Aktif' :'Tidak Aktif').'</td>
						</tr>';
			}
			$html.= 	'</tbody>';
			$html.= '</table>';
		}

		$data = [];
		$data['html'] = $html;
        $this->pdfgenerator->setPaper('A4', 'portrait');
        $this->pdfgenerator->filename = "laporan.pdf";
        $this->pdfgenerator->load_view('format_laporan', $data);
	}

	function export_excel()
	{
		$this->load->library('Excel');

		$list = $this->model->get_data();
		$spreadsheet = $this->excel->Spreadsheet();
		// Set document properties
		$spreadsheet->getProperties()->setCreator('SamaktaMitra Admin Area')
		->setLastModifiedBy('SamaktaMitra')
		->setTitle('Data Vendor')
		->setSubject('Data Vendor')
		->setDescription('Laporan Data Vendor')
		->setKeywords('excel,vendor,data')
		->setCategory('Report Excel');

		// Add some data
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A1', 'DATA VENDOR')->mergeCells('A1:M1');

		$spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray( [
			'font' => [ 
				'bold' => TRUE
			],
			'alignment' => [
				'horizontal' => 'center'
			]
		]);

		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A3', 'NO.')
		->setCellValue('B3', 'NAMA VENDOR')
		->setCellValue('C3', 'NPWP')
		->setCellValue('D3', 'ALAMAT')
		->setCellValue('E3', 'KOTA/KAB')
		->setCellValue('F3', 'PROVINSI')
		->setCellValue('G3', 'KODE POS')
		->setCellValue('H3', 'TELEPON')
		->setCellValue('I3', 'EMAIL')
		->setCellValue('J3', 'WEBSITE')
		->setCellValue('K3', 'PIC')
		->setCellValue('L3', 'TELP. PIC')
		->setCellValue('M3', 'STATUS AKTIF')
		;

		$spreadsheet->getActiveSheet()->getStyle('A3:M3')->getFont()->applyFromArray( [ 'bold' => TRUE] );



		// Fill data
		$i=4;
		$last_row=4;
		foreach ($list->result_array() as $key => $value) {
			$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A'.$i, ($key + 1) . '.')
			->setCellValue('B'.$i, $value['nama_vendor'])
			->setCellValue('C'.$i, $value['npwp_vendor'])
			->setCellValue('D'.$i, $value['alamat_vendor'])
			->setCellValue('E'.$i, cap_each_word($value['city_name']) )
			->setCellValue('F'.$i, cap_each_word($value['province_name']) )
			->setCellValue('G'.$i, $value['kode_pos_vendor'])
			->setCellValue('H'.$i, $value['telp_vendor'])
			->setCellValue('I'.$i, $value['email_vendor'])
			->setCellValue('J'.$i, $value['website_vendor'])
			->setCellValue('K'.$i, $value['pic_vendor'])
			->setCellValue('L'.$i, $value['telp_pic_vendor'])
			->setCellValue('M'.$i, $value['status_vendor'] == 'A' ? 'Aktif' : 'Tidak Aktif')
			;
			$i++;
		}
		$last_row = $i-1;

		$width = [
			'A' => 5,
			'B' => 20,
			'C' => 18,
			'D' => 35,
			'E' => 25,
			'F' => 25,
			'G' => 13,
			'H' => 20,
			'I' => 25,
			'J' => 25,
			'K' => 15,
			'L' => 15,
			'M' => 13,
		];

		//loop for set Auto Size Columns
		foreach(range('A','M') as $v){
			$spreadsheet->getActiveSheet()->getColumnDimension($v)->setWidth($width[$v]);
		}

		//style table header
		$spreadsheet->getActiveSheet()->getStyle('A3:M3')->applyFromArray([
			'borders' => [
              	'allBorders' => [
                      'borderStyle' => 'thin',
                      'color' => [
                          'rgb' => '000000'
                      ]
                  ]
             ],
             'fill' => [
             	'fillType' => 'solid',
             	'startColor' => [ 'rgb' => 'D3D3D3' ],
             	'endColor' => [ 'rgb' => 'D3D3D3' ],
             ]
         ]);

		//style tabel body
		$spreadsheet->getActiveSheet()->getStyle('A4:M'.$last_row)->applyFromArray([
			'borders' => [
              	'allBorders' => [
                      'borderStyle' => 'thin',
                      'color' => [
                          'rgb' => '000000'
                      ]
                  ]
             ],
         ]);

		//move cursor to cell A1
		$spreadsheet->getActiveSheet()->getStyle('A1');

		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Data Vendor');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Data Vendor.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = $this->excel->Xlsx($spreadsheet);
		// var_dump($borders);
		// die();
		$writer->save('php://output');
		exit;
	}

}

/* End of file Vendor.php */
/* Location: ./application/controllers/Vendor.php */
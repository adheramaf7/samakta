<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('id')) redirect('login');
		$this->load->model('Login_model','model');
	}

	function index()
	{
		$this->create_page();
	}

	function lost()
	{
		$this->load->view('404_view');
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}

	function create_page($page = "site/beranda",$konten="konten/home_view")
	{
		$ha = $this->hak_akses($page);
		
		if(!$ha['open']){
			echo '<script>alert("Anda tidak mempunyai akses ke halaman ini")</script>';
			echo '<script>location.href="'.base_url().'"</script>';
			die;
		}

		$admin = $this->model->get_data_admin_by_username($this->session->username)->row();

		$data_halaman = [
			'page' => $konten,
			'ha' => $ha,
			'foto_profil' => $admin->path_foto_admin
		];
		if(!file_exists(APPPATH.'views/'.$konten.'.php')) {
			$this->lost();
		} else{
			$this->load->view('base', $data_halaman);
		}
	}

	//routes halaman
	
	function beranda($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/home_view"); }
	function bidang_usaha($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/bidang_usaha_view"); }
	function dashboard($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/dashboard_view"); }
	function data_admin($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/data_admin_view"); }
	function data_customer($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/data_customer_view"); }
	function data_sales($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/data_sales_view"); }
	function data_vendor($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/data_vendor_view"); }
	function data_project($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/data_project_view"); }
	function jabatan($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/jabatan_view"); }
	function jenis_lampiran($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/jenis_lampiran_view"); }
	function monitoring($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/monitoring_view"); }
	function product($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/product_view"); }
	function profil($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/profil_view"); }
	function seeder($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/seeder_view"); }
	function setting_perusahaan($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/setting_perusahaan_view"); }
	function target_perusahaan($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/target_perusahaan_view"); }
	function tipe_customer($value='') { $this->create_page(__CLASS__.'/'.__FUNCTION__,"konten/tipe_customer_view"); }

	private function hak_akses($site_url)
	{
		$url_arr = explode('/', $site_url);
		$page = $url_arr[1];

		// NOTE
		// ADR : Administrator
		// ADM : Admin
		// MNG : Manager
		// 
		// Open : hak akses membuka halaman (untuk mencegah apabila user membuka halaman lewat url langsung tanpa klik menu)
		// Insert : hak akses nambah
		// Update : hak akses untuk mengubah (harus ada hak akses view) jika false, maka form akan disable
		// Delete : hak akses untuk hapus
		// View  : hak akses untuk menampilkan tombol ubah

		switch ($this->session->level) {
			case 'ADR':
				$ha = [];
				$ha['beranda'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
				];
				$ha['bidang_usaha'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
				];					
				$ha['dashboard'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
				];				
				$ha['data_admin'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
				];
				$ha['data_customer'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
					'lampiran' => [
						'open' => true,
						'insert' => true,
						'delete' => true,
					]
				];
				$ha['data_sales'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
				];
				$ha['data_vendor'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
					'lampiran' => [
						'open' => true,
						'insert' => true,
						'delete' => true,
					]
				];
				$ha['data_project'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
					'project_vendor' => [
						'insert' => true,
						'update' => true,
						'delete' => true,
					],
					'tender_project' => [
						'open' => true,
						'update' => true,
					],
					'tender_vendor' => [
						'open' => true, //membuka halaman kelola tender project
						'update' => true //update data
					],
				];
				$ha['jabatan'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
				];
				$ha['jenis_lampiran'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
				];
				$ha['monitoring'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
				];
				$ha['product'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
				];
				$ha['profil'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
				];
				$ha['seeder'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
				];
				$ha['seeder'] = [
					'open' => true,
					'update' => true,
				];
				$ha['setting_perusahaan'] = [
					'open' => true,
					'update' => true,
				];
				$ha['target_perusahaan'] = [
					'open' => true,
					'update' => true,
				];
				$ha['tipe_customer'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => true,
					'view' 	=> true,
				];
				return $ha[$page];
				break;

			case 'ADM':
				$ha = [];
				$ha['beranda'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => false,
					'view' 	=> true,
				];
				$ha['bidang_usaha'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => false,
					'view' 	=> true,
				];				
				$ha['dashboard'] = [
					'open' => false,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];				
				$ha['data_admin'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['data_customer'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => false,
					'view' 	=> true,
					'lampiran' => [
						'open' => true,
						'insert' => true,
						'delete' => false,
					]
				];
				$ha['data_sales'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> true,
				];
				$ha['data_vendor'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> true,
					'lampiran' => [
						'open' => true,
						'insert' => false,
						'delete' => false,
					]
				];
				$ha['data_project'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => false,
					'view' 	=> true,
					'project_vendor' => [
						'insert' => true,
						'update' => true,
						'delete' => false,
					],
					'tender_project' => [
						'open' => true,
						'update' => true,
					],
					'tender_vendor' => [
						'open' => true,
						'update' => true,
					],
				];
				$ha['jabatan'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['jenis_lampiran'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['monitoring'] = [
					'open' => false,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['product'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['profil'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => false,
					'view' 	=> true,
				];
				$ha['seeder'] = [
					'open' => false,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['seeder'] = [
					'open' => false,
					'update' => false,
				];
				$ha['setting_perusahaan'] = [
					'open' => false,
					'update' => false,
				];
				$ha['target_perusahaan'] = [
					'open' => false,
					'update' => false,
				];
				$ha['tipe_customer'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				return $ha[$page];
				break;

			case 'MNG':
				$ha['bidang_usaha'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['beranda'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];				
				$ha['dashboard'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];				
				$ha['data_admin'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['data_customer'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
					'lampiran' => [
						'open' => true,
						'insert' => false,
						'delete' => false,
					]
				];
				$ha['data_sales'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['data_vendor'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
					'lampiran' => [
						'open' => true,
						'insert' => false,
						'delete' => false,
					]
				];
				$ha['data_project'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> true,
					'project_vendor' => [
						'insert' => false,
						'update' => false,
						'delete' => false,
					],
					'tender_project' => [
						'open' => true,
						'update' => false,
					],
					'tender_vendor' => [
						'open' => true,
						'update' => false,
					],
				];
				$ha['jabatan'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['jenis_lampiran'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['monitoring'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['product'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['profil'] = [
					'open' => true,
					'insert' => true,
					'update' => true,
					'delete' => false,
					'view' 	=> true,
				];
				$ha['seeder'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				$ha['seeder'] = [
					'open' => false,
					'update' => false,
				];
				$ha['setting_perusahaan'] = [
					'open' => true,
					'update' => false,
				];
				$ha['target_perusahaan'] = [
					'open' => true,
					'update' => false,
				];
				$ha['tipe_customer'] = [
					'open' => true,
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				return $ha[$page];
				break;
			
			default:
				$ha = [
					'insert' => false,
					'update' => false,
					'delete' => false,
					'view' 	=> false,
				];
				return $ha;
				break;
		}
	}

}

/* End of file Site.php */
/* Location: ./application/controllers/Site.php */